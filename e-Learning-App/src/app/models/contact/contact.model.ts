export class ContactModel {
    senderAddress: string;
    senderName: string;
    subject: string;
    content: string;
    attachments: FormData[];
}