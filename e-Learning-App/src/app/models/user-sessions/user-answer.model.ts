export class UserAnswer {
    id: string;
    userSessionId: string;
    questionId: string;
    choiceIds: string[];
    correctlyAnswered: boolean;
}