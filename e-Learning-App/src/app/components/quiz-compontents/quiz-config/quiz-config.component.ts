import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { QuestionTypeEnum } from 'src/app/enums';
import { CustomPopupService } from 'src/app/services/custom-popup.service';
import { QuestionService } from 'src/app/services/question.service';
import { QuestionVm } from 'src/app/view-models/quiz/question.view-model';
import { QuizVm } from 'src/app/view-models/quiz/quiz.view-model';
import { ConfirmationPopupAction } from '../../custom-popup/custom-popup.component';

@Component({
    selector: 'quiz-config',
    templateUrl: './quiz-config.component.html',
    styleUrls: ['./quiz-config.component.scss']
})

export class QuizConfigComponent implements OnInit {
    @Input() quiz: QuizVm = new QuizVm();
    @Output() quizChange: EventEmitter<QuizVm> = new EventEmitter<QuizVm>();
    quizTitleControl: FormControl;
    questionTitleControl: FormControl;
    anyQuestionInEditOrAddMode: boolean = false;

    constructor(private customPopupService: CustomPopupService, private questionService: QuestionService) { }

    ngOnInit() {
        this.quizTitleControl = new FormControl('', [Validators.required]);
        this.questionTitleControl = new FormControl('', [Validators.required]);
        if (!this.quiz) {
            this.quiz = new QuizVm();
        }
        if (!this.quiz?.questions) {
            this.quiz.questions = [];
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes["quiz"]) {
            this.quizTitleControl?.setValue(this.quiz.title);
        }
    }

    onAddQuestion() {
        this.questionTitleControl.reset();
        this.questionTitleControl.markAsUntouched();
        this.quiz?.questions?.push({
            questionType: QuestionTypeEnum.SingleChoice,
            choices: [],
            isAddMode: true
        } as QuestionVm);
        this.anyQuestionInEditOrAddMode = true;
    }

    async onDeleteAllQuestions() {
        let response = await this.customPopupService.openPopup("Delete questions",
            ["Are you sure that you want to delete all the questions?"],
            [
                { text: "Yes", action: ConfirmationPopupAction.Ok },
                { text: "Cancel", action: ConfirmationPopupAction.Cancel }
            ]);
        if (response == ConfirmationPopupAction.Ok && !this.quiz?.id) {
            this.quiz.questions = [];
        } else if (response == ConfirmationPopupAction.Ok && this.quiz.id) {
            await this.questionService.deleteQuestionsByQuizId(this.quiz.id);
        }
        this.anyQuestionInEditOrAddMode = false;
    }

    async onDeleteQuestion(question: QuestionVm) {
        if (!question.id) {
            this.quiz.questions = this.quiz.questions.filter(q => q != question);
        } else {

        }
    }

    onFinish(event: any) {
        this.anyQuestionInEditOrAddMode = false;
    }

    onCancel(question: QuestionVm) {
        if (question.isAddMode) {
            this.quiz.questions = this.quiz.questions.filter(q => !q.isAddMode);
        } else {
            var question = this.quiz.questions.find(q => q.isEditMode);
            question.isEditMode = false;
        }
        this.anyQuestionInEditOrAddMode = false;
    }

    onTitleChange(event: any) {
        this.quiz.title = this.quizTitleControl?.value;
        this.quizChange.emit(this.quiz);
    }
}
