﻿namespace Authentication.Dtos
{
    public class UserClaims
    {
        public static string Id { get; set; } = "Id";
        public static string Username { get; set; } = "Username";
        public static string FirstName { get; set; } = "FirstName";
        public static string LastName { get; set; } = "LastName";
        public static string EmailAddress { get; set; } = "EmailAddress";
        public static string Address { get; set; } = "Address";
        public static string PhoneNumber { get; set; } = "PhoneNumber";
        public static string RoleId { get; set; } = "RoleId";
        public static string RoleName { get; set; } = "RoleName";
    }
}
