﻿using eLearningAPI.Enums;
using eLearningAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Interfaces
{
    public interface ILearningPathRepository
    {
        public Task<List<LearningPath>> GetAllLearningPathsAsync();
        public Task<LearningPath> GetLearningPathAsync(int learningPathId);
        public Task<List<LearningPath>> GetByProgrammingLanguageAsync(ProgrammingLanguageEnum programmingLanguage);
        public Task<int> SaveLearningPathAsync(LearningPath learningPath);
        public Task UnlinkLessonFromLearningPathAsync(int lessonId, int learningPathId);
    }
}
