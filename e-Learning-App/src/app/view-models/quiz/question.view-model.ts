import { QuestionTypeEnum } from "src/app/enums";
import { ChoiceVm } from "./choice.view-model";

export class QuestionVm {
    id: string;
    text: string;
    questionType: QuestionTypeEnum;
    rewardPoints: number;
    choices: ChoiceVm[];
    isAddMode: boolean;
    isEditMode: boolean;
}