﻿using eLearningAPI.Enums;
using System;

namespace eLearningAPI.Dtos.UserSesssion
{
    public class UserLessonSessionListItem
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int LessonId { get; set; }
        public int LearningPathId { get; set; }
        public UserSessionStatusEnum Status { get; set; }
    }
}
