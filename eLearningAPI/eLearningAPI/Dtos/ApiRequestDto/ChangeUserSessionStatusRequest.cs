﻿using eLearningAPI.Enums;
using System;

namespace eLearningAPI.Dtos.ApiRequestDto
{
    public class ChangeUserSessionStatusRequest
    {
        public Guid UserSessionId { get; set; }
        public UserSessionStatusEnum Status { get; set; }
    }
}
