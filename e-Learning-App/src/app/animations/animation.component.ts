import { animate, query, state, style, transition, trigger } from "@angular/animations";

export const fader =
    trigger('fader', [
        state('before',
            style({
                position: 'absolute',
                width: '100%',
                opacity: 0,
                transform: 'scale(0) translateY(100%)',
            })
        ),
        state('after', style({ opacity: 1 })),
        transition('before => after', animate('600ms ease'))
    ])