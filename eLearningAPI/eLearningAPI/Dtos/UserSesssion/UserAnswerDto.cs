﻿using System;
using System.Collections.Generic;

namespace eLearningAPI.Dtos.UserSesssion
{
    public class UserAnswerDto
    {
        public Guid Id { get; set; }
        public Guid UserSessionId { get; set; }
        public Guid QuestionId { get; set; }
        public List<Guid> ChoiceIds { get; set; }
        public bool CorrectlyAnswered { get; set; }
    }
}
