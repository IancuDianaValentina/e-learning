﻿CREATE PROCEDURE [dbo].[UserSession_Create]
(
	@UserId UNIQUEIDENTIFIER,
	@LearningPathId INT,
	@LessonId INT,
	@RequiredPoints INT
)
AS
BEGIN

	UPDATE Users
	SET Points = Points - @RequiredPoints
	WHERE Users.Id = @UserId;

	INSERT INTO UserLesson (Id, UserId, LessonId, LearningPathId, [Status])
		OUTPUT INSERTED.Id
		VALUES (NEWID(), @UserId, @LessonId, @LearningPathId, 1);
END