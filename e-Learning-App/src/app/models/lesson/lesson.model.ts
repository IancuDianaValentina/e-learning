import { ProgrammingLanguageEnum } from "src/app/enums";
import { QuizVm } from "src/app/view-models/quiz/quiz.view-model";
import { LearningPathLink } from "../learning-path/learning-path-link.model";
import { LearningPath } from "../learning-path/learning-path.model";

export class Lesson {
    id: number;
    title: string;
    content: string;
    learningTime: number;
    requiredPoints: number;
    programmingLanguage: ProgrammingLanguageEnum;
    quiz: QuizVm;
    learningPathLinks: Array<LearningPathLink>;
    learningPaths: Array<LearningPath>;
}