﻿using MailManagement.Models;
using System.Threading.Tasks;

namespace MailManagement.Services
{
    public interface IMailService
    {
        public Task SendContactMailAsync(ContactMailRequest mailRequest);
    }
}
