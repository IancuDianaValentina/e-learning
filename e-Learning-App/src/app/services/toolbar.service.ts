import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { ToolbarItem } from "../components/toolbar/toolbar.component";


export enum ToolbarActionEnum {
    SetItems = 1
}
export class ToolbarItemCommand {
    action: ToolbarActionEnum;
};

@Injectable({
    providedIn: 'root'
})
export class ToolbarService {
    toolbarMenuItems: Array<ToolbarItem> = [];
    simpleToolbarItems: Array<ToolbarItem> = [];

    toolbarItemsCommands: BehaviorSubject<ToolbarItemCommand> = new BehaviorSubject<ToolbarItemCommand>({ action: ToolbarActionEnum.SetItems }); s

    constructor() { }

    setToolbarItems() {
        this.toolbarItemsCommands.next({ action: ToolbarActionEnum.SetItems });
    }

    getToolbarCommands() {
        return this.toolbarItemsCommands;
    }
}