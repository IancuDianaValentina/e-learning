﻿using Dapper;
using eLearningAPI.Models;
using eLearningAPI.Models.Lessons;
using eLearningAPI.Repositories.Interfaces;
using eLearningAPI.Utils;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Implementations
{
    public class LessonRepository: BaseRepository, ILessonRepository
    {
        private readonly string GetAllLessonsSP = "Lessons_GetAll";
        private readonly string SaveLessonSP = "Lesson_Save";
        private readonly string DeleteLessonSP = "Lesson_Delete";
        private readonly string GetLessonByIdSP = "Lesson_GetById";
        private readonly string GetLessonLearningPathSP = "LessonLearningPath_Get";
        public LessonRepository(IConfiguration configuration): base(configuration) { }

        public async Task DeleteLessonAsync(int lessonId)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(DeleteLessonSP,
                    param: new
                    {
                        Id = lessonId
                    },
                   commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<List<Lesson>> GetAllLessonsAsync()
        {
            using(var connection = Connection)
            {
                var lessonDictionary = new Dictionary<int, Lesson>();
                return (await connection.QueryAsync<Lesson, LearningPathLink, LearningPath, Lesson>(GetAllLessonsSP,
                    (Lesson lesson, LearningPathLink learningPathLink, LearningPath learningPath) => 
                    {
                        Lesson lessonEntry;
                        if (!lessonDictionary.TryGetValue(lesson.Id, out lessonEntry))
                        {
                            lessonEntry = lesson;
                            lessonEntry.LearningPathLinks = new List<LearningPathLink>();
                            lessonEntry.LearningPaths = new List<LearningPath>();
                            lessonDictionary.Add(lessonEntry.Id, lessonEntry);
                        }

                        if (learningPathLink != null)
                        {
                            lessonEntry.LearningPathLinks.Add(learningPathLink);
                        }
                        if (learningPath != null)
                        {
                            lessonEntry.LearningPaths.Add(learningPath);
                        }
                        return lessonEntry;
                    },
                    splitOn: "LearningPathId,Id",
                    commandType: System.Data.CommandType.StoredProcedure)).Distinct().ToList();
            }
        }

        public async Task<Lesson> GetLessonByIdAsync(int lessonId)
        {
            using (var connection = Connection)
            {
                var lessonDictionary = new Dictionary<int, Lesson>();
                return (await connection.QueryAsync<Lesson, LearningPathLink, LearningPath, Lesson>(GetLessonByIdSP,
                    (Lesson lesson, LearningPathLink learningPathLink, LearningPath learningPath) =>
                    {
                        Lesson lessonEntry;
                        if (!lessonDictionary.TryGetValue(lesson.Id, out lessonEntry))
                        {
                            lessonEntry = lesson;
                            lessonEntry.LearningPathLinks = new List<LearningPathLink>();
                            lessonEntry.LearningPaths = new List<LearningPath>();
                            lessonDictionary.Add(lessonEntry.Id, lessonEntry);
                        }

                        if (learningPathLink != null)
                        {
                            lessonEntry.LearningPathLinks.Add(learningPathLink);
                        }
                        if (learningPath != null)
                        {
                            lessonEntry.LearningPaths.Add(learningPath);
                        }
                        return lessonEntry;
                    },
                    splitOn: "LearningPathId,Id",
                    param: new { LessonId = lessonId },
                    commandType: System.Data.CommandType.StoredProcedure)).Distinct().FirstOrDefault();
            }
        }

        public async Task SaveLessonAsync(Lesson lesson)
        {
            var learningPaths = RepositoryUtils.ToIntList(lesson.LearningPathLinks?.Select(l => l.LearningPathId).ToList());
            var questions = new DataTable();
            questions.Columns.Add("Id");
            questions.Columns.Add("QuizId");
            questions.Columns.Add("Type");
            questions.Columns.Add("Text");
            questions.Columns.Add("RewardPoints");
            var choices = new DataTable();
            choices.Columns.Add("Id");
            choices.Columns.Add("QuestionId");
            choices.Columns.Add("Text");
            choices.Columns.Add("IsAnswer");
            foreach (var question in lesson?.Quiz?.Questions)
            {
                var questionId = question.Id != Guid.Empty && question.Id != null ? question.Id : Guid.NewGuid();
                questions.Rows.Add(questionId, lesson.Quiz.Id, (int) question.QuestionType, question.Text, question.RewardPoints);
                question?.Choices?.ForEach(c => choices.Rows.Add(c.Id != Guid.Empty && c.Id != null ? c.Id : Guid.NewGuid(), questionId, c.Text, c.IsAnswer));
            }
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(SaveLessonSP,
                    param: new
                    {
                        LessonId = lesson.Id,
                        Title = lesson.Title,
                        HtmlContent = lesson.Content,
                        LearningTime = lesson.LearningTime,
                        RequiredPoints = lesson.RequiredPoints,
                        ProgrammingLanguageId = lesson.ProgrammingLanguage,
                        LearningPaths = learningPaths.AsTableValuedParameter("IntList"),
                        QuizTitle = lesson.Quiz.Title,
                        Questions = questions.AsTableValuedParameter("Question"),
                        Choices = choices.AsTableValuedParameter("Choice")
                    }, commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<LessonLearningPath> GetLessonLearningPathAsync(int lessonId, int learningPathId)
        {
            using (var connection = Connection)
            {
                return (await connection.QueryAsync<LessonLearningPath, Lesson, LessonLearningPath>(GetLessonLearningPathSP,
                    (LessonLearningPath lessonLearningPath, Lesson lesson) =>
                    {
                        if (lessonLearningPath.NextLesson == null)
                        {
                            lessonLearningPath.NextLesson = lesson;
                        }
                        return lessonLearningPath;
                    },
                    splitOn: "LessonId,Id",
                    param: new { LessonId = lessonId, LearningPathId = learningPathId },
                    commandType: System.Data.CommandType.StoredProcedure)).Distinct().FirstOrDefault();
            }
        }
    }
}
