import { Injectable } from "@angular/core";
import { HttpBackend, HttpClient } from "@angular/common/http";
import { lastValueFrom } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private appConfig: any;
  private http: HttpClient;

  constructor(private handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }

  async loadAppConfig() {
    return lastValueFrom(this.http.get('/assets/config.json'))
      .then(data => {
        this.appConfig = data;
      });
  }

  getUrl(url: string) {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig[url];
  }
}