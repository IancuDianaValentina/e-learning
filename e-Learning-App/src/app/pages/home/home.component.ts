import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LearningPathHelper } from 'src/app/helpers';
import { LearningPathService } from 'src/app/services/learning-path.service';
import { LearningPathVm } from 'src/app/view-models/learning-path.view-model';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
    carouselUrls: string[] = [];
    learningPaths: LearningPathVm[] = [];

    constructor(private learningPathServie: LearningPathService, private router: Router) { }

    async ngOnInit(): Promise<void> {
        this.carouselUrls = [
            '../../../assets/Images/laptop3.jpg',
            '../../../assets/Images/laptop2.jpg',
            '../../../assets/Images/laptop1.jpg',
        ];

        const learningPathModels = await this.learningPathServie.getAllLearningPaths();
        this.learningPaths = learningPathModels.map(item => ({
            id: item.id,
            title: item.title,
            description: item.description,
            noLearningHours: item.noLearningHours,
            programmingLanguage: LearningPathHelper.getProgrammingLanguage(item.programmingLanguage),
            learningLevel: LearningPathHelper.getLearningLevel(item.learningLevel),
            learningLevelIcon: LearningPathHelper.getLearningLevelIcon(item.learningLevel)
        }) as LearningPathVm);
    }

    onOpenLearningPath(learningPathId: number) {
        this.router.navigateByUrl(`/learning-path/${learningPathId}/view`);
    }
}
