import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmationPopupAction } from 'src/app/components/custom-popup/custom-popup.component';
import { LearningLevelEnum } from 'src/app/enums';
import { ProgrammingLanguageHelper } from 'src/app/helpers';
import { CustomPopupService } from 'src/app/services/custom-popup.service';
import { LessonService } from 'src/app/services/lesson.service';
import { LearningLevelIcon } from 'src/app/view-models/learning-path.view-model';
import { LessonVm } from 'src/app/view-models/lesson.view-model';

@Component({
    selector: 'lesson-list',
    templateUrl: './lesson-list.component.html',
    styleUrls: ['./lesson-list.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})

export class LessonListComponent implements OnInit {
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    displayedColumns: string[] = [
        "id",
        "title",
        "learningTime",
        "requiredPoints",
        "programmingLanguage",
        "actions"
    ];
    lessons: LessonVm[] = [];
    lessonsDataSource: MatTableDataSource<LessonVm> = new MatTableDataSource<LessonVm>();
    expandedLesson: LessonVm | null;

    constructor(private router: Router, private lessonService: LessonService, private popupService: CustomPopupService) { }

    async ngOnInit() {
        await this.loadData();
        this.lessonsDataSource.paginator = this.paginator;
        this.lessonsDataSource.sort = this.sort;
        const filteredColumns = this.displayedColumns.filter(s => s != "actions");
        this.lessonsDataSource.filterPredicate = (data: any, filter: string): boolean => {
            return filteredColumns.some(attribute =>
                data[attribute].toString().toLowerCase().indexOf(filter) !== -1
            );
        }
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.lessonsDataSource.filter = filterValue.trim().toLowerCase();
        if (this.lessonsDataSource.paginator) {
            this.lessonsDataSource.paginator.firstPage();
        }
    }

    onAddLesson() {
        this.router.navigate([`/lesson/add`]);
    }

    onEditLesson(lessonId: number, event: any) {
        event.stopPropagation();
        this.router.navigate([`lesson/${lessonId}/edit`]);
    }

    async onDeleteLesson(id: number, event: any) {
        event.stopPropagation();
        const response = await this.popupService.openPopup("Delete lesson", ['Are you sure you want to delete this lesson?'],
            [{ text: 'Yes', action: ConfirmationPopupAction.Ok }, { text: 'Cancel', action: ConfirmationPopupAction.Cancel }]);
        if (response == ConfirmationPopupAction.Ok) {
            await this.lessonService.deleteLesson(id);
            await this.loadData();
        }
    }

    async loadData() {
        const lessons = await this.lessonService.getAllLessons();
        this.lessons = lessons.map(l => ({
            id: l.id,
            title: l.title,
            content: l.content,
            learningTime: l.learningTime,
            requiredPoints: l.requiredPoints,
            programmingLanguage: ProgrammingLanguageHelper.programmingLanguageToString(l.programmingLanguage),
            learningPathLinks: l.learningPathLinks,
            learningPaths: l.learningPaths.map(lp => ({
                id: lp.id,
                title: lp.title,
                description: lp.description,
                noLearningHours: lp.noLearningHours,
                learningLevel: this.getLearningLevel(lp.learningLevel),
                learningLevelIcon: this.getLearningLevelIcon(lp.noLearningHours)
            }))
        } as LessonVm));
        this.lessonsDataSource = new MatTableDataSource<LessonVm>(this.lessons);
    }

    onToggleExpandLesson(lesson: LessonVm, event: any) {
        this.expandedLesson = this.expandedLesson === lesson ? null : lesson;
        event.stopPropagation();
    }

    getLearningLevel(learningLevel: LearningLevelEnum): string {
        switch (learningLevel) {
            case LearningLevelEnum.Beginner:
                return 'Beginner';
            case LearningLevelEnum.Intermediate:
                return 'Intermediate';
            case LearningLevelEnum.Advanced:
                return 'Advanced';
            default:
                return '';
        }
    }

    getLearningLevelIcon(learningLevel: LearningLevelEnum): LearningLevelIcon {
        switch (learningLevel) {
            case LearningLevelEnum.Beginner:
                return { icon: 'rocket_launch', color: 'color-green' };
            case LearningLevelEnum.Intermediate:
                return { icon: 'rocket_launch', color: 'color-orange' };
            case LearningLevelEnum.Advanced:
                return { icon: 'rocket_launch', color: 'color-red' };
            default:
                return { icon: '', color: '' };;
        }
    }

    async onReload(event: any) {
        await this.loadData();
    }
}
