﻿using eLearningAPI.Enums;
using eLearningAPI.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eLearningAPI.Dtos
{
    public class LessonDto
    {
        public int Id { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }
        [Required]
        public int LearningTime { get; set; }
        [Required]
        public int RequiredPoints { get; set; }
        [Required]
        public ProgrammingLanguageEnum ProgrammingLanguage { get; set; }
        public List<LearningPathDto> LearningPaths { get; set; }
        public List<LearningPathLink> LearningPathLinks { get; set; }
        public Models.Quiz.Quiz Quiz { get; set; }
        public List<LessonDto> DependerLessons { get; set; }
        public List<LessonDto> DependeeLessons { get; set; }
    }
}
