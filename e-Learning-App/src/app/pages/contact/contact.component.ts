import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { fader } from 'src/app/animations/animation.component';
import { ContactForm } from 'src/app/models/contact/contact-form.model';
import { ContactModel } from 'src/app/models/contact/contact.model';
import { CustomFormGroup } from 'src/app/models/custom-form-group';
import { MailService } from 'src/app/services/mail.service';

@Component({
    selector: 'contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    animations: [fader]
})

export class ContactComponent implements OnInit {
    contactFormGroup: CustomFormGroup<ContactForm>;
    isInitialized: boolean = false;

    constructor(private mailService: MailService) { }

    ngOnInit(): void {
        this.contactFormGroup = new CustomFormGroup<ContactForm>({
            name: new FormControl('', [Validators.required]),
            subject: new FormControl('', [Validators.required]),
            emailAddress: new FormControl('', [Validators.required, Validators.email]),
            content: new FormControl('', [Validators.required, Validators.maxLength(600)])
        });
        setTimeout(() => {
            this.isInitialized = true;
        }, 10);
    }

    async onSubmit() {
        await this.mailService.sendContactMail(
            {
                senderName: this.contactFormGroup.getFormControl('name')?.value,
                senderAddress: this.contactFormGroup.getFormControl('emailAddress')?.value,
                subject: this.contactFormGroup.getFormControl('subject')?.value,
                content: this.contactFormGroup.getFormControl('content')?.value
            } as ContactModel);
    }
}
