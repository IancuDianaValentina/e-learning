export class UserAchievement {
    userId: string;
    achievementId: string;
    achievementName: string;
    achievementDescription: string;
    achievedAt: Date;
}
