import { Question } from "./question.model";

export class Quiz {
    id: number;
    title: string;
    questions: Question[];
}