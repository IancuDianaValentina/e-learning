﻿using Authentication.Attributes;
using eLearningAPI.APIServices.Interfaces;
using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Dtos.ApiRequestDto;
using eLearningAPI.Extensions;
using eLearningAPI.Models;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace eLearningAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserBL _userBL;
        private readonly IAuthenticationAPIService _authenticationAPIService;
        public UsersController(IUserRepository userRepository, IUserBL userBL, IAuthenticationAPIService authenticationAPIService)
        {
            _userRepository = userRepository;
            _userBL = userBL;
            _authenticationAPIService = authenticationAPIService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser([FromBody] UserDto user)
        {
            if (user == null)
            {
                return BadRequest("Invalid request");
            }
            if ((await _userRepository.GetUserByUsernameAsync(user.Username)) != null)
            {
                return BadRequest("An user with this username already exists");
            }
            await _userRepository.AddUserAsync(user.ToModel());
            var response = await _authenticationAPIService.AuthenticateUserAsync(user.Username, user.Password);
            return Ok(response);     
        }

        [HttpGet("GetUserPoints/{userId}")]
        [Authorize]
        public async Task<IActionResult> GetPointsForUser(Guid userId)
        {
            var response = await _userBL.GetUserPointsAsync(userId);
            return Ok(response);
        }

        [HttpGet("GetUsersForLeaderboard/{topNumber}")]
        public async Task<IActionResult> GetUsersForLeaderboard(int topNumber, [FromQuery] string startDate)
        {
            var response = await _userBL.GetUsersForLeaderboardAsync(startDate, topNumber);
            return Ok(response);
        }

        [HttpGet("GetUserById/{userId}")]
        [Authorize]
        public async Task<IActionResult> GetUserById(Guid userId)
        {
            var response = await _userBL.GetUserByIdAsync(userId);
            return Ok(response);
        }

        [HttpGet("GetUserSessions/{userId}/learningPath/{learningPathId}")]
        [Authorize]
        public async Task<IActionResult> GetUserSessionsByLearningPathId(int learningPathId, Guid userId)
        {
            var response = await _userBL.GetUserSessionsByLearningPathIdAsync(learningPathId, userId);
            return Ok(response);
        }

        [HttpPost("CreateSession")]
        public async Task<IActionResult> CreateUserSession([FromBody] CreateUserSessionRequest createSessionRequest)
        {
            var response = await _userBL.CreateUserSessionAsync(createSessionRequest.UserId, createSessionRequest.LearningPathId, 
                createSessionRequest.LessonId, createSessionRequest.RequiredPoints);
            return Ok(response);
        }

        [HttpGet("GetUserSession/{userId}/{lessonId}/{learningPathId}")]
        [Authorize]
        public async Task<IActionResult> GetUserSession(Guid userId, int lessonId, int learningPathId)
        {
            var response = await _userBL.GetUserSessionAsync(userId, lessonId, learningPathId);
            return Ok(response);
        }

        [HttpPost("ChangeUserSessionStatus")]
        [Authorize]
        public async Task<IActionResult> ChangeUserSessionStatus([FromBody] ChangeUserSessionStatusRequest changeUserSessionStatusRequest)
        {
            await _userBL.ChangeUserSessionStatusAsync(changeUserSessionStatusRequest.UserSessionId, changeUserSessionStatusRequest.Status);
            return Ok();
        }

        [HttpGet("GetAllUserAchievements/{userId}")]
        [Authorize]
        public async Task<IActionResult> GetAllUserAchievementsByUserId(Guid userId)
        {
            var response = await _userBL.GetAllUserAchievementByUserIdAsync(userId);
            return Ok(response);
        }
    } 
}
