﻿using System;
using static Authentication.Enums.Enums;

namespace eLearningAPI.Models
{
    public class User: BaseModel
    {
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int Points { get; set; }
        public int TotalAnswered { get; set; }
        public int TotalCorrectAnswered { get; set; }
        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpirationTime { get; set; }
        public UserRoleEnum RoleId {get; set;}
        public string RoleName { get; set; }
    }
}
