import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpParams
} from '@angular/common/http';
import { SessionService } from './session.service';
import { from, lastValueFrom, Observable } from 'rxjs';
import { Constants } from "../models/constants.model";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private sessionService: SessionService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(this.handle(request, next));
    }

    async handle(request: HttpRequest<any>, next: HttpHandler): Promise<any> {
        if (request.params.get(Constants.NoIntercept) === "true") {
            let params = new HttpParams({ fromString: request.params.delete(Constants.NoIntercept).toString() });
            return lastValueFrom(next.handle(
                request.clone({ params: params })
            ));
        }
        let session = await this.sessionService.getUserSession();
        const req = request.clone({
            setHeaders: {
                Authorization: session?.jwtToken ?? ''
            }
        });
        return lastValueFrom(next.handle(req));
    }
}
