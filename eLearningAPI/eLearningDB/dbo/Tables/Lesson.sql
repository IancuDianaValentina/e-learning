﻿CREATE TABLE [dbo].[Lesson] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [Title]                 NVARCHAR (150) NOT NULL,
    [HtmlContent]           NVARCHAR (MAX) NOT NULL,
    [ProgrammingLanguageId] TINYINT        NOT NULL,
    [LearningTime]          TINYINT        NOT NULL,
    [RequiredPoints]        TINYINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([ProgrammingLanguageId]) REFERENCES [dbo].[ProgrammingLanguage] ([Id])
);

