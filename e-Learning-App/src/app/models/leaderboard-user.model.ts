export class LeaderboardUser {
    id: string;
    userName: string;
    correctAnswers: number;
    userPoints: number;
}