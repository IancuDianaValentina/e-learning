﻿using Authentication.Dtos;
using System.Threading.Tasks;

namespace Authentication.Services.Interfaces
{
    public interface IAuthenticationService
    {
        public Task<AuthenticationResponse> GenerateAuthenticationResponse(AuthenticationRequest authReq);
        public Task<AuthenticationResponse> RefreshJwtToken(string refreshToken);
    }
}
