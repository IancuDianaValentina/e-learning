import { Injectable } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { lastValueFrom } from "rxjs";
import { ActionButton, ConfirmationPopupAction, CustomPopupComponent, FieldConfig } from "../components/custom-popup/custom-popup.component";

@Injectable({
    providedIn: 'root'
})
export class CustomPopupService {

    constructor(private dialog: MatDialog) { }

    dialogRef: MatDialogRef<any>;

    async openPopup(title: string, texts: string[], actionButtons: ActionButton[], fields?: FieldConfig[]): Promise<ConfirmationPopupAction> {
        this.dialogRef = this.dialog.open(CustomPopupComponent, {
            data: {
                title: title,
                texts: texts,
                actionButtons: actionButtons,
                fields: fields,
                onActionPerformed: this.onActionPerformed.bind(this)
            },
            width: '500px'
        });

        let response = await lastValueFrom(this.dialogRef.afterClosed());

        return response ?? ConfirmationPopupAction.Cancel;
    }

    onActionPerformed(action: ConfirmationPopupAction) {
        if (this.dialogRef) {
            this.dialogRef.close(action);
        }
    }
}