﻿CREATE TABLE [dbo].[QuestionAchievement] (
    [Id]               TINYINT IDENTITY (1, 1) NOT NULL,
    [AchievementId]    TINYINT NOT NULL,
    [NrCorrectAnswers] INT     NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([AchievementId]) REFERENCES [dbo].[Achievement] ([Id])
);

