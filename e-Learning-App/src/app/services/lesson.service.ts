import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { lastValueFrom } from "rxjs";
import { Lesson } from "../models/lesson/lesson.model";
import { LessonLearningPath } from "../models/lesson/lesson-learning-path.model";

@Injectable({
    providedIn: 'root'
})
export class LessonService {

    private baseUrl: string = '';

    constructor(private configService: ConfigService, private httpClient: HttpClient) {
        this.baseUrl = this.configService.getUrl("baseApiUrl");
    }

    deleteLesson(lessonId: number): Promise<void> {
        return lastValueFrom(this.httpClient.delete<void>(this.baseUrl + `/Lessons/DeleteLesson/${lessonId}`));
    }

    getAllLessons(): Promise<Lesson[]> {
        return lastValueFrom(this.httpClient.get<Lesson[]>(this.baseUrl + '/Lessons/GetAllLessons'));
    }

    getLessonWithQuizById(lessonId: number): Promise<Lesson> {
        return lastValueFrom(this.httpClient.get<Lesson>(this.baseUrl + `/Lessons/GetLessonWithQuizById/${lessonId}`));
    }

    getLessonLearningPath(lessonId: number, learningPathId: number): Promise<LessonLearningPath> {
        return lastValueFrom(this.httpClient.get<LessonLearningPath>(this.baseUrl + `/Lessons/GetLearningPathLesson/lesson/${lessonId}/learningPathId/${learningPathId}`));
    }

    saveLesson(lesson: Lesson): Promise<string> {
        return lastValueFrom(this.httpClient.post<string>(this.baseUrl + '/Lessons/SaveLesson', lesson));
    }
}