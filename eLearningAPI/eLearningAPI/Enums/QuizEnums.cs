﻿namespace eLearningAPI.Enums
{
    public enum QuestionTypeEnum
    {
        SingleChoice = 1,
        MultipleChoice = 2,
        WrittenAnswer = 3
    }
}
