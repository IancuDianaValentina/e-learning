import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { CustomFormGroup } from 'src/app/models/custom-form-group';
import { AuthenticationRequest } from 'src/app/models/authentication-request.model';
import { SessionService } from 'src/app/services/session.service';
import { CookieService } from 'ngx-cookie-service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToolbarService } from 'src/app/services/toolbar.service';

@Component({
  selector: 'authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})

export class AuthenticationComponent {
  authReq: AuthenticationRequest = new AuthenticationRequest;
  formGroup: CustomFormGroup<AuthenticationRequest>;
  hide: boolean = true;
  errorMessage: string = '';
  showApiError: boolean = false;
  authenticationSubscription: Subscription = new Subscription;

  constructor(private sessionService: SessionService, private cookieService: CookieService, private router: Router,
    private activatedRoute: ActivatedRoute, private toolbarService: ToolbarService) {
    this.formGroup = new CustomFormGroup<AuthenticationRequest>({
      username: new FormControl(this.authReq.username,
        [Validators.required]
      ),
      password: new FormControl(this.authReq.password,
        [Validators.required]
      )
    });
  }

  async onAuthenticate() {
    this.authReq = this.formGroup.getValue();
    if (!this.formGroup.invalid) {
      this.sessionService.authenticateUser(this.authReq).subscribe({
        next: async (response) => {
          this.onSuccessfulAuthentication(response);
          this.toolbarService.setToolbarItems();
        },
        error: (err) => {
          this.errorMessage = err;
          this.showApiError = true;
        }
      });
    } else {
      this.formGroup.markAllAsTouched();
    }
  }

  onSuccessfulAuthentication(response: any) {
    this.sessionService.setUserSession(response);
    console.log(response.jwtRefreshToken);
    this.cookieService.delete('refreshToken', response.jwtRefreshToken);
    this.cookieService.set('refreshToken', response.jwtRefreshToken);
    this.showApiError = false;
    this.activatedRoute.queryParamMap.subscribe(params => {
      const returnUrl = params.get('returnUrl');
      this.router.navigateByUrl(returnUrl || '/learning-paths');
    });
  }

  onGoToRegister() {
    this.router.navigateByUrl('/register');
  }

  ngOnDestroy() {
    this.authenticationSubscription.unsubscribe();
  }
}
