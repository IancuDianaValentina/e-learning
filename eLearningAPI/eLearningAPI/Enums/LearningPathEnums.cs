﻿namespace eLearningAPI.Enums
{
    public enum ProgrammingLanguageEnum
    {
        CSharp = 1,
        Java = 2,
        Javascript = 3
    }

    public enum LearningLevelEnum
    {
        Beginner = 1,
        Intermediate = 2,
        Advanced = 3
    }
}
