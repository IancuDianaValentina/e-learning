﻿namespace Authentication.Dtos
{
    public class RefreshTokenRequest
    {
        public string JwtRefreshToken { get; set; }
    }
}
