﻿
CREATE PROCEDURE [dbo].[LessonLearningPath_Get]
(
	@LessonId INT,
	@LearningPathId INT
)
AS
BEGIN
	DECLARE @LessonIndex INT;
	SELECT @LessonIndex = LessonIndex 
							FROM LessonLearningPath
							WHERE LessonId = @LessonId AND
								LearningPathId = @LearningPathId;

	SELECT L.Id As [LessonId],
		L.Title,
		L.HtmlContent as [Content],
		L.LearningTime,
		L.RequiredPoints,
		L.ProgrammingLanguageId AS [ProgrammingLanguage],
		LLP.LessonIndex,
		LP.Id as [LearningPathId],
		LP.Title as [LearningPathTitle],
		NextLesson.*
	FROM Lesson L
	INNER JOIN LessonLearningPath LLP
		ON LLP.LessonId = L.Id
	INNER JOIN LearningPath LP
		ON LLP.LearningPathId = LP.Id
	LEFT JOIN (
			SELECT NextLesson.Id,
				NextLesson.Title,
				NextLesson.HtmlContent as [Content],
				NextLesson.LearningTime,
				NextLesson.RequiredPoints,
				NextLesson.ProgrammingLanguageId as ProgrammingLanguage,
				LessonIndex
			FROM Lesson NextLesson
			INNER JOIN LessonLearningPath
				ON LessonLearningPath.LessonId = NextLesson.Id
			WHERE LessonLearningPath.LearningPathId = @LearningPathId
		) NextLesson
		ON NextLesson.LessonIndex = @LessonIndex + 1
	WHERE L.Id = @LessonId AND
		LLP.LearningPathId = @LearningPathId;
END