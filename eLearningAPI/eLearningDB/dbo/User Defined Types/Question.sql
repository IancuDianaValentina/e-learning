﻿CREATE TYPE [dbo].[Question] AS TABLE (
    [Id]           UNIQUEIDENTIFIER NULL,
    [QuizId]       INT              NULL,
    [Type]         TINYINT          NULL,
    [Text]         NVARCHAR (MAX)   NULL,
    [RewardPoints] INT              NULL);

