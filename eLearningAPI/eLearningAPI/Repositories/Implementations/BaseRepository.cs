﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace eLearningAPI.Repositories.Implementations
{
    public class BaseRepository
    {
        private readonly IConfiguration _configuration;
        protected IDbConnection Connection => new SqlConnection(_configuration.GetConnectionString("DefaultConnection"));
        public BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
    }
}
