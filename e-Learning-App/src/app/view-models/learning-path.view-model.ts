export class LearningPathVm {
    id: number;
    title: string;
    noLearningHours: number;
    description: string;
    programmingLanguage: string;
    learningLevel: string;
    learningLevelIcon: LearningLevelIcon;
}

export class LearningLevelIcon {
    icon: string;
    color: string;
}