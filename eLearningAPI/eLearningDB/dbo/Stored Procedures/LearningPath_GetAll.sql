﻿CREATE PROCEDURE [dbo].[LearningPath_GetAll]
AS
BEGIN
	SELECT [Id],
		[Title],
		[NoLearningHours],
		[Description],
		[ProgrammingLanguageId] AS ProgrammingLanguage,
		[LearningLevelId] AS LearningLevel
	FROM [dbo].[LearningPath]
END