﻿using eLearningAPI.Models;

namespace eLearningAPI.Extensions
{
    public static class UserExtension
    {
        public static User ToModel(this UserDto dto)
        {
            return new User
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                EmailAddress = dto.EmailAddress,
                Address = dto.Address,
                Username = dto.Username,
                PasswordHash = BCrypt.Net.BCrypt.HashPassword(dto.Password),
                PhoneNumber = dto.PhoneNumber,
                RoleId = (Authentication.Enums.Enums.UserRoleEnum)(dto.RoleId != null ? dto.RoleId : Authentication.Enums.Enums.UserRoleEnum.User)
            };
        }
    }
}
