import { AbstractControl, AbstractControlOptions, AsyncValidatorFn, FormControl, FormGroup, ValidatorFn } from "@angular/forms"

export class CustomFormGroup<T> extends FormGroup {

    constructor(controls: { [key: string]: AbstractControl; }, validatorOrOpts?: ValidatorFn | AbstractControlOptions | ValidatorFn[],
        asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]) {
        super(controls, validatorOrOpts, asyncValidator);
    }

    public getValue(): T {
        return Object.assign({}, this.getRawValue())
    }

    public override setValue(value: T, options?: {
        onlySelf?: boolean;
        emitEvent?: boolean;
        emitModelToViewChange?: boolean;
        emitViewToModelChange?: boolean;
    }): void {
        this.patchValue(value, options);
    }

    getFormControl(key: string): FormControl {
        return this.controls[key] as FormControl;
    }
}