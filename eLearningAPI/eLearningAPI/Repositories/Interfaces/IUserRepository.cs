﻿using eLearningAPI.Enums;
using eLearningAPI.Models;
using eLearningAPI.Models.UserSession;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public Task<User> GetUserByUsernameAsync(string username);
        public Task<User> GetUserByIdAsync(Guid userId);
        public Task AddUserAsync(User user);
        public Task<int> GetUserPointsAsync(Guid userId);
        public Task<List<LeaderboardUser>> GetUsersForLeaderboardAsync(DateTime startDate, int topNumber);
        public Task<List<UserLessonSession>> GetUserSessionsByLearningPathIdAsync(int learningPathId, Guid userId);
        public Task<Guid> CreateUserSessionAsync(Guid userId, int learningPathId, int lessonId, int requiredPoints);
        public Task<UserLessonSession> GetUserSessionAsync(Guid userId, int lessonId, int learningPathId);
        public Task ChangeUserSessionStatusAsync(Guid userSessionId, UserSessionStatusEnum status);
        public Task<List<UserAchievement>> GetAllUserAchievementByUserIdAsync(Guid userId);
    }
}
