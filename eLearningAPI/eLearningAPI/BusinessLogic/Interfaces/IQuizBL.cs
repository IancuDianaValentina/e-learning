﻿using eLearningAPI.Dtos.UserSesssion;
using eLearningAPI.Enums;
using eLearningAPI.Models.Quiz;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Interfaces
{
    public interface IQuizBL
    {
        public Task<Quiz> GetQuizByLessonIdAsync(int lessonId);
        public Task<bool> SaveUserAnswerAsync(UserAnswerDto answer, QuestionTypeEnum questionType, string writtenAnswer);
        public Task<bool> IsAnswerCorrectAsync(UserAnswerDto answer, QuestionTypeEnum questionType, string answerText = "");
    }
}
