﻿using System;
using System.Collections.Generic;
using System.Data;

namespace eLearningAPI.Utils
{
    public class RepositoryUtils
    {
        public static DataTable ToIntList(List<int> numbers)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(int));
            if (numbers != null)
            {
                foreach (int number in numbers)
                {
                    var row = dataTable.NewRow();
                    row["Id"] = number;
                    dataTable.Rows.Add(row);
                }
            }
            return dataTable;
        }

        public static DataTable ToGuidList(List<Guid> ids)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(Guid));
            if (ids != null)
            {
                foreach (Guid id in ids)
                {
                    var row = dataTable.NewRow();
                    row["Id"] = id;
                    dataTable.Rows.Add(row);
                }
            }
            return dataTable;
        }
    }
}
