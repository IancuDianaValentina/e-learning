﻿CREATE TYPE [dbo].[IntMap] AS TABLE (
    [Id]    INT NULL,
    [Value] INT NULL);

