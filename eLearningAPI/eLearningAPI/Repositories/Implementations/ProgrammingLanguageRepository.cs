﻿using Dapper;
using eLearningAPI.Models;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Implementations
{
    public class ProgrammingLanguageRepository: BaseRepository, IProgrammingLanguageRepository
    {
        private readonly string GetAllProgrammingLanguagesSP = "ProgrammingLanguage_GetAll";
        public ProgrammingLanguageRepository(IConfiguration configuration): base(configuration) { }

        public async Task<List<ProgrammingLanguage>> GetAllProgrammingLanguagesAsync()
        {
            using (var connection = Connection)
            {
                return (await connection.QueryAsync<ProgrammingLanguage>(GetAllProgrammingLanguagesSP,
                    commandType: System.Data.CommandType.StoredProcedure)) as List<ProgrammingLanguage>;
            }
        }
    }
}
