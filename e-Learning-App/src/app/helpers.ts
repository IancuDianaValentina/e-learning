import { LearningLevelEnum, ProgrammingLanguageEnum } from "./enums";
import { LearningLevelIcon } from "./view-models/learning-path.view-model";

export class ProgrammingLanguageHelper {
    public static programmingLanguageToString(programmingLanguage: ProgrammingLanguageEnum) {
        switch (programmingLanguage) {
            case ProgrammingLanguageEnum.CSharp:
                return 'C#';
            case ProgrammingLanguageEnum.Java:
                return 'Java';
            case ProgrammingLanguageEnum.JavaScript:
                return 'JavaScript';
            default:
                return '';
        }
    }
}

export class LearningPathHelper {
    static getProgrammingLanguage(programmingLanguageEnum: ProgrammingLanguageEnum): string {
        switch (programmingLanguageEnum) {
            case ProgrammingLanguageEnum.CSharp:
                return 'C#';
            case ProgrammingLanguageEnum.Java:
                return 'Java';
            case ProgrammingLanguageEnum.JavaScript:
                return 'JavaScript';
            default:
                return '';
        }
    }

    static getLearningLevel(learningLevel: LearningLevelEnum): string {
        switch (learningLevel) {
            case LearningLevelEnum.Beginner:
                return 'Beginner';
            case LearningLevelEnum.Intermediate:
                return 'Intermediate';
            case LearningLevelEnum.Advanced:
                return 'Advanced';
            default:
                return '';
        }
    }

    static getLearningLevelIcon(learningLevel: LearningLevelEnum): LearningLevelIcon {
        switch (learningLevel) {
            case LearningLevelEnum.Beginner:
                return { icon: 'rocket_launch', color: 'color-green' };
            case LearningLevelEnum.Intermediate:
                return { icon: 'rocket_launch', color: 'color-orange' };
            case LearningLevelEnum.Advanced:
                return { icon: 'rocket_launch', color: 'color-red' };
            default:
                return { icon: '', color: '' };;
        }
    }

}