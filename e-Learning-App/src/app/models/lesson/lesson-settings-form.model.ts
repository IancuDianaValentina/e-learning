import { ProgrammingLanguageEnum } from "src/app/enums";
import { LearningPathLink } from "../learning-path/learning-path-link.model";

export class LessonSettingsFormModel {
    title: string;
    programmingLanguage: ProgrammingLanguageEnum;
    learningTime: number;
    isLearningTimeInMinutes: boolean;
    requiredPoints: number;
    learningPathLinks: Array<LearningPathLink>;
}