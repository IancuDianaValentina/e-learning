﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MailManagement.Models;
using Microsoft.Extensions.Options;
using MimeKit;
using System.IO;
using System.Threading.Tasks;

namespace MailManagement.Services.Implementations
{
    public class MailService: IMailService
    {
        private readonly MailSettings _mailSettings;
        public MailService(IOptionsMonitor<MailSettings> options)
        {
            _mailSettings = options.CurrentValue;
        }
        public async Task SendContactMailAsync(ContactMailRequest mailRequest)
        {
            var email = new MimeMessage { 
                Sender = MailboxAddress.Parse(mailRequest.SenderName),
                Subject = mailRequest.Subject
            };
            email.To.Add(MailboxAddress.Parse(_mailSettings.ToEmail));
            email.From.Add(MailboxAddress.Parse(mailRequest.SenderAddress));

            var builder = new BodyBuilder();
            if (mailRequest.Attachments != null)
            {
                byte[] fileBytes;
                foreach (var file in mailRequest.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }
                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }
            builder.HtmlBody = mailRequest.Content;
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(_mailSettings.ToEmail, _mailSettings.Password);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }
    }
}
