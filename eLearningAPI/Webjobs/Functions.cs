﻿using Dapper;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Webjobs
{
    public class Functions
    {
        private static readonly string AddPointsToUserSP = "Users_AddPoints";
        private readonly IConfiguration _configuration;
        protected IDbConnection Connection => new SqlConnection(_configuration.GetConnectionString("DefaultConnection"));
        public Functions(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task AddPoints([TimerTrigger("0 30 9 * * *", RunOnStartup = false)] TimerInfo timer, ILogger logger)
        {
            if (timer.IsPastDue)
            {
                logger.LogInformation("Timer is running late!");
            }
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(AddPointsToUserSP,
                    param: new
                    {
                        Points = 15
                    },
                   commandType: System.Data.CommandType.StoredProcedure);
            }
            logger.LogInformation($"Timer trigger function executed at: {DateTime.Now}");
        }
    }
}
