import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { lastValueFrom } from "rxjs";
import { LearningPath } from "../models/learning-path/learning-path.model";
import { Constants } from "../models/constants.model";

@Injectable({
    providedIn: 'root'
})
export class LearningPathService {

    private baseUrl: string = '';
    private noInterceptParams = {
        [Constants.NoIntercept]: "true"
    };

    constructor(private configService: ConfigService, private httpClient: HttpClient) {
        this.baseUrl = this.configService.getUrl("baseApiUrl");
    }

    getAllLearningPaths(): Promise<Array<LearningPath>> {
        return lastValueFrom(this.httpClient.get<Array<LearningPath>>(this.baseUrl + '/LearningPath/getAllLearningPaths', { params: this.noInterceptParams }));
    }

    getLearningPath(learningPathId: number): Promise<LearningPath> {
        return lastValueFrom(this.httpClient.get<LearningPath>(this.baseUrl + `/LearningPath/GetLearningPath/${learningPathId}`));
    }

    getLearningPathsByProgrammingLanguage(programmingLanguageId: number): Promise<Array<LearningPath>> {
        return lastValueFrom(this.httpClient.get<Array<LearningPath>>(this.baseUrl + `/LearningPath/getByProgrammingLanguage/${programmingLanguageId}`));
    }

    saveLearningPath(learningPath: LearningPath): Promise<number> {
        return lastValueFrom(this.httpClient.post<number>(this.baseUrl + '/LearningPath/SaveLearningPath', learningPath));
    }

    unlinkLessonFromLearningPath(lessonId: number, learningPathId: number): Promise<void> {
        return lastValueFrom(this.httpClient.post<void>(this.baseUrl + '/LearningPath/UnlinkLessonFromLearningPath', { lessonId: lessonId, learningPathId: learningPathId }));
    }
}