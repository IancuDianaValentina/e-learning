import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomFormGroup } from 'src/app/models/custom-form-group';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';
import { UserSession } from './../../models/user-session.model';

@Component({
    selector: 'registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})

export class RegistrationComponent {
    user: User = new User;
    formGroup: CustomFormGroup<User>;
    hide: boolean = true;
    errorMessage: string = '';
    showApiError: boolean = false;
    passwordMinLength: number = 8;

    constructor(private userService: UserService, private router: Router, private sessionService: SessionService) {
        this.formGroup = new CustomFormGroup<User>({
            firstName: new FormControl(this.user.firstName,
                [Validators.required]
            ),
            lastName: new FormControl(this.user.lastName,
                [Validators.required]
            ),
            username: new FormControl(this.user.username,
                [Validators.required]
            ),
            emailAddress: new FormControl(this.user.emailAddress,
                [
                    Validators.required,
                    Validators.email
                ]
            ),
            phoneNumber: new FormControl(this.user.phoneNumber,
                [
                    Validators.required
                ]
            ),
            address: new FormControl(this.user.address,
                [Validators.required]
            ),
            password: new FormControl(this.user.password,
                [
                    Validators.required,
                    Validators.minLength(this.passwordMinLength)
                ]
            )
        });
    }

    onRegister() {
        this.user = this.formGroup.getValue();
        if (this.formGroup.valid) {
            this.userService.registerUser(this.user).subscribe({
                next: (response: UserSession) => {
                    this.sessionService.setUserSession(response);
                    this.router.navigate(['/']);
                },
                error: (err: string) => {
                    this.errorMessage = err;
                    this.showApiError = true;
                }
            });
        } else {
            this.formGroup.markAllAsTouched();
        }
    }
}
