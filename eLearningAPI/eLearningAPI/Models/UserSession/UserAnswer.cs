﻿using System;
using System.Collections.Generic;

namespace eLearningAPI.Models.UserSession
{
    public class UserAnswer
    {
        public Guid Id { get; set; }
        public Guid UserSessionId { get; set; }
        public Guid QuestionId { get; set; }
        public bool CorrectlyAnswered { get; set; }
        public List<Guid> ChoiceIds { get; set; }
    }
}
