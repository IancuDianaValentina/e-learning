﻿using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Dtos;
using eLearningAPI.Enums;
using eLearningAPI.Extensions;
using eLearningAPI.Models;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Implementations
{
    public class LearningPathBL: ILearningPathBL
    {
        private readonly ILearningPathRepository _learningPathRepository;
        private readonly ILogger<LearningPathBL> _logger;
        private readonly ILessonRepository _lessonRepository;
        public LearningPathBL(ILearningPathRepository learningPathRepository, ILessonRepository lessonRepository, ILogger<LearningPathBL> logger)
        {
            _learningPathRepository = learningPathRepository;
            _lessonRepository = lessonRepository;
            _logger = logger;
        }

        public async Task<List<LearningPathDto>> GetAllLearningPathsAsync()
        {
            _logger.LogInformation("Getting all learning paths");
            try
            {
                var models = await _learningPathRepository.GetAllLearningPathsAsync();
                _logger.LogInformation("Successfully retrieved learning paths.");
                return models?.Select(model => model.ToDto())?.ToList();
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving learning paths.");
                throw new Exception("Error retrieving learning paths.");
            }
        }

        public async Task<LearningPathDto> GetLearningPathAsync(int learningPathId)
        {
            try
            {
                var model = await _learningPathRepository.GetLearningPathAsync(learningPathId);
                _logger.LogInformation("Successfully retrieved learning path.");
                return model.ToDto();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving learning path.");
                throw new Exception("Error retrieving learning path.");
            }
        }

        public async Task<List<LearningPathDto>> GetByProgrammingLanguageAsync(ProgrammingLanguageEnum programmingLanguage)
        {
            _logger.LogInformation("Retrieving learning paths by programming language.");
            try
            {
                var models = await _learningPathRepository.GetByProgrammingLanguageAsync(programmingLanguage);
                _logger.LogInformation("Successfully retrieved learning pahts ny programming language.");
                return models?.Select(model => model.ToDto())?.ToList();
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error getting learning paths by programming language.");
                throw new Exception("Error getting learning paths by programming language.");
            }
        }

        public async Task<int> SaveLearningPathAsync(LearningPathDto learningPath)
        {
            _logger.LogInformation("Adding learning path.");
            try
            {
                var learningPathId = await _learningPathRepository.SaveLearningPathAsync(learningPath.ToModel());
                _logger.LogInformation("Successfully added learning path.");
                return learningPathId;
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error adding learning path.");
                throw new Exception("Error adding learning path.");
            }
        }

        public async Task UnlinkLessonFromLearningPathAsync(int lessonId, int learningPathId)
        {
            _logger.LogInformation("Unlinking lesson {0} from learning path {1}.", lessonId, learningPathId);
            try
            {
                await _learningPathRepository.UnlinkLessonFromLearningPathAsync(lessonId, learningPathId);
                _logger.LogInformation("Successfully unlinked lesson {0} from learning path {1}.", lessonId, learningPathId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error unlinking lesson {0} from learning path {1}.", lessonId, learningPathId);
                throw new Exception("Error unlinking lesson from learning path.");
            }
        }
    }
}
