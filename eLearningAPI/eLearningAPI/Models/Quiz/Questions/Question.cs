﻿using eLearningAPI.Enums;
using System;
using System.Collections.Generic;

namespace eLearningAPI.Models.Quiz
{
    public class Question
    {
        public Guid Id { get; set; }
        public QuestionTypeEnum QuestionType { get; set; }
        public string Text { get; set; }
        public int RewardPoints { get; set; }
        public List<Choice> Choices { get; set; }
    }
}
