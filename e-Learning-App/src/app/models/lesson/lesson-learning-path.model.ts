import { ProgrammingLanguageEnum } from "src/app/enums";
import { Lesson } from "./lesson.model";

export class LessonLearningPath {
    lessonId: number;
    title: string;
    content: string;
    learningTime: number;
    requiredPoints: number;
    programmingLanguage: ProgrammingLanguageEnum;
    lessonIndex: number;
    learningPathId: number;
    learningPathTitle: string;
    nextLesson: Lesson;
}