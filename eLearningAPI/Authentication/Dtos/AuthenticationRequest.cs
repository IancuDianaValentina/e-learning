﻿using System.ComponentModel.DataAnnotations;

namespace Authentication.Dtos
{
    public class AuthenticationRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
