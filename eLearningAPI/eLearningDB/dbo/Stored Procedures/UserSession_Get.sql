﻿CREATE PROCEDURE [dbo].[UserSession_Get]
	@UserId UNIQUEIDENTIFIER,
	@LessonId INT,
	@LearningPathId INT
AS
BEGIN
	SELECT UL.Id,
		UL.UserId,
		UL.LessonId,
		UL.LearningPathId,
		UL.[Status],
		Quiz.Id as [QuizId],
		UA.Id,
		UL.Id as [UserSessionId],
		UA.QuestionId,
		UA.CorrectlyAnswered,
		AC.ChoiceId
	FROM UserLesson UL
	INNER JOIN Quiz
		ON Quiz.LessonId = UL.LessonId
	INNER JOIN Question Q
		ON Q.QuizId = Quiz.Id
	LEFT JOIN UserSessionAnswer UA
		ON UA.UserSessionId = UL.Id
	LEFT JOIN AnswerChoice AC
		ON AC.AnswerId = UA.Id
	WHERE UL.UserId = @UserId AND
		UL.LessonId = @LessonId AND
		UL.LearningPathId = @LearningPathId;
END