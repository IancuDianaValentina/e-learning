import { Component, OnInit } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { QuestionTypeEnum, UserSessionStatusEnum } from "src/app/enums";
import { Choice } from "src/app/models/quiz/choice.model";
import { Question } from "src/app/models/quiz/question.model";
import { Quiz } from "src/app/models/quiz/quiz.model";
import { UserAnswer } from "src/app/models/user-sessions/user-answer.model";
import { UserLessonSession } from "src/app/models/user-sessions/user-lesson-lession.component";
import { QuizService } from "src/app/services/quiz.service";
import { SessionService } from "src/app/services/session.service";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'user-quiz',
    templateUrl: './user-quiz.component.html',
    styleUrls: ['./user-quiz.component.scss']
})

export class UserQuizComponent implements OnInit {
    quiz: Quiz;
    currentQuestion: Question = new Question;
    currentQuestionIndex: number = 0;
    questionTypeEnum = QuestionTypeEnum;
    userId: string;
    tooltipInfo: string;
    currentAnswers: any;
    answersFormControl: FormControl = new FormControl('', [Validators.required]);
    userSession: UserLessonSession;
    percentComplete: number;
    error: boolean = false;

    constructor(private webSessionService: SessionService,
        private activeRoute: ActivatedRoute, private router: Router,
        private userService: UserService, private quizService: QuizService, private route: ActivatedRoute) { }

    async ngOnInit() {
        this.tooltipInfo = "If you answer a question correctly from the first try, you will gain the corresponding points, else you will gain nothing for that question.";
        this.userId = (await this.webSessionService.getUserSession())?.id;

        // for test only
        //this.userId = 'B675C58D-2ADD-41AB-BE69-952B2E40B03F';

        const lessonId = parseInt(this.activeRoute.snapshot.paramMap.get("lessonId") ?? '0');
        const learningPathId = parseInt(this.activeRoute.snapshot.paramMap.get("learningPathId") ?? '0');
        this.quiz = await this.quizService.getQuizByLessonId(lessonId);
        this.percentComplete = 100 / this.quiz.questions.length;
        this.currentQuestion = this.quiz.questions?.[0];
        this.userSession = await this.userService.getUserSession(this.userId, lessonId, learningPathId);
        if (!this.userSession?.userAnswers) {
            this.userSession.userAnswers = [];
        }
    }

    onClickPrev() {
        this.currentQuestionIndex--;
        this.changeQuestion();
        this.answersFormControl.setValue(this.currentQuestion.questionType == QuestionTypeEnum.WrittenAnswer ?
            this.currentQuestion.choices[0].text : this.currentQuestion.choices.map(c => c.id));
    }

    async onClickNext() {
        const userAnswer = await this.checkIfCorrect(this.hasAnswer());
        if (!this.hasAnswer()) {
            this.userSession.userAnswers.push(userAnswer);
        }
        this.error = userAnswer.correctlyAnswered ? false : true;
        if (userAnswer.correctlyAnswered) {
            this.currentQuestionIndex++;
            this.changeQuestion();
        }
    }

    changeQuestion() {
        this.error = false;
        this.answersFormControl.reset();
        this.currentQuestion = this.quiz.questions?.[this.currentQuestionIndex];
        this.percentComplete = ((this.currentQuestionIndex + 1) * 100) / this.quiz.questions.length;
    }

    async onFinish() {
        const userAnswer = await this.checkIfCorrect(this.hasAnswer());
        this.error = userAnswer.correctlyAnswered ? false : true;
        await this.userService.changeUserSessionStatus(this.userSession.id, UserSessionStatusEnum.Completed);
        if (userAnswer.correctlyAnswered) {
            this.router.navigate(['../../../view'], { relativeTo: this.route });
        }
    }

    async checkIfCorrect(isAnswerSaved: boolean) {
        const writtenAnswer = this.currentQuestion.questionType == QuestionTypeEnum.WrittenAnswer ? this.answersFormControl?.value : '';
        const choiceIds = this.currentQuestion.questionType == QuestionTypeEnum.WrittenAnswer ? this.currentQuestion.choices.map(c => c.id) :
            (this.currentQuestion.questionType == QuestionTypeEnum.MultipleChoice ? this.answersFormControl?.value : [this.answersFormControl?.value]);
        const userAnswer: UserAnswer = {
            userSessionId: this.userSession.id,
            questionId: this.currentQuestion.id,
            choiceIds: choiceIds
        } as UserAnswer;
        userAnswer.correctlyAnswered = !isAnswerSaved ?
            await this.quizService.saveUserAnswer(userAnswer as UserAnswer, this.currentQuestion.questionType, writtenAnswer)
            :
            await this.quizService.checkIfCorrectAnswer(userAnswer as UserAnswer, this.currentQuestion.questionType, writtenAnswer);
        return userAnswer;
    }

    hasAnswer() {
        return this.userSession?.userAnswers?.find(a => a.questionId == this.currentQuestion.id) ? true : false;
    }

    hasCorrectAnswer() {
        var answer = this.userSession?.userAnswers?.find(a => a.questionId == this.currentQuestion.id)
        return answer && answer.correctlyAnswered ? true : false;
    }

    hasIncorrectAnswer() {
        var answer = this.userSession?.userAnswers?.find(a => a.questionId == this.currentQuestion.id)
        return answer && !answer.correctlyAnswered ? true : false;
    }

    onCheckboxStateChange(event: any, choice: Choice) {
        let selectedChoices = this.answersFormControl?.value != '' && this.answersFormControl?.value != undefined && this.answersFormControl?.value != null
            ? this.answersFormControl?.value : [];
        if (event.checked) {
            selectedChoices.push(choice.id);
        } else {
            selectedChoices = selectedChoices.filter(c => c != choice.id);
        }
        this.answersFormControl.setValue(selectedChoices);
    }
}
