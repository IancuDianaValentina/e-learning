﻿
CREATE PROCEDURE [dbo].[Lesson_Delete]
(
	@Id INT
)
AS
BEGIN
	UPDATE LessonLearningPath
	SET LessonIndex = LessonIndex - 1
	FROM LessonLearningPath LLP
	WHERE LLP.LearningPathId IN (SELECT LearningPathId
								 FROM LessonLearningPath 
								 WHERE LessonId = @Id)
	AND LLP.LessonIndex > (SELECT LessonIndex
							FROM LessonLearningPath
							WHERE LessonId = @Id AND
								LearningPathId = LLP.LearningPathId);

	UPDATE LearningPath
	SET NoLearningHours = NoLearningHours - L.LearningTime
	FROM LearningPath LP
	INNER JOIN LessonLearningPath LLP
		ON LLP.LearningPathId = LP.Id
	INNER JOIN Lesson L
		ON L.Id = LLP.LessonId
	WHERE L.Id = @Id;

	DELETE FROM LessonLearningPath
	WHERE LessonId = @Id;

	DELETE FROM Quiz WHERE LessonId = @Id;
	DELETE FROM Lesson WHERE Id = @Id;
END