﻿
CREATE PROCEDURE [dbo].[User_GetByUsername] (
	@Username NVARCHAR(50)
)
AS
BEGIN
	SELECT TOP 1 u.*, r.Id as RoleId, r.Name as RoleName
	FROM [dbo].[Users] AS u
	INNER JOIN Roles r ON u.RoleId = r.Id
	WHERE u.Username = @Username;
END