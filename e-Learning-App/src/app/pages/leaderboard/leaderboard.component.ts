import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { LeaderboardUser } from "src/app/models/leaderboard-user.model";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'leaderboard',
    templateUrl: './leaderboard.component.html',
    styleUrls: ['./leaderboard.component.scss']
})

export class LeaderboardComponent implements OnInit {

    displayedColumns: string[] = [
        "rank",
        "userName",
        "userPoints",
        "correctAnswers"
    ];
    users: Array<LeaderboardUser> = [];
    usersDataSource: MatTableDataSource<LeaderboardUser> = new MatTableDataSource();
    constructor(private userService: UserService) { }

    async ngOnInit() {
        const startDate = new Date();
        const topNumber = 15;
        this.users = await this.userService.getUsersForLeaderboard(startDate, topNumber);
        this.usersDataSource = new MatTableDataSource<LeaderboardUser>(this.users);
    }
}
