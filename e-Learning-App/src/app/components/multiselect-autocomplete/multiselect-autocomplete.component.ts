import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OptionItem } from 'src/app/models/option-item.model';

@Component({
    selector: 'multiselect-autocomplete',
    templateUrl: './multiselect-autocomplete.component.html',
    styleUrls: ['./multiselect-autocomplete.component.scss']
})

export class MultiselectAutocompleteComponent {
    @Input() placeholder: string = '';
    @Input() label: string = '';
    @Input() options: Array<OptionItem> = [];
    @Input() modelControl: FormControl = new FormControl();
    @Output() selectionChanged: EventEmitter<any> = new EventEmitter();

    filterControl: FormControl = new FormControl('');
    filteredOptions: Array<OptionItem>;

    ngOnInit() {
        this.filteredOptions = this.options;
    }

    onFilterChange() {
        const filter = this.filterControl?.value?.trim()?.toLocaleLowerCase();
        this.filteredOptions = this.options.filter(o => o.name.trim().toLocaleLowerCase().indexOf(filter) !== -1);
    }

    onSelectionChanged(event: any) {
        this.selectionChanged.emit(this.modelControl.value);
    }

    onSelectClosed(event: any) {
        this.filterControl.setValue('');
        this.filteredOptions = this.options;
    }

    handleKeyDown(event: any) {
        event.stopPropagation();
    }
}
