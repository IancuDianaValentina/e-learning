﻿using System;

namespace eLearningAPI.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
    }
}
