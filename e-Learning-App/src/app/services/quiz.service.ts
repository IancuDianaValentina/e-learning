import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { lastValueFrom } from "rxjs";
import { Quiz } from "../models/quiz/quiz.model";
import { UserAnswer } from "../models/user-sessions/user-answer.model";
import { QuestionTypeEnum } from "../enums";

@Injectable({
    providedIn: 'root'
})
export class QuizService {

    private baseUrl: string = '';

    constructor(private configService: ConfigService, private httpClient: HttpClient) {
        this.baseUrl = this.configService.getUrl("baseApiUrl");
    }

    getQuizByLessonId(lessonId: number): Promise<Quiz> {
        return lastValueFrom(this.httpClient.get<Quiz>(this.baseUrl + `/Quiz/GetByLessonId/${lessonId}`));
    }

    saveUserAnswer(userAnswer: UserAnswer, questionType: QuestionTypeEnum, writtenAnswer: string = ''): Promise<boolean> {
        return lastValueFrom(this.httpClient.post<boolean>(this.baseUrl + `/Quiz/SaveUserAnswer`, { userAnswer: userAnswer, questionType: questionType, writtenAnswer: writtenAnswer }));
    }

    checkIfCorrectAnswer(userAnswer: UserAnswer, questionType: QuestionTypeEnum, writtenAnswer: string = ''): Promise<boolean> {
        return lastValueFrom(this.httpClient.post<boolean>(this.baseUrl + `/Quiz/CheckIfCorrectAnswer`, { userAnswer: userAnswer, questionType: questionType, writtenAnswer: writtenAnswer }));
    }
}