import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ChoiceVm } from 'src/app/view-models/quiz/choice.view-model';
import { SingleChoiceConfigComponent } from '../single-choice-config/single-choice-config.component';

@Component({
    selector: 'multiple-choice-config',
    templateUrl: '../single-choice-config/single-choice-config.component.html',
    styleUrls: ['../single-choice-config/single-choice-config.component.scss']
})

export class MultipleChoiceConfigComponent extends SingleChoiceConfigComponent implements OnInit {
    override ngOnInit(): void {
        this.choiceTextControl = new FormControl('', [Validators.required, this.uniqueTextValidator()]);
        if (this.choices?.length == 0) {
            this.choices = [
                { text: 'Choice 1', isAnswer: false, isEditMode: false, isAddMode: false } as ChoiceVm,
                { text: 'Choice 2', isAnswer: false, isEditMode: false, isAddMode: false } as ChoiceVm,
                { text: 'Choice 3', isAnswer: false, isEditMode: false, isAddMode: false } as ChoiceVm
            ]
        }
    }

    override onChoiceClick(choice: ChoiceVm) {
        if (!this.anyChoiceInAddOrEditMode) {
            choice.isAnswer = !choice.isAnswer;
        }
    }
}
