﻿CREATE PROCEDURE Users_AddPoints
(
	@Points INT
)
AS
BEGIN
	Update Users
	SET Points = Points + @Points;
END