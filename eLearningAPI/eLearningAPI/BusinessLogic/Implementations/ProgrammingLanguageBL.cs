﻿using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Models;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Implementations
{
    public class ProgrammingLanguageBL: IProgrammingLanguageBL
    {
        private readonly IProgrammingLanguageRepository _programmingLanguageRepository;
        private readonly ILogger<ProgrammingLanguageBL> _logger;
        public ProgrammingLanguageBL(IProgrammingLanguageRepository programmingLanguageRepository, ILogger<ProgrammingLanguageBL> logger)
        {
            _programmingLanguageRepository = programmingLanguageRepository;
            _logger = logger;
        }

        public async Task<List<ProgrammingLanguage>> GetAllProgrammingLanguagesAsync()
        {
            _logger.LogInformation("Retrieving all programming languages.");
            try
            {
                var models = await _programmingLanguageRepository.GetAllProgrammingLanguagesAsync();
                _logger.LogInformation(" Successfully retrieved all programming languages.");
                return models;
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving all programming languages.");
                throw new Exception("Error retrieving all programming languages.");
            }
        }
    }
}
