﻿using eLearningAPI.BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProgrammingLanguageController : ControllerBase
    {
        private readonly IProgrammingLanguageBL _programmingLanguageBL;
        public ProgrammingLanguageController(IProgrammingLanguageBL programmingLanguageBL)
        {
            _programmingLanguageBL = programmingLanguageBL;
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAllProgrammingLanguages()
        {
            var response = await _programmingLanguageBL.GetAllProgrammingLanguagesAsync();
            return Ok(response);
        }
    }
}
