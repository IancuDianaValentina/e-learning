﻿using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Dtos.UserSesssion;
using eLearningAPI.Enums;
using eLearningAPI.Extensions;
using eLearningAPI.Models.Quiz;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Implementations
{
    public class QuizBL: IQuizBL
    {
        private readonly IQuizRepository _quizRepository;
        private readonly ILogger<QuizBL> _logger;

        public QuizBL(IQuizRepository quizRepository, ILogger<QuizBL> logger)
        {
            _quizRepository = quizRepository;
            _logger = logger;
        }

        public async Task<Quiz> GetQuizByLessonIdAsync(int lessonId)
        {
            _logger.LogInformation("Retrieving quiz for lesson {0}", lessonId);
            try
            {
                var model = await _quizRepository.GetQuizByLessonIdAsync(lessonId);
                _logger.LogInformation("Successfully retrieved quiz for lesson {0}.", lessonId);
                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving quiz for lesson.");
                throw new Exception("Error retrieving quiz for lesson.");
            }
        }

        public async Task<bool> SaveUserAnswerAsync(UserAnswerDto answer, QuestionTypeEnum questionType, string answerText = "")
        {
            _logger.LogInformation("Saving answer for session {0}", answer.UserSessionId);
            var isAnswerCorrect = await IsAnswerCorrectAsync(answer, questionType, answerText);
            answer.Id = Guid.NewGuid();
            answer.CorrectlyAnswered = isAnswerCorrect;
            await _quizRepository.SaveUserAnswerAsync(answer.ToModel());
            return isAnswerCorrect;
        }

        public async Task<bool> IsAnswerCorrectAsync(UserAnswerDto answer, QuestionTypeEnum questionType, string answerText = "")
        {
            var answerChoices = await _quizRepository.GetAnswersByQuestionIdAsync(answer.QuestionId);

            var isAnswer = false;
            switch (questionType)
            {
                case QuestionTypeEnum.WrittenAnswer:
                    isAnswer = answerChoices[0].Text.Contains(answerText);
                    answer.ChoiceIds = new List<Guid>
                    {
                        isAnswer == true ? answerChoices[0].Id : new Guid()
                    };
                    break;
                case QuestionTypeEnum.SingleChoice:
                    isAnswer = answerChoices[0].Id == answer.ChoiceIds[0];
                    break;
                case QuestionTypeEnum.MultipleChoice:
                    isAnswer = answer.ChoiceIds.All(answerChoices.Select(c => c.Id).ToList().Contains) && answer.ChoiceIds.Count == answerChoices.Count;
                    break;
            }
            return isAnswer;
        }
    }
}
