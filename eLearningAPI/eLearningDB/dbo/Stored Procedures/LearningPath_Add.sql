﻿CREATE PROCEDURE LearningPath_Add
	(
		@Title NVARCHAR(255),
		@NoLearningHours SMALLINT,
		@Description NVARCHAR(MAX) = NULL,
		@ProgrammingLanguageId TINYINT,
		@LearningLevelId TINYINT
	)
AS
BEGIN
	INSERT INTO [dbo].[LearningPath]
	(
	[Title],
	[NoLearningHours],
	[Description],
	[ProgrammingLanguageId],
	[LearningLevelId]
	)
	VALUES
	(
	@Title,
	@NoLearningHours,
	@Description,
	@ProgrammingLanguageId,
	@LearningLevelId
	);
END