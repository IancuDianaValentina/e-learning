import { UserSessionStatusEnum } from "src/app/enums";
import { UserAnswer } from "./user-answer.model";

export class UserLessonSession {
    id: string;
    userId: string;
    lessonId: number;
    learningPathId: number;
    status: UserSessionStatusEnum;
    userAnswers: Array<UserAnswer>;
}