import { QuestionVm } from "./question.view-model";

export class QuizVm {
    id: number;
    title: string;
    questions: QuestionVm[];
}