import { Component, Input } from '@angular/core';

export class MenuItem {
  id?: any;
  itemName?: string;
  callback?: Function;
}

export class AppLogo {
  path: string;
  callback?: Function;
}

export class ToolbarItem {
  itemName?: string;
  callback?: Function;
  icon?: string;
  menuItems?: Array<MenuItem>;
}

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})

export class ToolbarComponent {
  @Input() toolbarSimpleItems: Array<ToolbarItem> = [];
  @Input() toolbarMenuItems: Array<ToolbarItem> = []
  @Input() appTitle: string = '';
  @Input() appLogo: AppLogo;

}
