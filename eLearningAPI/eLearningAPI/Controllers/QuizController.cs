﻿using Authentication.Attributes;
using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Dtos.ApiRequestDto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace eLearningAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class QuizController : ControllerBase
    {
        private readonly IQuizBL _quizBL;

        public QuizController(IQuizBL quizBL)
        {
            _quizBL = quizBL;
        }

        [HttpGet("GetByLessonId/{lessonId}")]
        public async Task<IActionResult> GetQuizByLessonId(int lessonId)
        {
            var response = await _quizBL.GetQuizByLessonIdAsync(lessonId);
            return Ok(response);
        }

        [HttpPost("SaveUserAnswer")]
        public async Task<IActionResult> SaveUserAnswer([FromBody] SaveAnswerRequest saveAnswerRequest)
        {
            var response = await _quizBL.SaveUserAnswerAsync(saveAnswerRequest.UserAnswer, saveAnswerRequest.QuestionType, saveAnswerRequest.WrittenAnswer);
            return Ok(response);
        }

        [HttpPost("CheckIfCorrectAnswer")]
        public async Task<IActionResult> CheckIfCorrectAnswer([FromBody] SaveAnswerRequest saveAnswerRequest)
        {
            var response = await _quizBL.IsAnswerCorrectAsync(saveAnswerRequest.UserAnswer, saveAnswerRequest.QuestionType, saveAnswerRequest.WrittenAnswer);
            return Ok(response);
        }
    }
}
