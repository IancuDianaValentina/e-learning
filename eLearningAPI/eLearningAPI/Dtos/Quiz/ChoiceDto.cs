﻿using System;

namespace eLearningAPI.Dtos.Quiz
{
    public class ChoiceDto
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public string Text { get; set; }
        public bool IsAnswer { get; set; }
    }
}
