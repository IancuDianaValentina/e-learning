﻿using Authentication.Dtos;
using Authentication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Authentication.Enums.Enums;

namespace Authentication.Middlewares
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly JwtSettings _jwtSettings;
        public AuthenticationMiddleware(IOptionsMonitor<JwtSettings> options, RequestDelegate next)
        {
            _jwtSettings = options.CurrentValue;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            // Get the token from the request's headers
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if (token != null)
            {
                var jwtToken = ValidateToken(token);
                if (jwtToken != null)
                {
                    AttachUserToContext(context, jwtToken);
                }
            }
            await _next(context);
        }

        public JwtSecurityToken ValidateToken(string token)
        {
            // Token handler used in order to validate the token
            var tokenHandler = new JwtSecurityTokenHandler();
            // Get the secret key from the jwtSettings instance
            var key = Encoding.ASCII.GetBytes(_jwtSettings.SecretKey);
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero // This is used so that the token expires exactly at its expiry time, not 5 minutes later
                }, out SecurityToken validatedToken);

                return (JwtSecurityToken) validatedToken;
            }
            catch
            {
                return null; // if validation fails
            }
        }

        public void AttachUserToContext(HttpContext context, JwtSecurityToken jwtToken)
        {
            try
            {
                context.Items["User"] = new UserDetails
                {
                    Id = Guid.Parse(jwtToken.Claims.First(c => c.Type == UserClaims.Id).Value),
                    FirstName = jwtToken.Claims.First(c => c.Type == UserClaims.FirstName).Value,
                    LastName = jwtToken.Claims.First(c => c.Type == UserClaims.LastName).Value,
                    Username = jwtToken.Claims.First(c => c.Type == UserClaims.Username).Value,
                    EmailAddress = jwtToken.Claims.First(c => c.Type == UserClaims.EmailAddress).Value,
                    Address = jwtToken.Claims.First(c => c.Type == UserClaims.Address).Value,
                    PhoneNumber = jwtToken.Claims.First(c => c.Type == UserClaims.PhoneNumber).Value,
                    RoleId = (UserRoleEnum) Int16.Parse(jwtToken.Claims.First(c => c.Type == UserClaims.RoleId).Value)
                };
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
}
