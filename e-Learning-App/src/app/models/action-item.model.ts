export interface ActionItem {
    icon: string;
    iconColor?: string;
    onActionClick(data: any): Function;
}