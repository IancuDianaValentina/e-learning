import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationPopupAction } from "src/app/components/custom-popup/custom-popup.component";
import { LessonLearningPath } from "src/app/models/lesson/lesson-learning-path.model";
import { UserLessonSessionListItem } from "src/app/models/user-sessions/user-lesson-session-list-item.model";
import { CustomPopupService } from "src/app/services/custom-popup.service";
import { LessonService } from "src/app/services/lesson.service";
import { SessionService } from "src/app/services/session.service";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'view-lesson',
    templateUrl: './view-lesson.component.html',
    styleUrls: ['./view-lesson.component.scss']
})

export class ViewLessonComponent implements OnInit {

    lessonLearningPath: LessonLearningPath = new LessonLearningPath();
    userSessions: UserLessonSessionListItem[] = [];
    userId: string;
    userPoints: number;
    learningPathId: number;

    constructor(private webSessionService: SessionService, private lessonService: LessonService,
        private activeRoute: ActivatedRoute, private router: Router, private customPopupService: CustomPopupService,
        private userService: UserService) { }

    async ngOnInit() {
        this.userId = (await this.webSessionService.getUserSession())?.id;
        // for test only
        //this.userId = 'B675C58D-2ADD-41AB-BE69-952B2E40B03F';

        this.activeRoute.params.subscribe(
            async param => {
                const lessonId = parseInt(param?.["lessonId"] ?? '0');
                this.learningPathId = parseInt(param?.["learningPathId"] ?? '0');

                this.lessonLearningPath = await this.lessonService.getLessonLearningPath(lessonId, this.learningPathId);
                this.userSessions = await this.userService.getUserSessions(this.userId, this.learningPathId);
            }
        );


    }

    async onOpenNextLesson(lessonId: number) {
        const sessionExists: boolean = this.userSessions.some(l => l.lessonId == lessonId);
        if (this.lessonLearningPath.nextLesson.requiredPoints > this.userPoints && !sessionExists) {
            this.customPopupService.openPopup('Start lesson', ['You can not start with the lesson because you do not have the required amount of points',
                'You must wait until tommorow to receive the amount of points needed'], [{ text: 'Ok', action: ConfirmationPopupAction.Ok }]);
            return;
        }
        if (this.lessonLearningPath.nextLesson.requiredPoints > 0 && !sessionExists) {
            var response = await this.customPopupService.openPopup('Start lesson', [`Starting this lesson will cause an automatic subtraction of
             ${this.lessonLearningPath.nextLesson.requiredPoints} points from your account`,
                'Are you sure you want to start?'], [{ text: 'Ok', action: ConfirmationPopupAction.Ok }, { text: 'Cancel', action: ConfirmationPopupAction.Cancel }]);
            if (response == ConfirmationPopupAction.Cancel) {
                return;
            }
        }
        if (!sessionExists) {
            await this.userService.createUserSession(this.userId, this.learningPathId, lessonId, this.lessonLearningPath.nextLesson.requiredPoints);
        }
        this.router.navigateByUrl(`learning-path/${this.learningPathId}/lesson/${lessonId}/view`);
    }

    lessonHasSession(lessonId: number) {
        return this.userSessions?.some(l => l.lessonId == lessonId);
    }

    onTakeQuiz() {
        this.router.navigateByUrl(`learning-path/${this.learningPathId}/lesson/${this.lessonLearningPath.lessonId}/quiz`);
    }
}
