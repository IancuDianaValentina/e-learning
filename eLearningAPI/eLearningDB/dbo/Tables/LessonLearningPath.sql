﻿CREATE TABLE [dbo].[LessonLearningPath] (
    [LearningPathId] SMALLINT NULL,
    [LessonId]       INT      NULL,
    [LessonIndex]    INT      NULL,
    FOREIGN KEY ([LearningPathId]) REFERENCES [dbo].[LearningPath] ([Id]),
    FOREIGN KEY ([LessonId]) REFERENCES [dbo].[Lesson] ([Id])
);

