﻿CREATE TABLE [dbo].[UserLesson] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [UserId]         UNIQUEIDENTIFIER NOT NULL,
    [LessonId]       INT              NOT NULL,
    [LearningPathId] SMALLINT         NOT NULL,
    [Status]         TINYINT          DEFAULT ((1)) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([LearningPathId]) REFERENCES [dbo].[LearningPath] ([Id]),
    FOREIGN KEY ([LessonId]) REFERENCES [dbo].[Lesson] ([Id]),
    FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

