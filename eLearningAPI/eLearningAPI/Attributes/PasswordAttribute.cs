﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace eLearningAPI.Attributes
{
    class PasswordAttribute: ValidationAttribute, IClientValidatable
    {
        private readonly int _minLength;  
        private readonly bool _includeSymbol;
        private readonly bool _includeUppercase;
        private readonly bool _includeDigit;
        private readonly List<String> errors = new List<String>();

        public PasswordAttribute(int minLength = 8, bool includeSymbol = false, bool includeDigit = false, bool includeUppercase = false)
        {
            _minLength = minLength;
            _includeSymbol = includeSymbol;
            _includeUppercase = includeUppercase;
            _includeDigit = includeDigit;
        }
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            bool isValid = true;

            if (value.ToString().Length < _minLength)
            {
                isValid = false;
                errors.Add(string.Format("The field {0} must have a min length of {1} characters", context.MemberName, _minLength));
            } 
            if (_includeDigit && !new Regex(".*[0-9].*").IsMatch(value.ToString()))
            {
                isValid = false;
                errors.Add(string.Format("The field {0} must contain a digit", context.MemberName));
            }
            if (_includeUppercase && !new Regex(".*[A-Z].*").IsMatch(value.ToString()))
            {
                isValid = false;
                errors.Add(string.Format("The field {0} must contain an uppercase character", context.MemberName));
            }
            if (_includeSymbol && !new Regex("[^A-Za-z0-9]").IsMatch(value.ToString()))
            {
                isValid = false;
                errors.Add(string.Format("The field {0} must contain a special character", context.MemberName));
            }
            if (isValid)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(
                    string.Join("\n", errors));
            }
        }


        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("minLength", _minLength);
            rule.ValidationParameters.Add("includeSymbol", _includeSymbol);
            rule.ValidationParameters.Add("includeUppercase", _includeUppercase);
            rule.ValidationParameters.Add("includeDigit", _includeDigit);
            rule.ValidationType = "password";
            yield return rule;
        }
    }
}
