import { LearningPathLink } from "../models/learning-path/learning-path-link.model";
import { LearningPathVm } from "./learning-path.view-model";

export class LessonVm {
    id: number;
    title: string;
    content: string;
    learningTime: number;
    requiredPoints: number;
    programmingLanguage: string;
    learningPathLinks: Array<LearningPathLink>;
    learningPaths: Array<LearningPathVm>;
}