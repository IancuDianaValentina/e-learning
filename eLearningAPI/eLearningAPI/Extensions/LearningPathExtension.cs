﻿using eLearningAPI.Dtos;
using eLearningAPI.Models;
using System.Linq;

namespace eLearningAPI.Extensions
{
    public static class LearningPathExtension
    {
        public static LearningPathDto ToDto(this LearningPath model)
        {
            return new LearningPathDto
            {
                Id = model.Id,
                Title = model.Title,
                NoLearningHours = model.NoLearningHours,
                Description = model.Description,
                LearningLevel = model.LearningLevel,
                ProgrammingLanguage = model.ProgrammingLanguage,
                Lessons = model.Lessons?.Select(l => l.ToDto()).ToList()
            };
        }

        public static LearningPath ToModel(this LearningPathDto dto)
        {
            return new LearningPath
            {
                Id = dto.Id,
                Title = dto.Title,
                NoLearningHours = dto.NoLearningHours,
                Description = dto.Description,
                ProgrammingLanguage = dto.ProgrammingLanguage,
                LearningLevel = dto.LearningLevel
            };
        }
    }
}
