import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'inline-edit',
    templateUrl: './inline-edit.component.html',
    styleUrls: ['./inline-edit.component.scss']
})

export class InlineEditComponent {
    @Input() valueControl: FormControl;
    @Input() isEditMode: boolean = false;
    @Input() value: string = 'Input text here';
    @Input() label: string = '';
    @Input() model: any;
    @Input() deleteButtonDisabled: boolean = false;
    @Input() hideDeleteButton: boolean = false;
    @Input() canNotEdit: boolean = false;
    @Output() valueChange: EventEmitter<string> = new EventEmitter();
    @Output() onStart: EventEmitter<any> = new EventEmitter();
    @Output() onCancel: EventEmitter<any> = new EventEmitter();
    @Output() onFinish: EventEmitter<any> = new EventEmitter();
    @Output() onDelete: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        if (!this.valueControl) {
            this.valueControl = new FormControl('', [Validators.required]);
        }
        this.valueControl.setValue(this.value);
    }

    onCancelOperation = () => {
        this.isEditMode = false;
        this.onCancel.emit(this.model);
    }

    onStartEdit = () => {
        this.valueControl.setValue(this.value);
        this.isEditMode = true;
        this.onStart.emit(this.model);
    }

    onFinishEdit = () => {
        this.value = this.valueControl?.value;
        this.valueChange.emit(this.value);
        this.isEditMode = false;
        this.onFinish.emit(this.model);
    }

    onDeleteItem = () => {
        this.onDelete.emit(this.model);
    }

    getFirstError = () => {
        var firstErrorKey = Object.keys(this.valueControl.errors)[0];
        return this.valueControl?.getError(firstErrorKey);
    }
}
