import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSliderModule } from '@angular/material/slider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FlexLayoutModule } from "@angular/flex-layout";

import { EditorModule } from "@tinymce/tinymce-angular";

import { AuthenticationComponent } from './pages/authentication/authentication.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ConfigService } from './services/config.service';
import { ErrorHandlerService } from './services/error-handling.service';
import { SessionService } from './services/session.service';
import { TokenInterceptor } from './services/token-interceptor.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { UserService } from './services/user.service';
import { RegistrationComponent } from './pages/registration/registration.component';
import { LearningPathService } from './services/learning-path.service';
import { LearningPathListComponent } from './pages/learning-path-list/learning-path-list.component';
import { LessonListComponent } from './pages/lesson-list/lesson-list.component';
import { LessonComponent } from './pages/lesson/lesson.component';
import { ProgrammingLanguageService } from './services/programming-language.service';
import { LessonService } from './services/lesson.service';
import { CustomPopupComponent } from './components/custom-popup/custom-popup.component';
import { MultiselectAutocompleteComponent } from './components/multiselect-autocomplete/multiselect-autocomplete.component';
import { CustomPopupService } from './services/custom-popup.service';
import { HomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { MatCarouselModule } from 'ng-mat-carousel';
import { QuizConfigComponent } from './components/quiz-compontents/quiz-config/quiz-config.component';
import { QuestionConfigComponent } from './components/quiz-compontents/question-config/question-config.component';
import { QuestionService } from './services/question.service';
import { SingleChoiceConfigComponent } from './components/quiz-compontents/single-choice-config/single-choice-config.component';
import { InlineEditComponent } from './components/inline-edit/inline-edit.component';
import { TextEditorComponent } from './components/text-editor/text-editor.component';
import { MultipleChoiceConfigComponent } from './components/quiz-compontents/multiple-choice-config/multiple-choice.component';
import { WrittenAnswerComponent } from './components/quiz-compontents/written-answer/written-answer.component';
import { MatSortModule } from '@angular/material/sort';
import { LearningPathTableComponent } from './components/learning-path-table/learning-path-table.component';
import { LearningPathComponent } from './pages/learning-path/learning-path.component';
import { LessonsTableComponent } from './components/lessons-table/lessons-table.component';
import { ViewLearningPathComponent } from './pages/view-learning-path/view-learning-path.component';
import { ViewLessonComponent } from './pages/view-lesson/view-lesson.component';
import { LeaderboardComponent } from './pages/leaderboard/leaderboard.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';

import * as CanvasJSAngularChart from '../assets/canvasjs.angular.component';
import { UserQuizComponent } from './pages/user-quiz/user-quiz.component';
var CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;

@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    RegistrationComponent,
    ToolbarComponent,
    LearningPathListComponent,
    LessonListComponent,
    LessonComponent,
    HomeComponent,
    ContactComponent,
    CustomPopupComponent,
    MultiselectAutocompleteComponent,
    QuizConfigComponent,
    QuestionConfigComponent,
    SingleChoiceConfigComponent,
    MultipleChoiceConfigComponent,
    WrittenAnswerComponent,
    InlineEditComponent,
    TextEditorComponent,
    LearningPathTableComponent,
    LearningPathComponent,
    ViewLearningPathComponent,
    LessonsTableComponent,
    ViewLessonComponent,
    LeaderboardComponent,
    UserProfileComponent,
    UserQuizComponent,
    CanvasJSChart
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    GoogleMapsModule,
    HttpClientModule,
    MatToolbarModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatSliderModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatDividerModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatTableModule,
    MatTooltipModule,
    MatStepperModule,
    MatSelectModule,
    MatDialogModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatProgressBarModule,
    MatCarouselModule,
    MatRadioModule,
    MatSortModule,
    FormsModule,
    EditorModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [ConfigService],
      useFactory: (appConfigService: ConfigService) => {
        return () => {
          return appConfigService.loadAppConfig();
        };
      }
    },
    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ErrorHandlerService,
        multi: true
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true
      }
    ],
    SessionService,
    UserService,
    LearningPathService,
    LessonService,
    CustomPopupService,
    ProgrammingLanguageService,
    QuestionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
