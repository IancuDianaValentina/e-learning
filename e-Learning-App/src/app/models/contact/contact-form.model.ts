export class ContactForm {
    name: string;
    subject: string;
    emailAddress: string;
    content: string;
}