﻿using MailManagement.Models;
using MailManagement.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MailManagement.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        private readonly IMailService mailService;
        public MailController(IMailService mailService)
        {
            this.mailService = mailService;
        }

        [HttpPost("SendContactMail")]
        public async Task<IActionResult> SendContactMail([FromBody] ContactMailRequest request)
        {
            await mailService.SendContactMailAsync(request);
            return Ok();
        }

    }
}
