﻿
CREATE PROCEDURE Question_DeleteById
(
	@QuestionId INT
)
AS
BEGIN
	DELETE FROM Question WHERE Id = @QuestionId;
END