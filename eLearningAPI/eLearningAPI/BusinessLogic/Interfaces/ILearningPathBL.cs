﻿using eLearningAPI.Dtos;
using eLearningAPI.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Interfaces
{
    public interface ILearningPathBL
    {
        public Task<List<LearningPathDto>> GetAllLearningPathsAsync();
        public Task<LearningPathDto> GetLearningPathAsync(int learningPathId);
        public Task<List<LearningPathDto>> GetByProgrammingLanguageAsync(ProgrammingLanguageEnum programmingLanguage);
        public Task<int> SaveLearningPathAsync(LearningPathDto learningPath);
        public Task UnlinkLessonFromLearningPathAsync(int lessonId, int learningPathId);
    }
}
