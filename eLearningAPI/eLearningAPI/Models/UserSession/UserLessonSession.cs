﻿using eLearningAPI.Enums;
using System;
using System.Collections.Generic;

namespace eLearningAPI.Models.UserSession
{
    public class UserLessonSession
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int LessonId { get; set; }
        public int LearningPathId { get; set; }
        public UserSessionStatusEnum Status { get; set; }
        public int QuizId { get; set; }
        public List<UserAnswer> UserAnswers { get; set; }
    }
}
