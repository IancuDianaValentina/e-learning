﻿using eLearningAPI.Dtos;
using eLearningAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Interfaces
{
    public interface ILessonBL
    {
        public Task DeleteLessonAsync(int lessonId);
        public Task SaveLessonAsync(Lesson lesson);
        public Task<List<LessonDto>> GetAllLessonsAsync();
        public Task<LessonDto> GetLessonWithQuizByIdAsync(int lessonId);
        public Task<LessonLearningPathDto> GetLessonLearningPathAsync(int lessonId, int learningPathId);
    }
}
