﻿
CREATE PROCEDURE User_GetById 
(
	@UserId UNIQUEIDENTIFIER
)
AS
BEGIN
	SELECT * 
	FROM Users
	WHERE Id = @UserId;
END