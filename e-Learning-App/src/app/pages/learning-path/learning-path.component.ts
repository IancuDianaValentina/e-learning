import { Component, OnInit } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationPopupAction } from "src/app/components/custom-popup/custom-popup.component";
import { ActionItem } from "src/app/models/action-item.model";
import { CustomFormGroup } from "src/app/models/custom-form-group";
import { LearningPath } from "src/app/models/learning-path/learning-path.model";
import { Lesson } from "src/app/models/lesson/lesson.model";
import { ProgrammingLanguage } from "src/app/models/programming-language.model";
import { CustomPopupService } from "src/app/services/custom-popup.service";
import { LearningPathService } from "src/app/services/learning-path.service";
import { ProgrammingLanguageService } from "src/app/services/programming-language.service";

@Component({
    selector: 'learning-path',
    templateUrl: './learning-path.component.html',
    styleUrls: ['./learning-path.component.scss']
})

export class LearningPathComponent implements OnInit {

    isEditMode: boolean = false;
    learningPathFormGroup: CustomFormGroup<LearningPath>;
    programmingLanguages: ProgrammingLanguage[] = [];
    learningPath: LearningPath = new LearningPath();
    lessonActions: ActionItem[] = [
        {
            icon: 'link_off',
            iconColor: 'color-red',
            onActionClick: this.onUnlinkLesson.bind(this)
        },
        {
            icon: 'edit',
            iconColor: 'color-indigo',
            onActionClick: this.onEditLesson.bind(this)
        }
    ];
    constructor(private route: ActivatedRoute, private programmingLanguageService: ProgrammingLanguageService,
        private router: Router, private learningPathService: LearningPathService, private popupService: CustomPopupService) { }

    async ngOnInit() {
        this.programmingLanguageService.getAllProgrammingLanguages().then(res => {
            this.programmingLanguages = res;
        });
        this.learningPathFormGroup = new CustomFormGroup<LearningPath>({
            title: new FormControl('', [Validators.required]),
            description: new FormControl('', [Validators.required]),
            programmingLanguage: new FormControl('', [Validators.required]),
            learningLevel: new FormControl('', [Validators.required])
        });
        const learningPathId = this.route.snapshot.paramMap.get("id");
        this.isEditMode = learningPathId ? true : false;
        if (this.isEditMode) {
            this.learningPath = await this.learningPathService.getLearningPath(parseInt(learningPathId ?? '0'));
            this.learningPathFormGroup.getFormControl('title').setValue(this.learningPath.title);
            this.learningPathFormGroup.getFormControl('description').setValue(this.learningPath.description);
            this.learningPathFormGroup.getFormControl('programmingLanguage').setValue(this.learningPath.programmingLanguage);
            this.learningPathFormGroup.getFormControl('learningLevel').setValue(this.learningPath.learningLevel);
        }
    }

    onAddLesson() {
        this.router.navigate(['/lesson/add'], { queryParams: { returnUrl: this.router.url }, state: { learningPathId: this.learningPath.id, programmingLanguage: this.learningPath.programmingLanguage } });
    }

    async onUnlinkLesson(lesson: Lesson) {
        var response = await this.popupService.openPopup('Unlink lesson', ['Are you sure you want to unlink this lesson?'],
            [{ text: 'Ok', action: ConfirmationPopupAction.Ok }, { text: 'Cancel', action: ConfirmationPopupAction.Cancel }]);
        if (response == ConfirmationPopupAction.Ok) {
            await this.learningPathService.unlinkLessonFromLearningPath(lesson.id, this.learningPath.id);
            await this.loadData(this.learningPath.id);
        }
    }

    onEditLesson(lesson: any) {
        this.router.navigateByUrl(`/lesson/${lesson.id}/edit`);
    }

    async onSave() {
        this.learningPath.learningLevel = this.learningPathFormGroup.getFormControl('learningLevel')?.value;
        this.learningPath.programmingLanguage = this.learningPathFormGroup.getFormControl('programmingLanguage')?.value;
        this.learningPath.title = this.learningPathFormGroup.getFormControl('title')?.value;
        this.learningPath.description = this.learningPathFormGroup.getFormControl('description')?.value;
        this.learningPath.id = await this.learningPathService.saveLearningPath(this.learningPath);
        if (!this.isEditMode) {
            this.router.navigateByUrl(`/learning-path/${this.learningPath.id}/edit`);
        }
    }

    async loadData(learningPathId: number) {
        this.learningPath = await this.learningPathService.getLearningPath(learningPathId);
    }
}
