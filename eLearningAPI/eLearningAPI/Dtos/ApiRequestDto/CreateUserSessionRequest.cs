﻿using System;

namespace eLearningAPI.Dtos.ApiRequestDto
{
    public class CreateUserSessionRequest
    {
        public Guid UserId { get; set; }
        public int LearningPathId { get; set; }
        public int LessonId { get; set; }
        public int RequiredPoints { get; set; }
    }
}
