﻿CREATE PROCEDURE UserSession_ChangeStatus
	@UserSessionId UNIQUEIDENTIFIER,
	@Status TINYINT
AS
BEGIN
	UPDATE UserLesson
	SET Status = @Status
	WHERE Id = @UserSessionId;
END