﻿using eLearningAPI.Models.Quiz.Questions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Interfaces
{
    public interface IQuestionBL
    {
        public Task<List<QuestionType>> GetQuestionTypesAsync();
        public Task DeleteQuestionsByQuizIdAsync(int quizId);
        public Task DeleteChoiceByIdAsync(int choiceId);
        public Task DeleteQuestionByIdAsync(int questionId);
    }
}
