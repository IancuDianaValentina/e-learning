﻿CREATE PROCEDURE [dbo].[LessonLearningPath_Unlink]
(
	@LessonId INT,
	@LearningPathId INT
)
AS
BEGIN
	UPDATE LP
	SET NoLearningHours = NoLearningHours - L.LearningTime
	FROM LearningPath LP 
	INNER JOIN LessonLearningPath LLP
		ON LLP.LearningPathId = LP.Id
	INNER JOIN Lesson L
		ON L.Id = LLP.LessonId
	WHERE LLP.LessonId = @LessonId AND
		LLP.LearningPathId = @LearningPathId;

	DELETE
	FROM LessonLearningPath
	WHERE LessonId = @LessonId AND
		LearningPathId = @LearningPathId;

END