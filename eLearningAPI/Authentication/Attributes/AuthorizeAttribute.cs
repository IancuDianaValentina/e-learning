﻿using Authentication.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using static Authentication.Enums.Enums;

namespace Authentication.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private UserRoleEnum[] _roleIds;

        public AuthorizeAttribute(params UserRoleEnum[] roleIds)
        {
            _roleIds = roleIds;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // Skip authorization if action is decorated with [AllowAnonymous] attribute
            var allowAnonymous = context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().Any();
            if (allowAnonymous)
            {
                return;
            }

            // Get the authenticated user from the  HttpContext
            var user = (UserDetails) context.HttpContext.Items["User"];

            // If there is no user attached to the context, return Unauthorized
            if (user == null)
            {
                context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
                return;
            }

            if (_roleIds == null || _roleIds.Length == 0)
            {
                return;
            }

            if (_roleIds != null && _roleIds.Length > 0)
            {
                foreach (var roleId in _roleIds)
                {
                    if (roleId == user.RoleId)
                    {
                        return;
                    }
                }
            }
            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
        }
    }
}
