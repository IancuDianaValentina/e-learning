﻿CREATE TABLE [dbo].[UserSessionAnswer] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [QuestionId]        UNIQUEIDENTIFIER NOT NULL,
    [CorrectlyAnswered] BIT              DEFAULT ((0)) NULL,
    [AnsweredAt]        DATETIME2 (7)    NOT NULL,
    [UserSessionId]     UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([Id]),
    FOREIGN KEY ([UserSessionId]) REFERENCES [dbo].[UserLesson] ([Id])
);

