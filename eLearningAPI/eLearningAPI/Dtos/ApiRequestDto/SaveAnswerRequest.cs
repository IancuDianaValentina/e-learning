﻿using eLearningAPI.Dtos.UserSesssion;
using eLearningAPI.Enums;

namespace eLearningAPI.Dtos.ApiRequestDto
{
    public class SaveAnswerRequest
    {
        public UserAnswerDto UserAnswer { get; set; }
        public QuestionTypeEnum QuestionType { get; set; }
        public string WrittenAnswer { get; set; }
    }
}
