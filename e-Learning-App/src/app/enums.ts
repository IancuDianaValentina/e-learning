export enum UserRoleEnum {
    Administrator = 1,
    User = 2
}

export enum ProgrammingLanguageEnum {
    CSharp = 1,
    Java = 2,
    JavaScript = 3
}

export enum LearningLevelEnum {
    Beginner = 1,
    Intermediate = 2,
    Advanced = 3
}

export enum QuestionTypeEnum {
    SingleChoice = 1,
    MultipleChoice = 2,
    WrittenAnswer = 3
}

export enum UserSessionStatusEnum {
    NotStarted = 0,
    InProgress = 1,
    Completed = 2
}