﻿CREATE TABLE [dbo].[ProgrammingLanguage] (
    [Id]   TINYINT       NOT NULL,
    [Name] NVARCHAR (15) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

