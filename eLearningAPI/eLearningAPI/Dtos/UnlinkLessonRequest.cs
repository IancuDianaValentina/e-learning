﻿namespace eLearningAPI.Dtos
{
    public class UnlinkLessonRequest
    {
        public int LessonId { get; set; }
        public int LearningPathId { get; set; }
    }
}
