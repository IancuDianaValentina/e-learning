﻿using Authentication.Dtos;
using Authentication.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Authentication.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        public AuthenticationController(IAuthenticationService authenticationService) {
            _authenticationService = authenticationService;
        }

        [HttpPost("Authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]AuthenticationRequest authReq)
        {
            if (authReq == null) { return BadRequest(); }
            var response = await _authenticationService.GenerateAuthenticationResponse(authReq);
            if (response == null){ return BadRequest("Invalid username and/or password"); }
            return Ok(response);
        }

        [HttpPost("RefreshJwtToken")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest refreshTokenReq)
        {
            var response = await _authenticationService.RefreshJwtToken(refreshTokenReq.JwtRefreshToken);
            return Ok(response);
        }
    }
}
