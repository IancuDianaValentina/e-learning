﻿namespace Common.Models
{
    public class JwtSettings
    {
        public string SecretKey { get; set; }
        public int JwtTokenExpirationLimit { get; set; }
        public int RefreshTokenExpirationLimit { get; set; }
    }
}
