﻿using Authentication.Models;
using System;
using System.Threading.Tasks;

namespace Authentication.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public Task<UserDetails> GetUserByUsernameAsync(string username);
        public Task<UserDetails> GetUserByRefreshTokenAsync(string refreshToken);
        public Task AddRefreshTokenToUserAsync(RefreshToken refreshToken, Guid userId);
    }
}
