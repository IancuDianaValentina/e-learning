import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActionItem } from 'src/app/models/action-item.model';
import { Lesson } from 'src/app/models/lesson/lesson.model';

export enum ListItemSymbolType {
    Bullet = 1,
    Arrow = 2
}

@Component({
    selector: 'lessons-table',
    templateUrl: './lessons-table.component.html',
    styleUrls: ['./lessons-table.component.scss']
})

export class LessonsTableComponent implements OnInit, OnChanges {
    displayedColumns: string[] = [
        "line",
        "id",
        "title",
        "learningTime",
        "requiredPoints"
    ];
    @Input() lessons: Lesson[] = [];
    @Input() actions: ActionItem[] = [];
    @Input() clickable = false;
    @Input() listItemSymbolType: ListItemSymbolType = ListItemSymbolType.Bullet;
    @Output() rowClick: EventEmitter<Lesson> = new EventEmitter<Lesson>();

    lessonsDataSource: MatTableDataSource<Lesson> = new MatTableDataSource<Lesson>();
    listItemSymbolTypeEnum = ListItemSymbolType;

    constructor() { }

    ngOnInit() {
        this.lessonsDataSource = new MatTableDataSource<Lesson>(this.lessons);
        if (this.actions && this.actions?.length > 0) {
            this.displayedColumns.push("actions");
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['lessons']) {
            this.lessonsDataSource = new MatTableDataSource<Lesson>(this.lessons);
        }
    }

    onRowClick(row: Lesson) {
        this.rowClick.emit(row);
    }
}
