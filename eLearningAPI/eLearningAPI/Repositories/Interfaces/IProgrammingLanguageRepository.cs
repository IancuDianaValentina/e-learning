﻿using eLearningAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Interfaces
{
    public interface IProgrammingLanguageRepository
    {
        public Task<List<ProgrammingLanguage>> GetAllProgrammingLanguagesAsync();
    }
}
