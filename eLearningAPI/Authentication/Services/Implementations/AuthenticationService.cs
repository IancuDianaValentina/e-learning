﻿using Authentication.Dtos;
using Authentication.Models;
using Authentication.Repositories.Interfaces;
using Authentication.Services.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Services
{
    public class AuthenticationService: IAuthenticationService
    {
        private readonly JwtSettings _jwtSettings;
        private readonly IUserRepository _userRepository;

        public AuthenticationService(IOptionsMonitor<JwtSettings> monitor, IUserRepository userRepository)
        {
            _jwtSettings = monitor.CurrentValue;
            _userRepository = userRepository;
        }

        public async Task<AuthenticationResponse> GenerateAuthenticationResponse(AuthenticationRequest authReq)
        {
            var user = await _userRepository.GetUserByUsernameAsync(authReq.Username);
            if (user == null) { return null; }

            var isPasswordCorrect = BCrypt.Net.BCrypt.Verify(authReq.Password, user.PasswordHash);
            if (!isPasswordCorrect) { return null; }

            var jwtToken = GenerateJwtToken(user);
            var jwtRefreshToken = GenerateRefreshToken();

            if (jwtToken != null && jwtRefreshToken != null)
            {
                await _userRepository.AddRefreshTokenToUserAsync(jwtRefreshToken, user.Id);
                return new AuthenticationResponse
                {
                    Id = user.Id,
                    Username = user.Username,
                    Role = user.RoleId,
                    JwtToken = jwtToken,
                    JwtRefreshToken = jwtRefreshToken.Token
                };
            }

            return null;
        }

        public async Task<AuthenticationResponse> RefreshJwtToken(string refreshToken)
        {
            var user = await _userRepository.GetUserByRefreshTokenAsync(refreshToken);
            if (user == null || DateTime.UtcNow > user.RefreshTokenExpirationTime) { return null; }

            var jwtRefreshToken = GenerateRefreshToken();
            var jwtToken = GenerateJwtToken(user);

            if (jwtToken != null && refreshToken != null)
            {
                return new AuthenticationResponse
                {
                    Id = user.Id,
                    Username = user.Username,
                    Role = user.RoleId,
                    JwtToken = jwtToken,
                    JwtRefreshToken = jwtRefreshToken.Token
                };
            }
            return null;
        }
        private string GenerateJwtToken(UserDetails user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.SecretKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(UserClaims.Id, user.Id.ToString()),
                    new Claim(UserClaims.Username, user.Username.ToString()),
                    new Claim(UserClaims.FirstName, user.FirstName.ToString()),
                    new Claim(UserClaims.LastName, user.LastName.ToString()),
                    new Claim(UserClaims.EmailAddress, user.EmailAddress.ToString()),
                    new Claim(UserClaims.Address, user.Address != null ? user.Address.ToString() : ""),
                    new Claim(UserClaims.PhoneNumber, user.PhoneNumber != null ? user.PhoneNumber.ToString() : ""),
                    new Claim(UserClaims.RoleId, user.RoleId.ToString("d"))
                }),
                Expires = DateTime.UtcNow.AddMinutes(_jwtSettings.JwtTokenExpirationLimit),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private RefreshToken GenerateRefreshToken()
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    ExpiresAt = DateTime.UtcNow.AddDays(_jwtSettings.RefreshTokenExpirationLimit),
                    CreatedAt = DateTime.UtcNow
                };
            }
        }
    }
}
