﻿using System.ComponentModel.DataAnnotations;

namespace Common.Dtos
{
    class AuthenticationRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
