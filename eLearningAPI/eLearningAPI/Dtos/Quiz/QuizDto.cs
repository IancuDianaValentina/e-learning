﻿using eLearningAPI.Models.Quiz;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eLearningAPI.Dtos.Quiz
{
    public class QuizDto
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
        public List<Question> Questions { get; set; }
    }
}
