﻿CREATE TABLE [dbo].[QuestionType] (
    [Id]   TINYINT        NOT NULL,
    [Type] NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

