export class ChoiceVm {
    id: string;
    text: string;
    isAnswer: boolean;
    isEditMode: boolean;
    isAddMode: boolean;
}