﻿namespace eLearningAPI.Enums
{
    public enum UserSessionStatusEnum
    {
        NotStarted = 0,
        InProgress = 1,
        Completed = 2
    }
}
