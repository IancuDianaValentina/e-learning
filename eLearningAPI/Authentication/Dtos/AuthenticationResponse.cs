﻿using System;
using static Authentication.Enums.Enums;

namespace Authentication.Dtos
{
    public class AuthenticationResponse
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string JwtToken { get; set; }
        public string JwtRefreshToken { get; set; }
        public UserRoleEnum Role { get; set; }
    }
}
