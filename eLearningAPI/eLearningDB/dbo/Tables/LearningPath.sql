﻿CREATE TABLE [dbo].[LearningPath] (
    [Title]                 NVARCHAR (255) NOT NULL,
    [NoLearningHours]       SMALLINT       NOT NULL,
    [Description]           NVARCHAR (MAX) NULL,
    [ProgrammingLanguageId] TINYINT        NOT NULL,
    [LearningLevelId]       TINYINT        NOT NULL,
    [Id]                    SMALLINT       IDENTITY (1, 1) NOT NULL,
    [Tags]                  NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([LearningLevelId]) REFERENCES [dbo].[LearningLevel] ([Id]),
    FOREIGN KEY ([ProgrammingLanguageId]) REFERENCES [dbo].[ProgrammingLanguage] ([Id])
);



