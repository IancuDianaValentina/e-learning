﻿CREATE TABLE [dbo].[Quiz] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Title]     NVARCHAR (255) NOT NULL,
    [LessonId]  INT            NOT NULL,
    [UpdatedAt] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([LessonId]) REFERENCES [dbo].[Lesson] ([Id])
);

