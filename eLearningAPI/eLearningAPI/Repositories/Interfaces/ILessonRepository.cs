﻿using eLearningAPI.Models;
using eLearningAPI.Models.Lessons;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Interfaces
{
    public interface ILessonRepository
    {
        public Task DeleteLessonAsync(int lessonId);
        public Task<List<Lesson>> GetAllLessonsAsync();
        public Task<Lesson> GetLessonByIdAsync(int lessonId);
        public Task SaveLessonAsync(Lesson lesson);
        public Task<LessonLearningPath> GetLessonLearningPathAsync(int lessonId, int learningPathId);
    }
}
