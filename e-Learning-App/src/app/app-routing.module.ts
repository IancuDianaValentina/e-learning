import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './pages/authentication/authentication.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { LeaderboardComponent } from './pages/leaderboard/leaderboard.component';
import { LearningPathListComponent } from './pages/learning-path-list/learning-path-list.component';
import { LearningPathComponent } from './pages/learning-path/learning-path.component';
import { LessonListComponent } from './pages/lesson-list/lesson-list.component';
import { LessonComponent } from './pages/lesson/lesson.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { UserQuizComponent } from './pages/user-quiz/user-quiz.component';
import { ViewLearningPathComponent } from './pages/view-learning-path/view-learning-path.component';
import { ViewLessonComponent } from './pages/view-lesson/view-lesson.component';

const routes: Routes = [
  { path: 'login', component: AuthenticationComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'learning-paths', component: LearningPathListComponent },
  { path: 'lessons', component: LessonListComponent },
  { path: 'lesson/:id/edit', component: LessonComponent },
  { path: 'learning-path/:learningPathId/lesson/:lessonId/view', component: ViewLessonComponent },
  { path: 'lesson/add', component: LessonComponent },
  { path: 'learning-path/:learningPathId/lesson/:lessonId/quiz', component: UserQuizComponent },
  { path: 'learning-path/add', component: LearningPathComponent },
  { path: 'learning-path/:id/edit', component: LearningPathComponent },
  { path: 'learning-path/:id/view', component: ViewLearningPathComponent },
  { path: 'leaderboard', component: LeaderboardComponent },
  { path: 'profile', component: UserProfileComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
