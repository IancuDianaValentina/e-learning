import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationPopupAction } from "src/app/components/custom-popup/custom-popup.component";
import { UserSessionStatusEnum } from "src/app/enums";
import { LearningPath } from "src/app/models/learning-path/learning-path.model";
import { Lesson } from "src/app/models/lesson/lesson.model";
import { UserLessonSessionListItem } from "src/app/models/user-sessions/user-lesson-session-list-item.model";
import { CustomPopupService } from "src/app/services/custom-popup.service";
import { LearningPathService } from "src/app/services/learning-path.service";
import { SessionService } from "src/app/services/session.service";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'view-learning-path',
    templateUrl: './view-learning-path.component.html',
    styleUrls: ['./view-learning-path.component.scss']
})

export class ViewLearningPathComponent implements OnInit {

    learningPath: LearningPath = new LearningPath();
    userSessions: UserLessonSessionListItem[] = [];
    userPoints: number;
    userId: string;
    percentComplete: number = 0;
    percentInProgress: number = 0;

    constructor(private route: ActivatedRoute, private learningPathService: LearningPathService,
        private customPopupService: CustomPopupService, private userService: UserService,
        private webSessionService: SessionService, private router: Router) { }

    async ngOnInit() {
        const learningPathId = parseInt(this.route.snapshot.paramMap.get("id") ?? '0');
        this.userId = (await this.webSessionService.getUserSession())?.id;
        await this.loadData(learningPathId);
    }

    async loadData(learningPathId: number) {
        this.learningPath = await this.learningPathService.getLearningPath(learningPathId);
        this.userPoints = await this.userService.getUserPoints(this.userId);
        this.userSessions = await this.userService.getUserSessions(this.userId, this.learningPath.id);
        const nrCompleteSessions = this.userSessions?.filter(s => s.status == UserSessionStatusEnum.Completed)?.length;
        const nrInProgressSessions = this.userSessions?.filter(s => s.status == UserSessionStatusEnum.InProgress)?.length;
        this.percentComplete = (nrCompleteSessions * 100) / this.learningPath.lessons?.length;
        this.percentInProgress = (nrInProgressSessions * 100) / this.learningPath.lessons?.length;
    }

    async onClickLesson(lesson: Lesson) {
        const sessionExists: boolean = this.userSessions.some(l => l.lessonId == lesson.id);
        // TO-DO IMPLEMENT SOME KIND OF LESSON DEPENDENCY VALIDATION
        if (lesson.requiredPoints > this.userPoints && !sessionExists) {
            this.customPopupService.openPopup('Start lesson', ['You can not start with the lesson because you do not have the required amount of points',
                'You must wait until tommorow to receive the amount of points needed'], [{ text: 'Ok', action: ConfirmationPopupAction.Ok }]);
            return;
        }
        if (lesson.requiredPoints > 0 && !sessionExists) {
            var response = await this.customPopupService.openPopup('Start lesson', [`Starting this lesson will cause an automatic subtraction of ${lesson.requiredPoints} points from your account`,
                'Are you sure you want to start?'], [{ text: 'Ok', action: ConfirmationPopupAction.Ok }, { text: 'Cancel', action: ConfirmationPopupAction.Cancel }]);
            if (response == ConfirmationPopupAction.Cancel) {
                return;
            }
        }
        if (!sessionExists) {
            await this.userService.createUserSession(this.userId, this.learningPath.id, lesson.id, lesson.requiredPoints);
        }
        this.router.navigateByUrl(`learning-path/${this.learningPath.id}/lesson/${lesson.id}/view`);
    }
}
