import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService implements HttpInterceptor {
  constructor(private router: Router) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMessage = this.handleError(error);
          return throwError(errorMessage);
        })
      )
  }

  private handleError = (error: HttpErrorResponse): string => {
    if (error.status === 404) {
      return this.handleNotFound(error);
    }
    else if (error.status === 400) {
      return this.handleBadRequest(error);
    }
    else if (error.status === 401) {
      return this.handleUnauthorized(error);
    }
    return '';
  }

  private handleNotFound = (error: HttpErrorResponse) => {
    this.router.navigate(['/']);
    return '';
  }

  private handleBadRequest = (error: HttpErrorResponse): string => {
    if (error.error.errors) {
      let message = '';
      const values = Object.values(error.error.errors);
      values.map(m => {
        message += m + '<br>';
      })
      return message.slice(0, -4);
    }
    else {
      console.log(error)
      return error.error ? error.error : error.message;
    }
  }

  private handleUnauthorized = (error: HttpErrorResponse): string => {
    this.router.navigate(['/login']);
    return error.message;
  }
}