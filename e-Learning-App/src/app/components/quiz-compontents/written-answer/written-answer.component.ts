import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChoiceVm } from 'src/app/view-models/quiz/choice.view-model';

@Component({
    selector: 'written-answer',
    templateUrl: './written-answer.component.html',
    styleUrls: ['./written-answer.component.scss']
})

export class WrittenAnswerComponent implements OnInit {
    @Input() choices: Array<ChoiceVm> = [];
    @Input() isReadonly: boolean = false;
    @Output() choicesChange: EventEmitter<Array<ChoiceVm>> = new EventEmitter<Array<ChoiceVm>>();

    constructor() { }

    ngOnInit(): void {
        if (!this.choices) {
            this.choices = [];
        }
        if (this.choices.length == 0) {
            this.choices.push({
                text: 'Answer',
                isAnswer: true
            } as ChoiceVm)
        }
    }

    onFinish(event: any) {
        this.choicesChange.emit(this.choices);
    }
}
