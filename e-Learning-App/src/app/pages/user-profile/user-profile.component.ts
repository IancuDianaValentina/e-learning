import { Component, OnInit } from "@angular/core";
import { UserAchievement } from "src/app/models/user-sessions/user-achievement.model";
import { User } from "src/app/models/user.model";
import { SessionService } from "src/app/services/session.service";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})

export class UserProfileComponent implements OnInit {

    user: User;
    achievements: UserAchievement[];
    userId: string;
    charts = [];

    learningPathChartOptions = {
        animationEnabled: true,
        theme: "light2",
        exportEnabled: false,
        title: {
            text: "Complete/Incomplete"
        },
        subtitles: [{
            text: "Learning Java for Beginners"
        }],
        data: [{
            type: "pie",
            indexLabel: "{name}: {z}%",
            dataPoints: [
                { name: "Complete", y: 2, z: 85.72 },
                { name: "Incomplete", y: 1, z: 14.28 },
            ]
        }]
    }

    constructor(private webSessionService: SessionService, private userService: UserService) {
    }

    async ngOnInit() {
        this.userId = (await this.webSessionService.getUserSession())?.id;

        this.user = await this.userService.getUserById(this.userId);
        this.achievements = await this.userService.getAllUserAchievements(this.userId);
        this.charts.push({
            animationEnabled: true,
            theme: "light2",
            exportEnabled: false,
            title: {
                text: "Answers ratio"
            },
            subtitles: [{
                text: "Ratio between correct and incorrect answers"
            }],
            data: [{
                type: "pie",
                indexLabel: "{name}: {z}%",
                dataPoints: [
                    { name: "Correct", y: this.user.totalCorrectAnswered, z: (this.user.totalCorrectAnswered * 100) / this.user.totalAnswered },
                    { name: "Incorrect", y: (this.user.totalAnswered - this.user.totalCorrectAnswered), z: ((this.user.totalAnswered - this.user.totalCorrectAnswered) * 100) / this.user.totalAnswered },
                ]
            }]
        })
    }
}
