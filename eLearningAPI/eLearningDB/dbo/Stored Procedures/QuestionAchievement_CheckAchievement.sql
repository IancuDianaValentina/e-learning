﻿CREATE PROCEDURE QuestionAchievement_CheckAchievement
	@UserId UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @AchievementId TINYINT = NULL;
	SELECT @AchievementId = AchievementId 
							FROM QuestionAchievement QA
							INNER JOIN Users U
								ON U.TotalCorrectAnswered = QA.NrCorrectAnswers
							WHERE U.Id = @UserId;
	IF @AchievementId IS NOT NULL
		INSERT INTO UserAchievement VALUES (@AchievementId, @UserId, GETUTCDATE());
END