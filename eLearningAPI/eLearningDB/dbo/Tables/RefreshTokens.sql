﻿CREATE TABLE [dbo].[RefreshTokens] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [Token]          NVARCHAR (MAX)   NOT NULL,
    [ExpirationDate] DATETIME2 (7)    NULL,
    [CreatedAt]      DATETIME2 (7)    NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

