﻿
CREATE PROCEDURE Questions_DeleteByQuizId 
(
	@QuizId INT
)
AS
BEGIN
	DELETE FROM Question
	WHERE QuizId = @QuizId;
END