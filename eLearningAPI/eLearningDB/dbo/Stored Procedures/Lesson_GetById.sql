﻿
CREATE PROCEDURE [dbo].[Lesson_GetById]
(
	@LessonId INT
)
AS
BEGIN
	SELECT L.Id,
		L.HtmlContent as Content,
		L.Title,
		L.LearningTime,
		L.RequiredPoints,
		L.ProgrammingLanguageId as ProgrammingLanguage,
		LLP.LearningPathId,
		LLP.LessonIndex,
		LP.Id,
		LP.Title,
		LP.NoLearningHours,
		LP.[Description],
		LP.ProgrammingLanguageId as [ProgrammingLanguage],
		LP.LearningLevelId as [LearningLevel]
	FROM Lesson L
	LEFT JOIN LessonLearningPath LLP
		ON LLP.LessonId = L.Id
	LEFT JOIN LearningPath LP
		ON LP.Id = LLP.LearningPathId
	WHERE L.Id = @LessonId;
END