﻿using eLearningAPI.BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace eLearningAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly IQuestionBL _questionBL;
        public QuestionController(IQuestionBL questionBL)
        {
            _questionBL = questionBL;
        }

        [HttpGet("GetAllQuestionTypes")]
        public async Task<IActionResult> GetQuestionTypes()
        {
            var response = await _questionBL.GetQuestionTypesAsync();
            return Ok(response);
        }

        [HttpDelete("DeleteQuestionsByQuizId")]
        public async Task<IActionResult> DeleteQuestionsByQuizIdAsync(int quizId)
        {
            await _questionBL.DeleteQuestionsByQuizIdAsync(quizId);
            return Ok();
        }

        [HttpDelete("DeleteChoice")]
        public async Task<IActionResult> DeleteChoice(int choiceId)
        {
            await _questionBL.DeleteChoiceByIdAsync(choiceId);
            return Ok();
        }

        [HttpDelete("DeleteQuestion")]
        public async Task<IActionResult> DeleteQuestion(int questionId)
        {
            await _questionBL.DeleteChoiceByIdAsync(questionId);
            return Ok();
        }
    }
}
