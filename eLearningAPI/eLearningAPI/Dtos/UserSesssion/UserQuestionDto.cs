﻿using eLearningAPI.Dtos.Quiz;
using System;
using System.Collections.Generic;

namespace eLearningAPI.Dtos.UserSesssion
{
    public class UserQuestionDto
    {
        public Guid QuestionId { get; set; }
        public Guid UserId { get; set; }
        public List<ChoiceDto> Choices { get; set; }
        public Guid UserAnswerId { get; set; }
        public Guid UserChoiceId { get; set; }
        public bool CorrectlyAnswered { get; set; }
    }
}
