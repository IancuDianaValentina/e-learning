﻿CREATE PROCEDURE [dbo].[LearningPath_Save]
(
	@Id INT = NULL,
	@Title NVARCHAR(255),
	@Description NVARCHAR(MAX),
	@ProgrammingLanguage TINYINT,
	@LearningLevel TINYINT,
	@Tags NVARCHAR(MAX) = NULL
)
AS
BEGIN
	IF NOT EXISTS (
					SELECT *
					FROM LearningPath
					WHERE Id = @Id
					)
	INSERT INTO LearningPath 
	(
		Title,
		[Description],
		ProgrammingLanguageId,
		NoLearningHours,
		LearningLevelId,
		Tags
	)
	OUTPUT INSERTED.Id
	VALUES
	(
		@Title,
		@Description,
		@ProgrammingLanguage,
		0,
		@LearningLevel,
		@Tags
	)
	ELSE
	UPDATE LearningPath
	SET Title = @Title,
		[Description] = @Description,
		ProgrammingLanguageId = @ProgrammingLanguage,
		LearningLevelId = @LearningLevel,
		Tags = @Tags
	OUTPUT INSERTED.Id
	WHERE Id = @Id;
END