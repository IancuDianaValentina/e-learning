﻿using eLearningAPI.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eLearningAPI.Dtos
{
    public class LearningPathDto
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
        public int NoLearningHours { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public ProgrammingLanguageEnum ProgrammingLanguage { get; set; }
        [Required]
        public LearningLevelEnum LearningLevel { get; set; }
        public List<LessonDto> Lessons { get; set; }
    }
}
