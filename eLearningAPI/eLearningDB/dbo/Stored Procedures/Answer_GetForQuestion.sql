﻿CREATE PROCEDURE Answer_GetForQuestion
(
	@QuestionId UNIQUEIDENTIFIER
)
AS
BEGIN
	SELECT Choice.Id,
		Choice.QuestionId,
		Choice.[Text]
	FROM Question
	INNER JOIN Choice	
		ON Choice.QuestionId = Question.Id
	WHERE Choice.IsAnswer = 1 AND
		Question.Id = @QuestionId;
END