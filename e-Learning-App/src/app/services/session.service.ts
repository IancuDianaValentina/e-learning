import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { AuthenticationRequest } from "../models/authentication-request.model";
import { lastValueFrom, Observable } from "rxjs";
import { UserSession } from "../models/user-session.model";
import { CookieService } from "ngx-cookie-service";
import { RefreshTokenRequest } from "../models/refresh-token-request.model";
import { Constants } from "../models/constants.model";

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private authUrl: string = '';
  private userSession: UserSession;
  private noInterceptParams = {
    [Constants.NoIntercept]: "true"
  }

  constructor(private configService: ConfigService, private cookieService: CookieService, private httpClient: HttpClient) {
    this.authUrl = this.configService.getUrl("authApiUrl");
  }

  authenticateUser(authReq: AuthenticationRequest): Observable<UserSession> {
    return this.httpClient.post<UserSession>(this.authUrl + '/authentication/authenticate', authReq, { params: this.noInterceptParams });
  }

  isTokenExpired(token: string): boolean {
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime() / 1000)) >= expiry;
  }

  async getUserSession(): Promise<UserSession> {
    const refreshToken = this.cookieService.get('refreshToken');
    if (this.shouldRefreshUserToken()) {
      return lastValueFrom(this.httpClient.post<UserSession>(this.authUrl + '/authentication/refreshJwtToken', { jwtRefreshToken: refreshToken } as RefreshTokenRequest, { params: this.noInterceptParams }));
    } else {
      return this.userSession;
    }
  }

  shouldRefreshUserToken(): boolean {
    if (!this.userSession || this.isTokenExpired(this.userSession.jwtToken)) {
      return true;
    }
    return false;
  }

  setUserSession(userSession: UserSession): void {
    this.userSession = userSession;
  }

  logout(): void {
    this.cookieService.delete('refreshToken');
  }
}