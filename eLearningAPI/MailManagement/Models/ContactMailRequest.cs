﻿namespace MailManagement.Models
{
    public class ContactMailRequest : BaseMailRequest
    {
        public string SenderAddress { get; set; }
        public string SenderName { get; set; }
    }
}
