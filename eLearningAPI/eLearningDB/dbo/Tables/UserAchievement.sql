﻿CREATE TABLE [dbo].[UserAchievement] (
    [AchievementId] TINYINT          NOT NULL,
    [UserId]        UNIQUEIDENTIFIER NOT NULL,
    [AchievedAt]    DATETIME2 (7)    NULL,
    FOREIGN KEY ([AchievementId]) REFERENCES [dbo].[Achievement] ([Id]),
    FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

