export class User {
    username: string;
    firstName: string;
    lastName: string;
    emailAddress: string;
    phoneNumber: string;
    address: string;
    password: string;
    points: number;
    totalAnswered: number;
    totalCorrectAnswered: number;
}