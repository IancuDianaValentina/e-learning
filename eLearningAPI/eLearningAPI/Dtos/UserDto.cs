﻿using eLearningAPI.Attributes;
using System.ComponentModel.DataAnnotations;
using static Authentication.Enums.Enums;

namespace eLearningAPI.Models
{
    public class UserDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [Password(8)]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        public UserRoleEnum? RoleId { get; set; }
    }
}
