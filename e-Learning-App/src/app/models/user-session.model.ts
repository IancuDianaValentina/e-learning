import { UserRoleEnum } from "../enums";

export class UserSession {
    id: string;
    username: string;
    role: UserRoleEnum;
    jwtToken: string;
    jwtRefreshToken: string;
}