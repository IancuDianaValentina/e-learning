﻿namespace Common.Dtos
{
    class AuthenticationResponse
    {
        public string Username { get; set; }
        public string JwtToken { get; set; }
        public string Role { get; set; }
    }
}
