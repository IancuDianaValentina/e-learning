﻿CREATE TABLE [dbo].[Users] (
    [Id]                   UNIQUEIDENTIFIER NOT NULL,
    [FirstName]            NVARCHAR (50)    NOT NULL,
    [LastName]             NVARCHAR (50)    NOT NULL,
    [Username]             NVARCHAR (50)    NOT NULL,
    [PasswordHash]         NVARCHAR (MAX)   NOT NULL,
    [EmailAddress]         NVARCHAR (50)    NOT NULL,
    [PhoneNumber]          NVARCHAR (13)    NULL,
    [Address]              NVARCHAR (150)   NULL,
    [RefreshTokenId]       UNIQUEIDENTIFIER NULL,
    [RoleId]               TINYINT          NULL,
    [Points]               INT              DEFAULT ((0)) NULL,
    [TotalAnswered]        INT              DEFAULT ((0)) NOT NULL,
    [TotalCorrectAnswered] INT              DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([RefreshTokenId]) REFERENCES [dbo].[RefreshTokens] ([Id]),
    FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([Id])
);



