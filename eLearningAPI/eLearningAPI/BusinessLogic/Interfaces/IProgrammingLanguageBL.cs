﻿using eLearningAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Interfaces
{
    public interface IProgrammingLanguageBL
    {
        public Task<List<ProgrammingLanguage>> GetAllProgrammingLanguagesAsync();
    }
}
