﻿using System;

namespace eLearningAPI.Models.Quiz
{
    public class Choice
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public string Text { get; set; }
        public bool IsAnswer { get; set; }
    }
}
