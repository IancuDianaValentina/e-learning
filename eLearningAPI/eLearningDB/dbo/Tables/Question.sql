﻿CREATE TABLE [dbo].[Question] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [QuizId]       INT              NOT NULL,
    [Type]         TINYINT          NOT NULL,
    [Text]         NVARCHAR (MAX)   NOT NULL,
    [RewardPoints] INT              NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([QuizId]) REFERENCES [dbo].[Quiz] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ([Type]) REFERENCES [dbo].[QuestionType] ([Id])
);

