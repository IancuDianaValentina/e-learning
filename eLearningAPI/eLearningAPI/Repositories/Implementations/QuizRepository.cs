﻿using Dapper;
using eLearningAPI.Models.Quiz;
using eLearningAPI.Models.UserSession;
using eLearningAPI.Repositories.Interfaces;
using eLearningAPI.Utils;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Implementations
{
    public class QuizRepository: BaseRepository, IQuizRepository
    {
        private static readonly string GetQuizByLessonIdSP = "Quiz_GetByLessonId";
        private static readonly string SaveUserAnswerSP = "UserAnswer_Save";
        private static readonly string GetUserAnswersSP = "Answer_GetForQuestion";
        public QuizRepository(IConfiguration configuration): base(configuration) { }

        public async Task<Quiz> GetQuizByLessonIdAsync(int lessonId)
        {
            using (var connection = Connection)
            {
                var quizDictionary = new Dictionary<int, Quiz>();
                return (await connection.QueryAsync<Quiz, Question, Choice, Quiz>(GetQuizByLessonIdSP,
                    (Quiz quiz, Question question, Choice choice) =>
                    {
                        Quiz quizEntry;
                        if (!quizDictionary.TryGetValue(quiz.Id, out quizEntry))
                        {
                            quizEntry = quiz;
                            quizEntry.Questions = new List<Question>();
                            quizDictionary.Add(quizEntry.Id, quizEntry);
                        }
                        
                        if (!quizEntry.Questions.Exists(q => q.Id == question.Id))
                        {
                            quizEntry.Questions.Add(question);
                        }

                        var index = quizEntry.Questions.FindIndex(q => q.Id == choice.QuestionId);
                        if (index != -1)
                        {
                            if (quizEntry.Questions[index].Choices == null)
                            {
                                quizEntry.Questions[index].Choices = new List<Choice>();
                            }
                            quizEntry.Questions[index].Choices.Add(choice);
                        }
                        return quizEntry;
                    },
                    splitOn: "Id,Id,Id",
                    param: new { LessonId = lessonId },
                    commandType: System.Data.CommandType.StoredProcedure)).Distinct().FirstOrDefault();
            }
        }

        public async Task<Guid> SaveUserAnswerAsync(UserAnswer answer)
        {
            var choiceIds = RepositoryUtils.ToGuidList(answer.ChoiceIds);
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<Guid>(SaveUserAnswerSP,
                    param: new
                    {
                        Id = answer.Id,
                        UserSessionId = answer.UserSessionId,
                        QuestionId = answer.QuestionId,
                        ChoiceIds = choiceIds,
                        CorrectlyAnswered = answer.CorrectlyAnswered
                    },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<List<Choice>> GetAnswersByQuestionIdAsync(Guid questionId)
        {
            using (var connection = Connection)
            {
                return await connection.QueryAsync<Choice>(GetUserAnswersSP,
                    param: new
                    {
                        QuestionId = questionId,
                    },
                    commandType: System.Data.CommandType.StoredProcedure) as List<Choice>;
            }
        }

    }
}
