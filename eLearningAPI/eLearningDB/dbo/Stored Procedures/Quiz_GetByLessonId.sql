﻿CREATE PROCEDURE [dbo].[Quiz_GetByLessonId]
(
	@LessonId INT
)
AS
BEGIN
	SELECT Q.Id,
		Q.Title,
		Question.Id,
		Question.[Type] as QuestionType,
		Question.[Text],
		Question.RewardPoints,
		C.Id,
		C.QuestionId,
		C.[Text],
		C.IsAnswer
	FROM Quiz Q 
	LEFT JOIN Question
		ON Q.Id = Question.QuizId
	INNER JOIN Choice C
		ON Question.Id = C.QuestionId
	WHERE Q.LessonId = @LessonId;
END