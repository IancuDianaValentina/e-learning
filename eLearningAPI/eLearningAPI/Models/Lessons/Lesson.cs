﻿using eLearningAPI.Enums;
using System.Collections.Generic;

namespace eLearningAPI.Models{
    public class Lesson
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public int LearningTime { get; set; }
        public int RequiredPoints { get; set; }
        public ProgrammingLanguageEnum ProgrammingLanguage { get; set; }
        public List<LearningPathLink> LearningPathLinks { get; set; }
        public List<LearningPath> LearningPaths { get; set; }
        public Quiz.Quiz Quiz { get; set; }
        public List<Lesson> DependerLessons { get; set; }
        public List<Lesson> DependeeLessons { get; set; }
    }
}
