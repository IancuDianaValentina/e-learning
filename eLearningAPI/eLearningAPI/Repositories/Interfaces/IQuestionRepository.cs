﻿using eLearningAPI.Models.Quiz.Questions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Interfaces
{
    public interface IQuestionRepository
    {
        public Task<List<QuestionType>> GetQuestionTypesAsync();
        public Task DeleteQuestionsByQuizIdAsync(int quizId);
        public Task DeleteChoiceByIdAsync(int choideId);
        public Task DeleteQuestionByIdAsync(int questionId);
    }
}
