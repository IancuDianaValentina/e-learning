import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CustomPopupService } from 'src/app/services/custom-popup.service';
import { LearningPathService } from 'src/app/services/learning-path.service';
import { LearningPathVm } from 'src/app/view-models/learning-path.view-model';
import { ConfirmationPopupAction } from '../custom-popup/custom-popup.component';


export enum ListItemSymbolType {
    Bullet = 1,
    Arrow = 2
}

@Component({
    selector: 'learning-path-table',
    templateUrl: './learning-path-table.component.html',
    styleUrls: ['./learning-path-table.component.scss']
})

export class LearningPathTableComponent implements OnInit, OnChanges {
    displayedColumns: string[] = [
        "line",
        "id",
        "title",
        "noLearningHours",
        "learningLevel",
        "actions"
    ];
    @Input() learningPaths: LearningPathVm[] = [];
    @Input() lessonId: number;
    @Output() onReload: EventEmitter<any> = new EventEmitter<any>();
    @Input() listItemSymbolType: ListItemSymbolType = ListItemSymbolType.Bullet;
    learningPathsDataSource: MatTableDataSource<LearningPathVm> = new MatTableDataSource<LearningPathVm>();
    listItemSymbolTypeEnum = ListItemSymbolType;

    constructor(private learningPathService: LearningPathService, private popupService: CustomPopupService, private router: Router) { }

    ngOnInit() {
        this.learningPathsDataSource = new MatTableDataSource<LearningPathVm>(this.learningPaths);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['learningPaths']) {
            this.learningPathsDataSource = new MatTableDataSource<LearningPathVm>(this.learningPaths);
        }
    }

    async onUnlinkLearningPath(learningPathId: number) {
        var response = await this.popupService.openPopup('Unlink learning path', ['Are you sure you want to unlink this learning path?'],
            [{ text: 'Ok', action: ConfirmationPopupAction.Ok }, { text: 'Cancel', action: ConfirmationPopupAction.Cancel }]);
        if (response == ConfirmationPopupAction.Ok) {
            await this.learningPathService.unlinkLessonFromLearningPath(this.lessonId, learningPathId);
            this.onReload.emit();
        }
    }

    onEditLearningPath(learningPathId: number) {
        this.router.navigateByUrl(`/learning-path/${learningPathId}/edit`);
    }
}
