﻿using eLearningAPI.Enums;
using System.Collections.Generic;

namespace eLearningAPI.Models
{
    public class LearningPath
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int NoLearningHours { get; set; }
        public string Description { get; set; }
        public ProgrammingLanguageEnum ProgrammingLanguage { get; set; }
        public LearningLevelEnum LearningLevel { get; set; }
        public List<Lesson> Lessons { get; set; }
    }
}
