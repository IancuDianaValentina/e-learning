import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { QuestionService } from 'src/app/services/question.service';
import { ChoiceVm } from 'src/app/view-models/quiz/choice.view-model';

@Component({
    selector: 'single-choice-config',
    templateUrl: './single-choice-config.component.html',
    styleUrls: ['./single-choice-config.component.scss']
})

export class SingleChoiceConfigComponent implements OnInit {
    choiceTextControl: FormControl;
    anyChoiceInAddOrEditMode: boolean = false;
    @Input() choices: Array<ChoiceVm> = [];
    @Input() isReadonly: boolean = false;
    @Output() choicesChange: EventEmitter<Array<ChoiceVm>> = new EventEmitter<Array<ChoiceVm>>();
    @Output() onChoiceModeChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private questionService: QuestionService) { }

    ngOnInit(): void {
        this.choiceTextControl = new FormControl('', [Validators.required, this.uniqueTextValidator()]);
        if (this.choices?.length == 0) {
            this.choices = [
                { text: 'Choice 1', isAnswer: false, isEditMode: false, isAddMode: false } as ChoiceVm,
                { text: 'Choice 2', isAnswer: false, isEditMode: false, isAddMode: false } as ChoiceVm
            ]
        }
    }

    onAddChoice() {
        this.choiceTextControl.reset();
        this.choiceTextControl.setValue('Choice ' + (this.choices.length + 1));
        this.choices.push({
            text: this.choiceTextControl.value,
            isAddMode: true
        } as ChoiceVm);
        this.anyChoiceInAddOrEditMode = true;
        this.onChoiceModeChanged.emit(this.anyChoiceInAddOrEditMode);
    }

    onCancel(choice: ChoiceVm) {
        this.anyChoiceInAddOrEditMode = false;
        this.onChoiceModeChanged.emit(this.anyChoiceInAddOrEditMode);
        if (choice.isAddMode) {
            this.choices = this.choices.filter(c => !c.isAddMode);
        } else {
            choice.isEditMode = false;
        }
    }

    onChoiceClick(choice: ChoiceVm) {
        if (!this.anyChoiceInAddOrEditMode) {
            this.choices.forEach(c => c.isAnswer = c != choice ? false : !c.isAnswer);
            this.choicesChange.emit(this.choices);
        }
    }

    onStartEdit(choice: ChoiceVm) {
        choice.isEditMode = true;
        choice.isAddMode = false;
        this.anyChoiceInAddOrEditMode = true;
        this.onChoiceModeChanged.emit(this.anyChoiceInAddOrEditMode);
    }

    onFinish(choice: ChoiceVm) {
        choice.isAddMode = false;
        choice.isEditMode = false;
        this.anyChoiceInAddOrEditMode = false;
        this.onChoiceModeChanged.emit(this.anyChoiceInAddOrEditMode);
        this.choicesChange.emit(this.choices);
    }

    async onDelete(choice: ChoiceVm) {
        if (choice.id) {
            await this.questionService.deleteChoice(choice.id);
        } else {
            this.choices = this.choices.filter(c => c != choice);
        }
        this.choicesChange.emit(this.choices);
    }

    uniqueTextValidator(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            var isDuplicate = this.choices.find(c => !c.isAddMode && !c.isEditMode &&
                this.anyChoiceInAddOrEditMode && control?.value &&
                c.text.trim().toLowerCase() == control?.value?.trim()?.toLowerCase());
            return isDuplicate ? { sameText: 'Choices must have unique texts' } : null;
        }
    }
}
