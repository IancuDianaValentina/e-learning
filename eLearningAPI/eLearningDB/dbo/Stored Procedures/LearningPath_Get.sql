﻿
CREATE PROCEDURE [dbo].[LearningPath_Get]
(
	@LearningPathId INT
)
AS
BEGIN
	SELECT LP.Id,
		LP.Title,
		LP.NoLearningHours,
		LP.[Description],
		LP.ProgrammingLanguageId AS [ProgrammingLanguage],
		LP.LearningLevelId AS [LearningLevel],
		L.Id,
		L.HtmlContent as [Content],
		L.Title,
		L.LearningTime,
		L.RequiredPoints,
		L.ProgrammingLanguageId as [ProgrammingLanguage]
	FROM LearningPath LP
	INNER JOIN LessonLearningPath LLP
		ON LLP.LearningPathId = LP.Id
	INNER JOIN Lesson L
		ON L.Id = LLP.LessonId
	WHERE LP.Id = @LearningPathId
		ORDER BY LLP.LessonId ASC;
END