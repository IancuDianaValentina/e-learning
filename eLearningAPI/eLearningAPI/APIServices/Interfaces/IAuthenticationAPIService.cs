﻿using Authentication.Dtos;
using System.Threading.Tasks;

namespace eLearningAPI.APIServices.Interfaces
{
    public interface IAuthenticationAPIService
    {
        public Task<AuthenticationResponse> AuthenticateUserAsync(string username, string password);
    }
}
