﻿CREATE TYPE [dbo].[Choice] AS TABLE (
    [Id]         UNIQUEIDENTIFIER NULL,
    [QuestionId] UNIQUEIDENTIFIER NULL,
    [Text]       NVARCHAR (MAX)   NULL,
    [IsAnswer]   BIT              NULL);

