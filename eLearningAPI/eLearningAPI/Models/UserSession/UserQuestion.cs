﻿using eLearningAPI.Models.Quiz;
using System;
using System.Collections.Generic;

namespace eLearningAPI.Models.UserSession
{
    public class UserQuestion
    {
        public Guid QuestionId { get; set; }
        public Guid UserId { get; set; }
        public List<Choice> Choices { get; set; }
        public Guid UserAnswerId { get; set; }
        public Guid UserChoiceId { get; set; }
        public bool CorrectlyAnswered { get; set; }
    }
}
