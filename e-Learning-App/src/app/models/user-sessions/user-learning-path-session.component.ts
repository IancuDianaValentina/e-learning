import { UserSessionStatusEnum } from "src/app/enums";
import { UserLessonSession } from "./user-lesson-lession.component";

export class UserLearningPathSession {
    id: string;
    userId: string;
    learningPathId: number;
    status: UserSessionStatusEnum;
    userLessonSession: Array<UserLessonSession>;
}