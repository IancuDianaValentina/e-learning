﻿using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Dtos;
using eLearningAPI.Extensions;
using eLearningAPI.Models;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Implementations
{
    public class LessonBL: ILessonBL
    {
        private readonly ILessonRepository _lessonRepository;
        private readonly IQuizRepository _quizRepository;
        private readonly ILogger<LessonBL> _logger;
        public LessonBL(ILessonRepository lessonRepository, IQuizRepository quizRepository, ILogger<LessonBL> logger) {
            _lessonRepository = lessonRepository;
            _quizRepository = quizRepository;
            _logger = logger;
        }

        public async Task DeleteLessonAsync(int lessonId)
        {
            _logger.LogInformation("Deleting lesson {0}", lessonId);
            try
            {
                await _lessonRepository.DeleteLessonAsync(lessonId);
                _logger.LogInformation("Successfully deleted lesson {0}.", lessonId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error deleting lesson.");
                throw new Exception("Error deleting lesson.");
            }
        }
        public async Task<List<LessonDto>> GetAllLessonsAsync()
        {
            _logger.LogInformation("Retrieving all lessons.");
            try
            {
                var models = await _lessonRepository.GetAllLessonsAsync();
                _logger.LogInformation("Successfully retrieved all lessons.");
                return models.Select(m => m.ToDto()).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving all lessons.");
                throw new Exception("Error retrieving all lessons.");
            }
        }

        public async Task<LessonDto> GetLessonWithQuizByIdAsync(int lessonId)
        {
            _logger.LogInformation("Retrieving lesson {0}.", lessonId);
            try
            {
                var lesson = await _lessonRepository.GetLessonByIdAsync(lessonId);
                var quiz = await _quizRepository.GetQuizByLessonIdAsync(lessonId);
                lesson.Quiz = quiz;
                _logger.LogInformation("Successfully retrieved lesson {0}.", lessonId);
                return lesson.ToDto();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving lesson.");
                throw new Exception("Error retrieving lesson.");
            }
        }
        public async Task SaveLessonAsync(Lesson lesson)
        {
            _logger.LogInformation("Adding lesson");
            try
            {
                await _lessonRepository.SaveLessonAsync(lesson);
                _logger.LogInformation("Successfully saved lesson");
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error saving lesson.");
                throw new Exception("Error saving lesson.");
            }
        }

        public async Task<LessonLearningPathDto> GetLessonLearningPathAsync(int lessonId, int learningPathId)
        {
            _logger.LogInformation("Get learning path lesson for lesson id {0} and learning path id {1}", lessonId, learningPathId);
            try
            {
                var model = await _lessonRepository.GetLessonLearningPathAsync(lessonId, learningPathId);
                _logger.LogInformation("Successfully retrieved learning path lesson for lesson id {0} and learning path id {1}", lessonId, learningPathId);
                return model?.ToDto();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving learning path lesson for lesson id {0} and learning path id {1}.", lessonId, learningPathId);
                throw new Exception("Error retrieving learning path.");
            }
        }
    }
}
