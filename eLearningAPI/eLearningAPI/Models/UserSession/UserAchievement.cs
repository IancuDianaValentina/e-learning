﻿using System;

namespace eLearningAPI.Models.UserSession
{
    public class UserAchievement
    {
        public Guid UserId { get; set; }
        public int AchievementId { get; set; }
        public string AchievementName { get; set; }
        public string AchievementDescription { get; set; }
        public DateTime AchievedAt { get; set; }
    }
}
