﻿using System;

namespace eLearningAPI.Models
{
    public class LeaderboardUser
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int CorrectAnswers { get; set; }
        public int UserPoints { get; set; }
    }
}
