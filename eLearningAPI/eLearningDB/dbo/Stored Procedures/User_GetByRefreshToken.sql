﻿
CREATE PROCEDURE [dbo].[User_GetByRefreshToken] (
	@RefreshToken NVARCHAR(MAX) 
)
AS
BEGIN
	SELECT TOP 1 u.*, r.ExpirationDate as RefreshTokenExpirationTime, r.Token as RefreshToken, ro.Name as RoleName
	FROM [dbo].[Users] AS u
		INNER JOIN [dbo].[RefreshTokens] AS r ON u.RefreshTokenId = r.Id
		INNER JOIN [dbo].[Roles] AS ro ON u.RoleId = ro.Id
	WHERE r.Token = @RefreshToken
END