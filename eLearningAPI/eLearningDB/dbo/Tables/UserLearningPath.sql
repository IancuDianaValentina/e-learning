﻿CREATE TABLE [dbo].[UserLearningPath] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [UserId]         UNIQUEIDENTIFIER NOT NULL,
    [LearningPathId] SMALLINT         NOT NULL,
    [Status]         TINYINT          DEFAULT ((1)) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([LearningPathId]) REFERENCES [dbo].[LearningPath] ([Id]),
    FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

