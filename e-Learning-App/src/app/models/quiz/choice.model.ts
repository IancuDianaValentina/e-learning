export class Choice {
    id: string;
    questionId: string;
    text: string;
    isAnswer: boolean;
}