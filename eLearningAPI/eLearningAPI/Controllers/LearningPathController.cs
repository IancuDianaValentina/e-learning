﻿using Authentication.Attributes;
using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static Authentication.Enums.Enums;

namespace eLearningAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LearningPathController : ControllerBase
    {
        private readonly ILearningPathBL _learningPathBL;
        public LearningPathController(ILearningPathBL learningPathBL)
        {
            _learningPathBL = learningPathBL;
        }

        [HttpGet("getAllLearningPaths")]
        public async Task<IActionResult> GetAllLearningPaths()
        {
            var result = await _learningPathBL.GetAllLearningPathsAsync();
            return Ok(result);
        }
        
        [HttpGet("GetLearningPath/{learningPathId}")]
        //[Authorize(UserRoleEnum.User, UserRoleEnum.Administrator)]
        public async Task<IActionResult> GetLearningPath(int learningPathId)
        {
            var result = await _learningPathBL.GetLearningPathAsync(learningPathId);
            return Ok(result);
        }

        [HttpGet("getByProgrammingLanguage/{programmingLanguageId}")]
        //[Authorize(UserRoleEnum.Administrator)]
        public async Task<IActionResult> GetByProgrammingLanguage(int programmingLanguageId)
        {
            var result = await _learningPathBL.GetByProgrammingLanguageAsync((Enums.ProgrammingLanguageEnum) programmingLanguageId);
            return Ok(result);
        }

        [HttpPost("SaveLearningPath")]
        //[Authorize(UserRoleEnum.Administrator)]
        public async Task<IActionResult> SaveLearningPath([FromBody] LearningPathDto learningPath)
        {
            var response = await _learningPathBL.SaveLearningPathAsync(learningPath);
            return Ok(response);
        }

        [HttpPost("UnlinkLessonFromLearningPath")]
        //[Authorize(UserRoleEnum.Administrator)]
        public async Task<IActionResult> UnlinkLessonFromLearningPath([FromBody] UnlinkLessonRequest unlinkLessonRequest)
        {
            await _learningPathBL.UnlinkLessonFromLearningPathAsync(unlinkLessonRequest.LessonId, unlinkLessonRequest.LearningPathId);
            return Ok();
        }
    }
}
