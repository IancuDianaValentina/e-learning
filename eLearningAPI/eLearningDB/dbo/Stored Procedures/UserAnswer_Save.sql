﻿CREATE PROCEDURE [dbo].[UserAnswer_Save]
(
	@Id UNIQUEIDENTIFIER,
	@UserSessionId UNIQUEIDENTIFIER,
	@QuestionId UNIQUEIDENTIFIER,
	@ChoiceIds GuidList READONLY,
	@CorrectlyAnswered BIT
)
AS
BEGIN
	INSERT INTO UserSessionAnswer(Id, UserSessionId, QuestionId, CorrectlyAnswered, AnsweredAt)
						VALUES(@Id, @UserSessionId, @QuestionId, @CorrectlyAnswered, GETUTCDATE());

	INSERT INTO AnswerChoice (Id, ChoiceId, AnswerId)
	SELECT NEWID(), Id, @Id
	FROM @ChoiceIds;

	DECLARE @RewardPoints INT;
	SELECT TOP 1 @RewardPoints = Question.RewardPoints
		FROM Question
		WHERE Id = @QuestionId;

	DECLARE @UserId UNIQUEIDENTIFIER;
	SELECT TOP 1 @UserId = UL.UserId 
							FROM UserLesson UL 
							WHERE UL.Id = @UserSessionId;

	IF @CorrectlyAnswered = 1
		BEGIN
			UPDATE U
			SET Points = Points + @RewardPoints,
				TotalAnswered = TotalAnswered + 1,
				TotalCorrectAnswered = TotalCorrectAnswered + 1
			FROM Users U
			INNER JOIN UserLesson UL
				ON U.Id = UL.UserId
			WHERE UL.Id = @UserSessionId;
			EXECUTE QuestionAchievement_CheckAchievement @UserId;
		END
	ELSE
		UPDATE U
		SET TotalAnswered = TotalAnswered + 1
		FROM Users U
		INNER JOIN UserLesson UL
			ON U.Id = UL.UserId
		WHERE UL.Id = @UserSessionId;

	SELECT @Id;
END