﻿using Authentication.Models;
using Authentication.Repositories.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace Authentication.Repositories.Implementations
{
    public class UserRepository: BaseRepository, IUserRepository
    {
        private readonly string GetUserByUsernameSP = "User_GetByUsername";
        private readonly string GetUserByRefreshTokenSP = "User_GetByRefreshToken";
        private readonly string AddRefreshTokenToUserSP = "User_AddRefreshToken";
        public UserRepository(IConfiguration configuration): base(configuration)
        {

        }

        public async Task<UserDetails> GetUserByUsernameAsync(string username) 
        {
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<UserDetails>(GetUserByUsernameSP,
                    param: new { Username = username },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<UserDetails> GetUserByRefreshTokenAsync(string refreshToken)
        {
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<UserDetails>(GetUserByRefreshTokenSP,
                    param: new { RefreshToken = refreshToken },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task AddRefreshTokenToUserAsync(RefreshToken refreshToken, Guid userId)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(AddRefreshTokenToUserSP,
                    param: new {
                        RefreshToken = refreshToken.Token, 
                        CreatedAt = refreshToken.CreatedAt,
                        ExpirationDate = refreshToken.ExpiresAt,
                        UserId = userId
                    },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }
    }
}
