﻿using eLearningAPI.Dtos.UserSesssion;
using eLearningAPI.Models.UserSession;
using System.Linq;

namespace eLearningAPI.Extensions
{
    public static class UserSessionExtension
    {
        public static UserLessonSessionDto ToDto(this UserLessonSession model)
        {
            return new UserLessonSessionDto
            {
                Id = model.Id,
                UserId = model.UserId,
                LessonId = model.LessonId,
                LearningPathId = model.LearningPathId,
                Status = model.Status,
                QuizId = model.QuizId,
                UserAnswers = model.UserAnswers?.Select(answer => answer.ToDto()).ToList()
            };
        }

        public static UserLessonSessionListItem ToListItemDto(this UserLessonSession model)
        {
            return new UserLessonSessionListItem
            {
                Id = model.Id,
                UserId = model.UserId,
                LessonId = model.LessonId,
                LearningPathId = model.LearningPathId,
                Status = model.Status
            };
        }
        public static UserAnswerDto ToDto(this UserAnswer model)
        {
            return new UserAnswerDto
            {
                Id = model.Id,
                UserSessionId = model.UserSessionId,
                QuestionId = model.QuestionId,
                ChoiceIds = model.ChoiceIds,
                CorrectlyAnswered = model.CorrectlyAnswered
            };
        }

        public static UserAnswer ToModel(this UserAnswerDto dto)
        {
            return new UserAnswer
            {
                Id = dto.Id,
                UserSessionId = dto.UserSessionId,
                QuestionId = dto.QuestionId,
                ChoiceIds = dto.ChoiceIds,
                CorrectlyAnswered = dto.CorrectlyAnswered
            };
        }
    }
}
