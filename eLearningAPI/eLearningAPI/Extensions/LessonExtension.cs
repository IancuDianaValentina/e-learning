﻿using eLearningAPI.Dtos;
using eLearningAPI.Models;
using eLearningAPI.Models.Lessons;
using System.Linq;

namespace eLearningAPI.Extensions
{
    public static class LessonExtension
    {
        public static LessonDto ToDto(this Lesson model)
        {
            return new LessonDto
            {
                Id = model.Id,
                Title = model.Title,
                Content = model.Content,
                LearningTime = model.LearningTime,
                RequiredPoints = model.RequiredPoints,
                ProgrammingLanguage = model.ProgrammingLanguage,
                LearningPathLinks = model.LearningPathLinks,
                LearningPaths = model.LearningPaths?.Select(l => l.ToDto()).ToList(),
                Quiz = model.Quiz
            };
        }
        public static Lesson ToModel(this LessonDto dto)
        {
            return new Lesson
            {
                Id = dto.Id,
                Title = dto.Title,
                Content = dto.Content,
                LearningTime = dto.LearningTime,
                RequiredPoints = dto.RequiredPoints,
                ProgrammingLanguage = dto.ProgrammingLanguage
            };
        }

        public static LessonLearningPathDto ToDto(this LessonLearningPath model)
        {
            return new LessonLearningPathDto
            {
                LessonId = model.LessonId,
                Title = model.Title,
                Content = model.Content,
                LearningTime = model.LearningTime,
                RequiredPoints = model.RequiredPoints,
                ProgrammingLanguage = model.ProgrammingLanguage,
                LessonIndex = model.LessonIndex,
                LearningPathId = model.LearningPathId,
                LearningPathTitle = model.LearningPathTitle,
                NextLesson = model.NextLesson?.ToDto()
            };
        }
    }
}
