﻿
CREATE PROCEDURE [dbo].[User_AddRefreshToken]
(
	@RefreshToken NVARCHAR(MAX),
	@CreatedAt DATETIME2,
	@ExpirationDate DATETIME2,
	@UserId UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @TempRefreshToken TABLE (
		Id UNIQUEIDENTIFIER
	);
	MERGE RefreshTokens AS TARGET
	USING Users AS SOURCE
	ON SOURCE.RefreshTokenId = TARGET.Id
	WHEN MATCHED AND SOURCE.Id = @UserId THEN
		UPDATE SET TARGET.Token = @RefreshToken, TARGET.ExpirationDate = @ExpirationDate, TARGET.CreatedAt = @CreatedAt
	WHEN NOT MATCHED BY TARGET AND SOURCE.Id = @UserId THEN
		INSERT
		(
		[Id],
		[Token],
		[ExpirationDate],
		[CreatedAt]
		)
		VALUES
		(
		NEWID(),
		@RefreshToken,
		@ExpirationDate,
		@CreatedAt
		)
		OUTPUT inserted.Id INTO @TempRefreshToken;
	IF (SELECT Id FROM @TempRefreshToken FIRST) IS NOT NULL 
	BEGIN
		UPDATE Users SET RefreshTokenId = (SELECT Id FROM @TempRefreshToken FIRST) WHERE Users.Id = @UserId;
	END;
END