﻿using Authentication.Dtos;
using eLearningAPI.APIServices.Interfaces;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace eLearningAPI.APIServices.Implementations
{
    public class AuthenticationAPIService: IAuthenticationAPIService
    {
        private readonly HttpClient _httpClient;
        public AuthenticationAPIService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<AuthenticationResponse> AuthenticateUserAsync(string username, string password)
        {
            var authReq = new AuthenticationRequest { Username = username, Password = password };
            var content = new StringContent(JsonConvert.SerializeObject(authReq), Encoding.UTF8, "application/json");
            
            var response = await _httpClient.PostAsync("Authentication/authenticate", content); 
            if (response.IsSuccessStatusCode)
            {
                var stringResult = await response.Content.ReadAsStringAsync();
                var authResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(stringResult);
                return authResponse;
            }
            else
            {
                throw new System.Exception("Unable to authenticate user after registration");
            }
        }
    }
}
