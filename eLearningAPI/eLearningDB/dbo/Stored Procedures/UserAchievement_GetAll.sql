﻿CREATE PROCEDURE UserAchievement_GetAll
	@UserId UNIQUEIDENTIFIER
AS
BEGIN
	SELECT UA.UserId,
		UA.AchievementId,
		A.[Name] AS AchievementName,
		A.[Description] AS AchievementDescription,
		UA.AchievedAt
	FROM UserAchievement UA
	INNER JOIN Achievement A
	ON UA.AchievementId = A.Id
	WHERE UA.UserId = @UserId;
END