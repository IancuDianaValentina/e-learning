﻿CREATE PROCEDURE [dbo].[Users_GetForLeaderboard]
	@StartDate DATETIME2 = GETUTCDATE,
	@TopNumber TINYINT = 15
AS
BEGIN
	SELECT TOP(@TopNumber) U.Id,
		U.Username,
		COUNT(UA.Id) as CorrectAnswers,
		U.Points as UserPoints
	FROM Users U
	LEFT JOIN UserLesson UL
		ON UL.UserId = U.Id
	LEFT JOIN UserSessionAnswer UA 
		ON UA.UserSessionId = UL.Id
	WHERE UA.CorrectlyAnswered = 1 AND
		CAST(UA.AnsweredAt AS DATE) >= CAST(@StartDate AS DATE)
	GROUP BY U.Id, U.Username, U.Points
	ORDER BY UserPoints DESC;
END