import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { lastValueFrom } from "rxjs";
import { QuestionType } from "../models/quiz/question-type.model";

@Injectable({
    providedIn: 'root'
})
export class QuestionService {

    private baseUrl: string = '';

    constructor(private configService: ConfigService, private httpClient: HttpClient) {
        this.baseUrl = this.configService.getUrl("baseApiUrl");
    }

    getAllQuestionTypes(): Promise<Array<QuestionType>> {
        return lastValueFrom(this.httpClient.get<Array<QuestionType>>(this.baseUrl + '/Question/getAllQuestionTypes'));
    }

    deleteQuestionsByQuizId(quizId: number): Promise<void> {
        return lastValueFrom(this.httpClient.delete<void>(this.baseUrl + `/Question/DeleteQuestionsByQuizId?quizId=${quizId}`));
    }

    deleteChoice(choiceId: string): Promise<void> {
        return lastValueFrom(this.httpClient.delete<void>(this.baseUrl + `/Question/DeleteChoice?choiceId=${choiceId}`));
    }

    deleteQuestion(questionId: string): Promise<void> {
        return lastValueFrom(this.httpClient.delete<void>(this.baseUrl + `/Question/DeleteQuestion?questionId=${questionId}`));
    }
}