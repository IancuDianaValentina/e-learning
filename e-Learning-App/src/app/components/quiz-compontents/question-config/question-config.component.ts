import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { QuestionTypeEnum } from 'src/app/enums';
import { CustomFormGroup } from 'src/app/models/custom-form-group';
import { QuestionType } from 'src/app/models/quiz/question-type.model';
import { CustomPopupService } from 'src/app/services/custom-popup.service';
import { QuestionService } from 'src/app/services/question.service';
import { QuestionFormModel } from 'src/app/view-models/quiz/question-form.model';
import { QuestionVm } from 'src/app/view-models/quiz/question.view-model';
import { ConfirmationPopupAction } from '../../custom-popup/custom-popup.component';

@Component({
    selector: 'question-config',
    templateUrl: './question-config.component.html',
    styleUrls: ['./question-config.component.scss']
})

export class QuestionConfigComponent implements OnInit {
    @Input() question: QuestionVm;
    @Input() canNotEdit: boolean = false;
    @Output() onCancel: EventEmitter<QuestionVm> = new EventEmitter<QuestionVm>();
    @Output() onDelete: EventEmitter<QuestionVm> = new EventEmitter<QuestionVm>();
    @Output() questionChange: EventEmitter<QuestionVm> = new EventEmitter<QuestionVm>();
    @Output() onFinishOperation: EventEmitter<any> = new EventEmitter<any>();
    questionTypes: Array<QuestionType> = [];
    questionFormGroup: CustomFormGroup<QuestionFormModel>;
    questionTypeEnum = QuestionTypeEnum;
    isAddOrEditMode: boolean = true;
    anyChoiceInAddOrEditMode: boolean = false;

    constructor(private questionService: QuestionService, private popupService: CustomPopupService) { }

    async ngOnInit(): Promise<void> {
        if (!this.question) {
            this.question = new QuestionVm();
            this.question.choices = [];
        }
        if (this.question.id != null && this.question.id != undefined) {
            this.isAddOrEditMode = false;
        }
        this.questionService.getAllQuestionTypes().then(res => {
            this.questionTypes = res;
        });
        this.questionFormGroup = new CustomFormGroup({
            questionText: new FormControl(this.question.text, [Validators.required]),
            questionType: new FormControl(this.question.questionType, [Validators.required]),
            rewardPoints: new FormControl(this.question.rewardPoints, [Validators.required])
        });

    }

    onCancelOperation() {
        this.isAddOrEditMode = false;
        this.onCancel.emit(this.question);
    }

    onQuestionTypeChanged(event: any) {
        this.question.choices = [];
    }

    onStartEdit() {
        this.isAddOrEditMode = true;
        this.question.isAddMode = false;
        this.question.isEditMode = true;
        this.questionChange.emit(this.question);
    }

    onClickDelete() {
        this.onDelete.emit(this.question);
    }

    onFinish() {
        const anyAnswer = this.question.choices.some(c => c.isAnswer);
        if (!anyAnswer) {
            this.popupService.openPopup('Add question', ['Before adding the question, please select the answer by clicking on the choice you want to mark as an aswer.'],
                [{ text: 'Ok', action: ConfirmationPopupAction.Ok }]);
            return;
        }
        this.question.text = this.questionFormGroup.getFormControl('questionText').value;
        this.question.questionType = this.questionFormGroup.getFormControl('questionType').value;
        this.question.rewardPoints = this.questionFormGroup.getFormControl('rewardPoints').value;
        this.questionChange.emit(this.question);
        this.onFinishOperation.emit();
        this.isAddOrEditMode = false;
        this.question.isAddMode = false;
        this.question.isEditMode = false;
    }

    onChoiceModeChanged(isAddOrEditMode: boolean) {
        this.anyChoiceInAddOrEditMode = isAddOrEditMode;
    }
}
