﻿using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Dtos.UserSesssion;
using eLearningAPI.Enums;
using eLearningAPI.Extensions;
using eLearningAPI.Models;
using eLearningAPI.Models.UserSession;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Implementations
{
    public class UserBL: IUserBL
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<UserBL> _logger;
        public UserBL(IUserRepository userRepository, ILogger<UserBL> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public async Task<int> GetUserPointsAsync(Guid userId)
        {
            _logger.LogInformation("Retrieving number of points for user {0}.", userId);
            try
            {
                var points = await _userRepository.GetUserPointsAsync(userId);
                _logger.LogInformation("Successfully retrieved number of points for user {0}.", userId);
                return points;
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving number of points for user {0}.", userId);
                throw new Exception("Error retrieving number of points for user.");
            }
        }

        public async Task<List<LeaderboardUser>> GetUsersForLeaderboardAsync(string startDate, int topNumber)
        {
            _logger.LogInformation("Retrieving user for leaderboard.");
            try
            {
                var users = await _userRepository.GetUsersForLeaderboardAsync(Convert.ToDateTime(startDate), topNumber);
                _logger.LogInformation("Successfully retrieved users for leaderboard.");
                return users;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving users for leaderboard.");
                throw new Exception("Error retrieving users for leaderboard.");
            }
        }

        public async Task<User> GetUserByIdAsync(Guid userId)
        {

            _logger.LogInformation("Retrieving user by id {0}.", userId);
            try
            {
                var user = await _userRepository.GetUserByIdAsync(userId);
                _logger.LogInformation("Successfully retrieved user by id {0}.", userId);
                return user;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving user by id {0}.", userId);
                throw new Exception("Error retrieving user by id.");
            }
        }

        public async Task<List<UserLessonSessionListItem>> GetUserSessionsByLearningPathIdAsync(int learningPathId, Guid userId)
        {
            _logger.LogInformation("Retrieving user sessions by learning path id {0} for user {1}.", learningPathId, userId);
            try
            {
                var models = await _userRepository.GetUserSessionsByLearningPathIdAsync(learningPathId, userId);
                _logger.LogInformation("Successfully retrieved user sessions by learning path id {0} for user {1}.", learningPathId, userId);
                return models.Select(model => model.ToListItemDto()).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving user sessions by learning path id {0} for user {1}.", learningPathId, userId);
                throw new Exception("Error retrieving user sessions by learning path id for user.");
            }
        }

        public async Task<Guid> CreateUserSessionAsync(Guid userId, int learningPathId, int lessonId, int requiredPoints)
        {
            _logger.LogInformation("Creating user session.");
            try
            {
                var sessionId = await _userRepository.CreateUserSessionAsync(userId, learningPathId, lessonId, requiredPoints);
                _logger.LogInformation("Successfully created user session");
                return sessionId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating user session.");
                throw new Exception("Error creating user session.");
            }
        }

        public async Task<UserLessonSessionDto> GetUserSessionAsync(Guid userId, int lessonId, int learningPathId)
        {
            _logger.LogInformation("Retrieving user session.");
            try
            {
                var session = await _userRepository.GetUserSessionAsync(userId, lessonId, learningPathId);
                _logger.LogInformation("Successfully retrieved user session");
                return session.ToDto();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving user session.");
                throw new Exception("Error retrieving user session.");
            }
        }

        public async Task ChangeUserSessionStatusAsync(Guid userSessionId, UserSessionStatusEnum status)
        {
            _logger.LogInformation("Changing user session's status.");
            try
            {
                await _userRepository.ChangeUserSessionStatusAsync(userSessionId, status);
                _logger.LogInformation("Successfully changed user session's status");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error changing user session's status.");
                throw new Exception("Error changing user session's status.");
            }
        }

        public async Task<List<UserAchievement>> GetAllUserAchievementByUserIdAsync(Guid userId)
        {
            _logger.LogInformation("Retrieving achievements for user {0}.", userId);
            try
            {
                var models = await _userRepository.GetAllUserAchievementByUserIdAsync(userId);
                _logger.LogInformation("Successfully retrieved achievements for user {0}.", userId);
                return models;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving achievements for user {0}.", userId);
                throw new Exception("Error retrieving achievements for user.");
            }
        }
    }
}
