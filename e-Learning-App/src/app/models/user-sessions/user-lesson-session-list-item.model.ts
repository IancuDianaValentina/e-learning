import { UserSessionStatusEnum } from "src/app/enums";

export class UserLessonSessionListItem {
    id: string;
    userId: string;
    lessonId: number;
    learningPathId: number;
    status: UserSessionStatusEnum;
}