﻿CREATE PROCEDURE LearningPaths_GetByProgrammingLanguage
(
	@ProgrammingLanguageId TINYINT
)
AS
BEGIN
	SELECT LP.[Id], 
		LP.[Title],
		LP.[ProgrammingLanguageId] AS ProgrammingLanguage
	FROM LearningPath LP INNER JOIN ProgrammingLanguage PL
		ON LP.ProgrammingLanguageId = PL.Id
	WHERE PL.Id = @ProgrammingLanguageId;
END