﻿
CREATE PROCEDURE [dbo].[User_GetAllSessionsByLearningPathId] 
(
	@LearningPathId INT,
	@UserId UNIQUEIDENTIFIER
)
AS
BEGIN
	SELECT UL.Id,
		UL.UserId,
		UL.LessonId,
		UL.LearningPathId,
		UL.[Status]
	FROM UserLesson UL
	WHERE UL.LearningPathId = @LearningPathId AND
		UL.UserId = @UserId;
END