import { QuestionTypeEnum } from "src/app/enums";
import { Choice } from "./choice.model";

export class Question {
    id: string;
    text: string;
    questionType: QuestionTypeEnum;
    rewardPoints: number;
    choices: Choice[];
}