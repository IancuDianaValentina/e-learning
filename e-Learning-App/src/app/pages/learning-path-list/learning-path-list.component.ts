import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LearningLevelEnum, ProgrammingLanguageEnum, UserRoleEnum } from 'src/app/enums';
import { LearningPathHelper } from 'src/app/helpers';
import { UserSession } from 'src/app/models/user-session.model';
import { LearningPathService } from 'src/app/services/learning-path.service';
import { SessionService } from 'src/app/services/session.service';
import { LearningLevelIcon, LearningPathVm } from 'src/app/view-models/learning-path.view-model';

@Component({
    selector: 'learning-path-list',
    templateUrl: './learning-path-list.component.html',
    styleUrls: ['./learning-path-list.component.scss']
})

export class LearningPathListComponent implements OnInit {

    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    learningPathDataSource: MatTableDataSource<LearningPathVm> = new MatTableDataSource<LearningPathVm>();
    learningPaths: Array<LearningPathVm> = [];
    learningPathObservables: Observable<LearningPathVm[]>;
    userSession: UserSession;
    isAdmin: boolean = false;


    constructor(private learningPathService: LearningPathService, private sessionService: SessionService,
        private router: Router) { }

    async ngOnInit() {
        this.userSession = await this.sessionService.getUserSession();
        this.isAdmin = this.userSession?.role == UserRoleEnum.Administrator;
        let learningPathModels = await this.learningPathService.getAllLearningPaths();

        this.learningPaths = learningPathModels.map(item => ({
            id: item.id,
            title: item.title,
            description: item.description,
            noLearningHours: item.noLearningHours,
            programmingLanguage: LearningPathHelper.getProgrammingLanguage(item.programmingLanguage),
            learningLevel: LearningPathHelper.getLearningLevel(item.learningLevel),
            learningLevelIcon: LearningPathHelper.getLearningLevelIcon(item.learningLevel)
        }) as LearningPathVm);

        this.learningPathDataSource = new MatTableDataSource(this.learningPaths);
        this.learningPathDataSource.paginator = this.paginator;
        this.learningPathObservables = this.learningPathDataSource.connect();
    }

    onAddLearningPath() {
        this.router.navigate([`/learning-path/add`]);
    }

    onEditLearningPath(id: number) {
        this.router.navigate([`/learning-path/${id}/edit`]);
    }
}
