import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { lastValueFrom } from "rxjs";
import { Constants } from "../models/constants.model";
import { ProgrammingLanguage } from "../models/programming-language.model";

@Injectable({
    providedIn: 'root'
})
export class ProgrammingLanguageService {

    private baseUrl: string = '';
    private noInterceptParams = {
        [Constants.NoIntercept]: "true"
    };

    constructor(private configService: ConfigService, private httpClient: HttpClient) {
        this.baseUrl = this.configService.getUrl("baseApiUrl");
    }

    getAllProgrammingLanguages(): Promise<Array<ProgrammingLanguage>> {
        return lastValueFrom(this.httpClient.get<Array<ProgrammingLanguage>>(this.baseUrl + '/ProgrammingLanguage/getAll', { params: this.noInterceptParams }));
    }
}