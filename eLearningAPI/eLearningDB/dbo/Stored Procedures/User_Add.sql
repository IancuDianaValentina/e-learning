﻿CREATE PROCEDURE [dbo].[User_Add]
(
	@Username NVARCHAR(55),
	@PasswordHash NVARCHAR(MAX),
	@EmailAddress NVARCHAR(55),
	@Address NVARCHAR(150),
	@FirstName NVARCHAR(55),
	@LastName NVARCHAR(55),
	@PhoneNumber NVARCHAR(13),
	@RoleId TINYINT
)
AS
BEGIN
	INSERT INTO Users([Id], [Username], [PasswordHash], [EmailAddress], [Address], [FirstName], [LastName], [PhoneNumber], [RoleId], Points)
		VALUES (NEWID(), @Username, @PasswordHash, @EmailAddress, @Address, @FirstName, @LastName, @PhoneNumber, @RoleId, 15);
END