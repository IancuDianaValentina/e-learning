﻿using eLearningAPI.Enums;

namespace eLearningAPI.Models.Lessons
{
    public class LessonLearningPath
    {
        public int LessonId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int LearningTime { get; set; }
        public int RequiredPoints { get; set; }
        public ProgrammingLanguageEnum ProgrammingLanguage { get; set; }
        public int LessonIndex { get; set; }        public int LearningPathId { get; set; }        public string LearningPathTitle { get; set; }        public Lesson NextLesson { get; set; }    }
}
