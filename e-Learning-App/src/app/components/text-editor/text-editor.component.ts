import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'text-editor',
    templateUrl: './text-editor.component.html',
    styleUrls: ['./text-editor.component.scss']
})

export class TextEditorComponent {
    @Input() value: string = '';
    @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

    onValueChange(event: any) {
        this.valueChange.emit(this.value);
    }
}
