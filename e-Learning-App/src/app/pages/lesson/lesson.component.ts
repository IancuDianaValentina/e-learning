import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationPopupAction, FieldType } from 'src/app/components/custom-popup/custom-popup.component';
import { CustomFormGroup } from 'src/app/models/custom-form-group';
import { LearningPathLink } from 'src/app/models/learning-path/learning-path-link.model';
import { LessonContentFormModel } from 'src/app/models/lesson/lesson-content-form.model';
import { LessonSettingsFormModel } from 'src/app/models/lesson/lesson-settings-form.model';
import { Lesson } from 'src/app/models/lesson/lesson.model';
import { OptionItem } from 'src/app/models/option-item.model';
import { ProgrammingLanguage } from 'src/app/models/programming-language.model';
import { CustomPopupService } from 'src/app/services/custom-popup.service';
import { LearningPathService } from 'src/app/services/learning-path.service';
import { LessonService } from 'src/app/services/lesson.service';
import { ProgrammingLanguageService } from 'src/app/services/programming-language.service';
import { QuizVm } from 'src/app/view-models/quiz/quiz.view-model';

@Component({
    selector: 'lesson',
    templateUrl: './lesson.component.html',
    styleUrls: ['./lesson.component.scss']
})

export class LessonComponent implements OnInit {

    isEditMode: boolean = false;
    settingsFormGroup: CustomFormGroup<LessonSettingsFormModel>;
    contentFormGroup: CustomFormGroup<LessonContentFormModel>;
    programmingLanguages: Array<ProgrammingLanguage> = [];
    lesson: Lesson = new Lesson();
    learningPathLinks: FormControl;

    constructor(private activatedRoute: ActivatedRoute, private programmingLanguageService: ProgrammingLanguageService,
        private customPopupService: CustomPopupService, private learningPathService: LearningPathService,
        private lessonService: LessonService, private router: Router) { }

    async ngOnInit() {
        this.programmingLanguageService.getAllProgrammingLanguages().then(response => {
            this.programmingLanguages = response;
        });
        this.settingsFormGroup = new CustomFormGroup<LessonSettingsFormModel>({
            title: new FormControl('', [Validators.required, Validators.maxLength(150)]),
            programmingLanguage: new FormControl('', Validators.required),
            learningTime: new FormControl('', [Validators.required, this.nonNegativeValidator()]),
            isLearningTimeInMinutes: new FormControl(true, [Validators.required]),
            requiredPoints: new FormControl('', [Validators.required, this.nonNegativeValidator()])
        });
        this.contentFormGroup = new CustomFormGroup<LessonContentFormModel>({
            htmlContent: new FormControl('', [Validators.required])
        });
        this.learningPathLinks = new FormControl();

        const lessonId = this.activatedRoute.snapshot.paramMap.get("id");
        this.isEditMode = lessonId ? true : false;
        if (this.isEditMode) {
            this.lesson = await this.lessonService.getLessonWithQuizById(parseInt(lessonId ?? '0'));
            this.settingsFormGroup.getFormControl('title').setValue(this.lesson.title);
            this.settingsFormGroup.getFormControl('programmingLanguage').setValue(this.lesson.programmingLanguage);
            this.settingsFormGroup.getFormControl('learningTime').setValue(this.lesson.learningTime);
            this.settingsFormGroup.getFormControl('requiredPoints').setValue(this.lesson.requiredPoints);
            this.contentFormGroup.getFormControl('htmlContent').setValue(this.lesson.content);
            this.learningPathLinks.setValue(this.lesson.learningPathLinks.map(l => l.learningPathId));
        } else {
            const learningPathId = window.history.state.learningPathId;
            const programmingLanguage = window.history.state.programmingLanguage;

            if (learningPathId) {
                this.lesson.learningPathLinks = [];
                this.learningPathLinks.setValue([learningPathId]);
                this.settingsFormGroup.getFormControl('programmingLanguage').setValue(programmingLanguage);
                this.settingsFormGroup.getFormControl('programmingLanguage').disable();
            }
        }
        if (!this.lesson.quiz) {
            this.lesson.quiz = new QuizVm();
        }
    }

    async onAddLearningPathLink() {
        if (!this.settingsFormGroup.getFormControl('programmingLanguage').value) {
            this.settingsFormGroup.getFormControl('programmingLanguage').markAsTouched();
            return;
        } else {
            await this.onOpenAddLearningPathPopup();
        }
    }

    async onOpenAddLearningPathPopup() {
        const learningPaths = await this.learningPathService
            .getLearningPathsByProgrammingLanguage(this.settingsFormGroup.getFormControl('programmingLanguage').value);
        const options = learningPaths?.map(path => ({
            id: path.id,
            name: path.title
        }) as OptionItem);

        const formControl = new FormControl();
        formControl.setValue(this.learningPathLinks?.value);
        const learningPathId = window.history.state.learningPathId;
        if (learningPathId) {
            formControl.disable();
        }
        let response = await this.customPopupService.openPopup('Add learning path link',
            ['Link this lesson to one or more learning paths.'],
            [
                { text: 'Save', action: ConfirmationPopupAction.Ok },
                { text: 'Cancel', action: ConfirmationPopupAction.Cancel }
            ],
            [
                { type: FieldType.MultiselectAutocomplete, label: 'Learning paths', model: formControl, options: options }
            ]
        );
        if (response == ConfirmationPopupAction.Ok) {
            this.learningPathLinks.setValue(formControl.value);
        }
    }

    onContentValueChange(event: any) {
        this.contentFormGroup?.getFormControl('htmlContent').markAsTouched();
        this.contentFormGroup?.getFormControl('htmlContent').setValue(event);
    }

    async onSaveLesson() {
        this.lesson.title = this.settingsFormGroup.getFormControl('title').value;
        const isLearningTimeInMinutes = this.settingsFormGroup.getFormControl('isLearningTimeInMinutes').value;
        this.lesson.learningTime = isLearningTimeInMinutes ? this.settingsFormGroup.getFormControl('learningTime').value
            : this.settingsFormGroup.getFormControl('learningTime').value * 60;
        this.lesson.requiredPoints = this.settingsFormGroup.getFormControl('requiredPoints').value;
        this.lesson.programmingLanguage = this.settingsFormGroup.getFormControl('programmingLanguage').value;
        this.lesson.content = this.contentFormGroup.getFormControl('htmlContent').value;

        const learningPathLinks = this.learningPathLinks?.value;
        if (learningPathLinks) {
            this.lesson.learningPathLinks = [];
            this.lesson.learningPathLinks?.push(...(learningPathLinks.map(id => ({ learningPathId: id }) as LearningPathLink)));
        }

        const returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'];
        await this.lessonService.saveLesson(this.lesson);
        this.onGoBack(returnUrl);
    }

    nonNegativeValidator(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            return control?.value && control.value < 0 ? { negativeNumber: true } : null;
        }
    }

    onGoBack(returnUrl: string) {
        if (returnUrl) {
            this.router.navigateByUrl(returnUrl);
        } else {
            this.router.navigate(['/lessons']);
        }
    }
}
