import { LearningLevelEnum, ProgrammingLanguageEnum } from "../../enums";
import { Lesson } from "../lesson/lesson.model";

export class LearningPath {
    id: number;
    title: string;
    noLearningHours: number;
    description: string;
    programmingLanguage: ProgrammingLanguageEnum;
    learningLevel: LearningLevelEnum;
    lessons: Array<Lesson>;
}