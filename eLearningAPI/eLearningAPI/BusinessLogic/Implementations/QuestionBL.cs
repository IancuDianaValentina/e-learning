﻿using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Models.Quiz.Questions;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Implementations
{
    public class QuestionBL: IQuestionBL
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly ILogger<QuestionBL> _logger;
        public QuestionBL(IQuestionRepository questionRepository, ILogger<QuestionBL> logger)
        {
            _questionRepository = questionRepository;
            _logger = logger;
        }

        public async Task<List<QuestionType>> GetQuestionTypesAsync()
        {
            _logger.LogInformation("Retrieving all question types.");
            try
            {
                var models = await _questionRepository.GetQuestionTypesAsync();
                _logger.LogInformation("Successfully retrieved all question types.");
                return models;
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error retrieving question types.");
                throw new Exception("Error retrieving question types.");
            }
        }

        public async Task DeleteQuestionsByQuizIdAsync(int quizId)
        {
            _logger.LogInformation("Deleting questions for quiz {0}", quizId);
            try
            {
                await _questionRepository.DeleteQuestionsByQuizIdAsync(quizId);
                _logger.LogInformation("Succesfully deleted questions for quiz {0}", quizId);
            } catch (Exception ex)
            {
                _logger.LogError(ex, "Error deleting questions for quiz {0}", quizId);
                throw new Exception("Error deleting questions for quiz.");
            }

        }

        public async Task DeleteChoiceByIdAsync(int choiceId)
        {
            _logger.LogInformation("Deleting choice {0}", choiceId);
            try
            {
                await _questionRepository.DeleteChoiceByIdAsync(choiceId);
                _logger.LogInformation("Succesfully deleted choice {0}", choiceId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error deleting choice {0}", choiceId);
                throw new Exception("Error deleting choice.");
            }
        }

        public async Task DeleteQuestionByIdAsync(int questionId)
        {
            _logger.LogInformation("Deleting question {0}", questionId);
            try
            {
                await _questionRepository.DeleteQuestionByIdAsync(questionId);
                _logger.LogInformation("Succesfully deleted question {0}", questionId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error deleting question {0}", questionId);
                throw new Exception("Error deleting question.");
            }
        }
    }
}
