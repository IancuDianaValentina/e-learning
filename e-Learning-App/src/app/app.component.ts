import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AppLogo, ToolbarItem } from './components/toolbar/toolbar.component';
import { UserRoleEnum } from './enums';
import { UserSession } from './models/user-session.model';
import { SessionService } from './services/session.service';
import { ToolbarActionEnum, ToolbarService } from './services/toolbar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  appTitle = 'eLearning App';
  appLogo: AppLogo;
  userSession: UserSession;
  toolbarMenuItems: Array<ToolbarItem> = [];
  simpleToolbarItems: Array<ToolbarItem> = [];

  constructor(private sessionService: SessionService, private router: Router, private meta: Meta,
    private title: Title, private toolbarService: ToolbarService) {
    this.toolbarService.getToolbarCommands().subscribe(c => {
      switch (c.action) {
        case ToolbarActionEnum.SetItems:
          this.setToolbarItems();
      }
    })
  }

  async ngOnInit() {
    this.setAppDetails();
    this.userSession = await this.sessionService.getUserSession();
    // All the toolbar items/menus should have been stored in the database with access roles 
    this.setToolbarItems();
  }

  onViewAccount() {
    this.router.navigateByUrl(`profile`);
  }

  onLogout() {
    this.sessionService.logout();
    this.router.navigate(['/']);
    this.toolbarService.setToolbarItems();
  }

  onLogin() {
    this.router.navigate(['/login']);
  }

  onLogoClick() {
    this.router.navigate(['/home']);
  }

  onGoToContact() {
    this.router.navigate(['/contact']);
  }

  onGoToLessons() {
    this.router.navigate(['/lessons']);
  }

  onGoToLearningPaths() {
    this.router.navigate(['/learning-paths']);
  }

  onGoToLeaderboard() {
    this.router.navigate(['/leaderboard']);
  }

  async setToolbarItems() {
    this.simpleToolbarItems = [];
    this.toolbarMenuItems = [];

    this.userSession = await this.sessionService.getUserSession();

    this.simpleToolbarItems = [
      { itemName: 'Contact', callback: this.onGoToContact.bind(this) },
      { itemName: 'Leaderboard', callback: this.onGoToLeaderboard.bind(this) },
      { itemName: 'Home', callback: this.onLogoClick.bind(this) }
    ];

    if (this.userSession?.role == UserRoleEnum.Administrator) {
      this.initAdminToolbarItems();
    }

    if (this.userSession) {
      this.toolbarMenuItems.push(...[
        {
          icon: 'account_circle', menuItems: [
            { itemName: 'View Account', callback: this.onViewAccount.bind(this) },
            { itemName: 'Log out', callback: this.onLogout.bind(this) }
          ]
        }
      ]);
    } else {
      this.simpleToolbarItems.push(...[
        {
          itemName: 'Login', callback: this.onLogin.bind(this)
        }
      ]);
    }
  }

  initAdminToolbarItems() {
    this.simpleToolbarItems.push(...[
      {
        itemName: 'Lessons', callback: this.onGoToLessons.bind(this)
      },
      {
        itemName: 'Learning Paths', callback: this.onGoToLearningPaths.bind(this)
      }
    ]);
  }

  setAppDetails() {
    this.appLogo = { path: '../assets/Images/logo1.png', callback: this.onLogoClick.bind(this) }
    this.title.setTitle('e-Learning App');
    this.meta.addTags([
      { name: 'description', content: 'e-Learning App homepage' },
      { name: 'keywords', content: 'e-Learning, learning, programming, code, C#, Angular, typescript' }
    ]);
  }
}
