import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OptionItem } from 'src/app/models/option-item.model';

export enum ConfirmationPopupAction {
    Ok = 1,
    Cancel = 2
}

export enum FieldType {
    Input = 1,
    Select = 2,
    Checkbox = 3,
    Multiselect = 4,
    Autocomplete = 5,
    MultiselectAutocomplete = 6
}

export interface ActionButton {
    icon?: string;
    text?: string;
    action: ConfirmationPopupAction;
};

export interface FieldConfig {
    type: FieldType;
    label: string;
    model: FormControl;
    options?: OptionItem[];
}

@Component({
    selector: 'custom-popup',
    templateUrl: './custom-popup.component.html',
    styleUrls: ['./custom-popup.component.scss']
})

export class CustomPopupComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

    fieldTypeEnum = FieldType;
}
