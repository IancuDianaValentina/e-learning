﻿using Dapper;
using eLearningAPI.Enums;
using eLearningAPI.Models;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Implementations
{
    public class LearningPathRepository: BaseRepository, ILearningPathRepository
    {
        private static readonly string GetAllLearningPathsSP = "LearningPath_GetAll";
        private static readonly string GetLearningPathSP = "LearningPath_Get";
        private static readonly string SaveLearningPathSP = "LearningPath_Save";
        private static readonly string GetLearningPathsByProgrammingLanguageSP = "LearningPaths_GetByProgrammingLanguage";
        private static readonly string UnlinkLessonFromLearningPathSP = "LessonLearningPath_Unlink";
        public LearningPathRepository(IConfiguration configuration): base(configuration) { }

        public async Task<List<LearningPath>> GetAllLearningPathsAsync()
        {
            using (var connection = Connection)
            {
                return (await connection.QueryAsync<LearningPath>(GetAllLearningPathsSP,
                    commandType: System.Data.CommandType.StoredProcedure)) as List<LearningPath>;
            }
        }

        public async Task<List<LearningPath>> GetByProgrammingLanguageAsync(ProgrammingLanguageEnum programmingLanguage)
        {
            using (var connection = Connection)
            {
                return (await connection.QueryAsync<LearningPath>(GetLearningPathsByProgrammingLanguageSP,
                    param: new {
                        ProgrammingLanguageId = programmingLanguage
                    },
                    commandType: System.Data.CommandType.StoredProcedure)) as List<LearningPath>;
            }
        }

        public async Task<int> SaveLearningPathAsync(LearningPath learningPath)
        {
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<int>(SaveLearningPathSP,
                    param: new
                    {
                        Id = learningPath.Id,
                        Title = learningPath.Title,
                        Description = learningPath.Description,
                        ProgrammingLanguage = learningPath.ProgrammingLanguage,
                        LearningLevel = learningPath.LearningLevel
                    },
                    commandType: System.Data.CommandType.StoredProcedure) ;
            }
        }

        public async Task UnlinkLessonFromLearningPathAsync(int lessonId, int learningPathId)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(UnlinkLessonFromLearningPathSP,
                    param: new
                    {
                        LessonId = lessonId,
                        LearningPathId = learningPathId
                    },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<LearningPath> GetLearningPathAsync(int learningPathId)
        {
            using (var connection = Connection)
            {
                var learningPathDictionary = new Dictionary<int, LearningPath>();
                return (await connection.QueryAsync<LearningPath, Lesson, LearningPath>(GetLearningPathSP,
                    (LearningPath learningPath, Lesson lesson) =>
                    {
                        LearningPath learningPathEntry;
                        if (!learningPathDictionary.TryGetValue(learningPath.Id, out learningPathEntry))
                        {
                            learningPathEntry = learningPath;
                            learningPathEntry.Lessons = new List<Lesson>();
                            learningPathDictionary.Add(learningPathEntry.Id, learningPathEntry);
                        }

                        learningPathEntry.Lessons.Add(lesson);
                        return learningPathEntry;
                    },
                    splitOn: "Id,Id",
                    param: new { LearningPathId = learningPathId },
                    commandType: System.Data.CommandType.StoredProcedure)).Distinct().FirstOrDefault();
            }
        }
    }
}
