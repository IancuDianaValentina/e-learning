﻿using Dapper;
using eLearningAPI.Enums;
using eLearningAPI.Models;
using eLearningAPI.Models.UserSession;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Implementations
{
    public class UserRepository: BaseRepository, IUserRepository
    {
        private static readonly string GetUserByUsernameSP = "User_GetByUsername";
        private static readonly string AddUserSP = "User_Add";
        private static readonly string GetUserPointsSP = "User_GetPoints";
        private static readonly string GetUsersForLeaderboardSP = "Users_GetForLeaderboard";
        private static readonly string GetUserByIdSP = "User_GetById";
        private static readonly string GetUserSessionsByLearningPathIdSP = "User_GetAllSessionsByLearningPathId";
        private static readonly string CreateUserSessionSP = "UserSession_Create";
        private static readonly string GetUserSessionSP = "UserSession_Get";
        private static readonly string ChangeUserSessionStatusSP = "UserSession_ChangeStatus";
        private static readonly string GetAllUserAchievementsSP = "UserAchievement_GetAll";
        public UserRepository(IConfiguration configuration): base(configuration) { }
        
        public async Task<User> GetUserByUsernameAsync(string username)
        {
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<User>(GetUserByUsernameSP,
                    param: new { Username = username },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }
        public async Task AddUserAsync(User user)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(AddUserSP,
                    param: new
                    {
                        Username = user.Username,
                        PasswordHash = user.PasswordHash,
                        EmailAddress = user.EmailAddress,
                        Address = user.Address,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        PhoneNumber = user.PhoneNumber,
                        RoleId = user.RoleId
                    },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<int> GetUserPointsAsync(Guid userId)
        {
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<int>(GetUserPointsSP,
                    param: new { UserId = userId },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<List<LeaderboardUser>> GetUsersForLeaderboardAsync(DateTime startDate, int topNumber)
        {
            using (var connection = Connection)
            {
                return await connection.QueryAsync<LeaderboardUser>(GetUsersForLeaderboardSP,
                    param: new { StartDate = startDate, TopNumber = topNumber },
                    commandType: System.Data.CommandType.StoredProcedure) as List<LeaderboardUser>;
            }
        }

        public async Task<User> GetUserByIdAsync(Guid userId)
        {
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<User>(GetUserByIdSP,
                    param: new { UserId = userId },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<List<UserLessonSession>> GetUserSessionsByLearningPathIdAsync(int learningPathId, Guid userId)
        {
            using (var connection = Connection)
            {
                return await connection.QueryAsync<UserLessonSession>(GetUserSessionsByLearningPathIdSP,
                    param: new 
                    { 
                        LearningPathId = learningPathId,
                        UserId = userId 
                    },
                    commandType: System.Data.CommandType.StoredProcedure) as List<UserLessonSession>;
            }
        }

        public async Task<Guid> CreateUserSessionAsync(Guid userId, int learningPathId, int lessonId, int requiredPoints)
        {
            using (var connection = Connection)
            {
                return await connection.QueryFirstOrDefaultAsync<Guid>(CreateUserSessionSP,
                    param: new
                    {
                        UserId = userId,
                        LearningPathId = learningPathId,
                        LessonId = lessonId,
                        RequiredPoints = requiredPoints
                    },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<UserLessonSession> GetUserSessionAsync(Guid userId, int lessonId, int learningPathId)
        {
            using (var connection = Connection)
            {
                var sessionDictionary = new Dictionary<Guid, UserLessonSession>();
                return (await connection.QueryAsync<UserLessonSession, UserAnswer, Guid?, UserLessonSession>(GetUserSessionSP,
                    (UserLessonSession session, UserAnswer answer, Guid? choiceId) =>
                    {
                        UserLessonSession sessionEntry;
                        if (!sessionDictionary.TryGetValue(session.Id, out sessionEntry))
                        {
                            sessionEntry = session;
                            sessionEntry.UserAnswers = new List<UserAnswer>();
                            sessionDictionary.Add(sessionEntry.Id, sessionEntry);
                        }

                        if (!sessionEntry.UserAnswers.Exists(a => a.Id == answer?.Id) && answer?.Id != Guid.Empty && answer?.Id != null)
                        {
                            sessionEntry.UserAnswers.Add(answer);
                        }

                        var index = sessionEntry.UserAnswers.FindIndex(a => a.Id == answer?.Id);
                        if (index != -1)
                        {
                            if (sessionEntry.UserAnswers[index].ChoiceIds == null)
                            {
                                sessionEntry.UserAnswers[index].ChoiceIds = new List<Guid>();
                            }
                            sessionEntry.UserAnswers[index].ChoiceIds.Add(choiceId.GetValueOrDefault());
                        }

                        return sessionEntry;
                    },
                    splitOn: "Id,Id,ChoiceId",
                    param: new
                    {
                        UserId = userId,
                        LessonId = lessonId,
                        LearningPathId = learningPathId
                    },
                    commandType: System.Data.CommandType.StoredProcedure)).Distinct().FirstOrDefault();
            }
        }

        public async Task ChangeUserSessionStatusAsync(Guid userSessionId, UserSessionStatusEnum status)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(ChangeUserSessionStatusSP,
                    param: new
                    {
                        UserSessionId = userSessionId,
                        Status = status
                    },
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task<List<UserAchievement>> GetAllUserAchievementByUserIdAsync(Guid userId)
        {
            using (var connection = Connection)
            {
                return (await connection.QueryAsync<UserAchievement>(GetAllUserAchievementsSP,
                    param: new
                    {
                        UserId = userId
                    },
                    commandType: System.Data.CommandType.StoredProcedure)) as List<UserAchievement>;
            }
        }
    }
}
