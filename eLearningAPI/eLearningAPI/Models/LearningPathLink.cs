﻿namespace eLearningAPI.Models
{
    public class LearningPathLink
    {
        public int LessonIndex { get; set; }
        public int LearningPathId { get; set; }
    }
}
