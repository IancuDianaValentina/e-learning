import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { lastValueFrom } from "rxjs";
import { LearningPath } from "../models/learning-path/learning-path.model";
import { Constants } from "../models/constants.model";
import { ContactModel } from "../models/contact/contact.model";

@Injectable({
    providedIn: 'root'
})
export class MailService {

    private baseUrl: string = '';
    private noInterceptParams = {
        [Constants.NoIntercept]: "true"
    };

    constructor(private configService: ConfigService, private httpClient: HttpClient) {
        this.baseUrl = this.configService.getUrl("mailApiUrl");
    }

    sendContactMail(contact: ContactModel): Promise<void> {
        return lastValueFrom(this.httpClient.post<void>(this.baseUrl + '/Mail/sendContactMail', contact));
    }

}