﻿CREATE TABLE [dbo].[LearningLevel] (
    [Id]   TINYINT       NOT NULL,
    [Name] NVARCHAR (15) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

