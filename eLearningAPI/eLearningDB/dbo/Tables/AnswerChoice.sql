﻿CREATE TABLE [dbo].[AnswerChoice] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [ChoiceId] UNIQUEIDENTIFIER NOT NULL,
    [AnswerId] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([AnswerId]) REFERENCES [dbo].[UserSessionAnswer] ([Id]),
    FOREIGN KEY ([ChoiceId]) REFERENCES [dbo].[Choice] ([Id])
);

