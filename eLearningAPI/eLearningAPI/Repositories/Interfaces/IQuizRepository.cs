﻿using eLearningAPI.Models.Quiz;
using eLearningAPI.Models.UserSession;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Interfaces
{
    public interface IQuizRepository
    {
        public Task<Quiz> GetQuizByLessonIdAsync(int lessonId);
        public Task<Guid> SaveUserAnswerAsync(UserAnswer userAnswer);
        public Task<List<Choice>> GetAnswersByQuestionIdAsync(Guid questionId);
    }
}
