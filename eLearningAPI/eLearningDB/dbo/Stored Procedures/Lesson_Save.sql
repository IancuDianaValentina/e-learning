﻿
CREATE PROCEDURE [dbo].[Lesson_Save]
(
	@LessonId INT = NULL,
	@Title NVARCHAR(150),
	@HtmlContent NVARCHAR(MAX),
	@LearningTime TINYINT,
	@RequiredPoints TINYINT,
	@ProgrammingLanguageId TINYINT,
	@LearningPaths IntList READONLY,
	@QuizTitle NVARCHAR(255) = NULL,
	@Questions Question READONLY,
	@Choices Choice READONLY
)
AS
BEGIN
	DECLARE @LessonIdVariable INT;
	DECLARE @QuizId INT;
	DECLARE @LearningPathLinks TABLE (LearningPathId INT, LessonIndex TINYINT);

	IF NOT EXISTS (
					SELECT *
					FROM Lesson
					WHERE Id = @LessonId
					)
	INSERT INTO Lesson 
	(
		Title,
		HtmlContent,
		ProgrammingLanguageId,
		LearningTime,
		RequiredPoints
	)
	VALUES
	(
		@Title,
		@HtmlContent,
		@ProgrammingLanguageId,
		@LearningTime,
		@RequiredPoints
	)
	ELSE
	UPDATE Lesson
	SET Title = @Title,
		HtmlContent = @HtmlContent,
		ProgrammingLanguageId = @ProgrammingLanguageId,
		LearningTime = @LearningTime,
		RequiredPoints = @RequiredPoints
	WHERE Id = @LessonId;

	SET @LessonIdVariable = CASE WHEN SCOPE_IDENTITY() IS NOT NULL THEN SCOPE_IDENTITY()
							ELSE @LessonId END;

	INSERT INTO @LearningPathLinks(LearningPathId, LessonIndex)
	SELECT LP.Id, COUNT(LLP.LearningPathId) + 1
	FROM @LearningPaths Lp
	LEFT JOIN  LessonLearningPath LLP
		ON LP.Id = LLP.LearningPathId
	GROUP BY LP.Id;

	MERGE LessonLearningPath AS TARGET
	USING @LearningPathLinks AS SOURCE
		ON (TARGET.LearningPathId = SOURCE.LearningPathId AND TARGET.LessonId = @LessonIdVariable)
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (LearningPathId, LessonId, LessonIndex)
		VALUES (SOURCE.LearningPathId, @LessonIdVariable, SOURCE.LessonIndex)
	WHEN NOT MATCHED BY SOURCE AND @LessonIdVariable = TARGET.LessonId
	THEN DELETE;

	UPDATE LearningPath
	SET NoLearningHours = NoLearningHours + @LearningTime
	FROM LearningPath
	INNER JOIN @LearningPaths LP
		ON LP.Id = LearningPath.Id
	WHERE @LessonId IS NULL;

	IF NOT EXISTS (
					SELECT *
					FROM Quiz
					WHERE LessonId = @LessonIdVariable
					)
	INSERT INTO Quiz 
	(
		Title,
		LessonId,
		UpdatedAt
	) VALUES 
	(
		@QuizTitle,
		@LessonIdVariable,
		GETUTCDATE()
	) ELSE
	UPDATE Quiz 
	SET Title = @QuizTitle,
		UpdatedAt = GETUTCDATE();

	SELECT @QuizId =
		Id FROM Quiz WHERE LessonId = @LessonIdVariable;

	MERGE Question AS TARGET
	USING @Questions AS SOURCE
		ON (TARGET.Id = SOURCE.Id)
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (Id, QuizId, [Type], [Text], RewardPoints)
		VALUES (SOURCE.Id, @QuizId, SOURCE.[Type], SOURCE.[Text], SOURCE.RewardPoints)
	WHEN MATCHED
		THEN UPDATE 
		SET [Text] = SOURCE.[Text],
		[RewardPoints]  = SOURCE.[RewardPoints];

	MERGE Choice AS TARGET
	USING @Choices AS SOURCE
		ON (TARGET.Id = SOURCE.Id)
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (Id, QuestionId, [Text], IsAnswer)
		VALUES (SOURCE.Id, SOURCE.QuestionId, SOURCE.[Text], SOURCE.IsAnswer)
	WHEN MATCHED
		THEN UPDATE 
		SET [Text] = SOURCE.[Text],
		IsAnswer  = SOURCE.IsAnswer;
END