import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { lastValueFrom, Observable } from "rxjs";
import { User } from "../models/user.model";
import { Constants } from "../models/constants.model";
import { UserSession } from "../models/user-session.model";
import { LeaderboardUser } from "../models/leaderboard-user.model";
import { UserLessonSessionListItem } from "../models/user-sessions/user-lesson-session-list-item.model";
import { UserLessonSession } from "../models/user-sessions/user-lesson-lession.component";
import { UserSessionStatusEnum } from "../enums";
import { UserAchievement } from "../models/user-sessions/user-achievement.model";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private baseUrl: string = '';
    private noInterceptParams = {
        [Constants.NoIntercept]: "true"
    };
    constructor(private configService: ConfigService, private httpClient: HttpClient) {
        this.baseUrl = this.configService.getUrl("baseApiUrl");
    }

    registerUser(user: User): Observable<UserSession> {
        return this.httpClient.post<UserSession>(this.baseUrl + '/users/register', user, { params: this.noInterceptParams });
    }

    getUserPoints(userId: string): Promise<number> {
        return lastValueFrom(this.httpClient.get<number>(this.baseUrl + `/Users/GetUserPoints/${userId}`));
    }

    getUserSessions(userId: string, learningPathId: number): Promise<UserLessonSessionListItem[]> {
        return lastValueFrom(this.httpClient.get<UserLessonSessionListItem[]>(this.baseUrl + `/Users/GetUserSessions/${userId}/learningPath/${learningPathId}`));
    }

    getUserSession(userId: string, lessonId: number, learningPathId: number): Promise<UserLessonSession> {
        return lastValueFrom(this.httpClient.get<UserLessonSession>(this.baseUrl + `/Users/GetUserSession/${userId}/${lessonId}/${learningPathId}`));
    }

    getUsersForLeaderboard(startDate: Date, topNumber: number): Promise<Array<LeaderboardUser>> {
        const startDateStr = startDate.toLocaleDateString();
        return lastValueFrom(this.httpClient.get<LeaderboardUser[]>(this.baseUrl + `/Users/GetUsersForLeaderboard/${topNumber}?startDate=${startDateStr}`));
    }

    getUserById(userId: string): Promise<User> {
        return lastValueFrom(this.httpClient.get<User>(this.baseUrl + `/Users/GetUserById/${userId}`));
    }

    createUserSession(userId: string, learningPathId: number, lessonId: number, requiredPoints: number): Promise<string> {
        return lastValueFrom(this.httpClient.post<string>(this.baseUrl + `/Users/CreateSession`, { userId: userId, learningPathId: learningPathId, lessonId: lessonId, requiredPoints: requiredPoints }));
    }

    changeUserSessionStatus(userSessionId: string, status: UserSessionStatusEnum): Promise<void> {
        return lastValueFrom(this.httpClient.post<void>(this.baseUrl + `/Users/ChangeUserSessionStatus`, { userSessionId: userSessionId, status: status }));
    }

    getAllUserAchievements(userId: string): Promise<Array<UserAchievement>> {
        return lastValueFrom(this.httpClient.get<Array<UserAchievement>>(this.baseUrl + `/Users/GetAllUserAchievements/${userId}`));
    }
}