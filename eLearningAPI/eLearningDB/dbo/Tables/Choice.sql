﻿CREATE TABLE [dbo].[Choice] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [Text]       NVARCHAR (MAX)   NOT NULL,
    [IsAnswer]   BIT              NOT NULL,
    [QuestionId] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);

