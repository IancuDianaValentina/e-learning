import { QuestionTypeEnum } from "src/app/enums";
import { ChoiceVm } from "./choice.view-model";

export class QuestionFormModel {
    questionText: string;
    questionType: QuestionTypeEnum;
    rewardPoints: number;
}