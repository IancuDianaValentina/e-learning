﻿namespace eLearningAPI.Models.Quiz.Questions
{
    public class QuestionType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
