using Authentication.Middlewares;
using Authentication.Models;
using eLearningAPI.APIServices.Implementations;
using eLearningAPI.APIServices.Interfaces;
using eLearningAPI.BusinessLogic.Implementations;
using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Repositories.Implementations;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Net.Http.Headers;

namespace eLearningAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.Configure<JwtSettings>(Configuration.GetSection("JwtSettings"));

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ILearningPathRepository, LearningPathRepository>();
            services.AddTransient<IProgrammingLanguageRepository, ProgrammingLanguageRepository>();
            services.AddTransient<ILessonRepository, LessonRepository>();
            services.AddTransient<IQuestionRepository, QuestionRepository>();
            services.AddTransient<IQuizRepository, QuizRepository>();

            services.AddTransient<IUserBL, UserBL>();
            services.AddTransient<ILearningPathBL, LearningPathBL>();
            services.AddTransient<IProgrammingLanguageBL, ProgrammingLanguageBL>();
            services.AddTransient<ILessonBL, LessonBL>();
            services.AddTransient<IQuestionBL, QuestionBL>();
            services.AddTransient<IQuizBL, QuizBL>();

            services.AddHttpClient<IAuthenticationAPIService, AuthenticationAPIService>(client =>
            {
                client.BaseAddress = new Uri(ApiUrlsConfiguration.AuthenticationApiUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });

            services.AddCors(options =>
            {
                options.AddPolicy("EnableCORS", builder =>
                {
                    builder.AllowAnyOrigin()
                       .AllowAnyHeader()
                       .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("EnableCORS");
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseMiddleware<AuthenticationMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
