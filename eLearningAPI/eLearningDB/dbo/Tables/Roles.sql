﻿CREATE TABLE [dbo].[Roles] (
    [Id]   TINYINT       NOT NULL,
    [Name] NVARCHAR (20) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

