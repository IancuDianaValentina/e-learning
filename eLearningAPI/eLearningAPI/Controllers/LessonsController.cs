﻿using Authentication.Attributes;
using eLearningAPI.BusinessLogic.Interfaces;
using eLearningAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static Authentication.Enums.Enums;

namespace eLearningAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LessonsController : ControllerBase
    {
        private readonly ILessonBL _lessonBL;
        public LessonsController(ILessonBL lessonBL)
        {
            _lessonBL = lessonBL;
        }

        [HttpDelete("DeleteLesson/{lessonId}")]
        [Authorize(UserRoleEnum.Administrator)]
        public async Task<IActionResult> DeleteLesson(int lessonId)
        {
            await _lessonBL.DeleteLessonAsync(lessonId);
            return Ok();
        }

        [HttpGet("GetAllLessons")]
        public async Task<IActionResult> GetAllLessons()
        {
            var response = await _lessonBL.GetAllLessonsAsync();
            return Ok(response);
        }

        [HttpGet("GetLessonWithQuizById/{lessonId}")]
        public async Task<IActionResult> GetLesssonWithQuizById(int lessonId)
        {
            var response = await _lessonBL.GetLessonWithQuizByIdAsync(lessonId);
            return Ok(response);
        }

        [HttpPost("SaveLesson")]
        //[Authorize(UserRoleEnum.Administrator)]
        public async Task<IActionResult> SaveLesson([FromBody] Lesson lesson)
        {
            await _lessonBL.SaveLessonAsync(lesson);
            return Ok();
        }

        [HttpGet("GetLearningPathLesson/lesson/{lessonId}/learningPathId/{learningPathId}")]
        public async Task<IActionResult> GetLearningPathLesson(int lessonId, int learningPathId)
        {
            var response = await _lessonBL.GetLessonLearningPathAsync(lessonId, learningPathId);
            return Ok(response);
        }
    }
}
