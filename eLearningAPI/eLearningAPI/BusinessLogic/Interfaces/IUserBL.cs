﻿using eLearningAPI.Dtos.UserSesssion;
using eLearningAPI.Enums;
using eLearningAPI.Models;
using eLearningAPI.Models.UserSession;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.BusinessLogic.Interfaces
{
    public interface IUserBL
    {
        public Task<int> GetUserPointsAsync(Guid userId);
        public Task<User> GetUserByIdAsync(Guid userId);
        public Task<List<LeaderboardUser>> GetUsersForLeaderboardAsync(string startDate, int topNumber);
        public Task<List<UserLessonSessionListItem>> GetUserSessionsByLearningPathIdAsync(int learningPathId, Guid userId);
        public Task<Guid> CreateUserSessionAsync(Guid userId, int learningPathId, int lessonId, int requiredPoints);
        public Task<UserLessonSessionDto> GetUserSessionAsync(Guid userId, int lessonId, int learningPathId);
        public Task ChangeUserSessionStatusAsync(Guid userSessionId, UserSessionStatusEnum status);
        public Task<List<UserAchievement>> GetAllUserAchievementByUserIdAsync(Guid userId);
    }
}
