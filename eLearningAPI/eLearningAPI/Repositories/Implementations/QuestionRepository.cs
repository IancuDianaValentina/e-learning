﻿using Dapper;
using eLearningAPI.Models.Quiz.Questions;
using eLearningAPI.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eLearningAPI.Repositories.Implementations
{
    public class QuestionRepository: BaseRepository, IQuestionRepository
    {
        private static readonly string GetQuestionTypesSP = "QuestionTypes_Get";
        private static readonly string DeleteQuestionsByQuizIdSP = "Questions_DeleteByQuizId";
        private static readonly string DeleteChoiceByIdSP = "Choice_DeleteById";
        private static readonly string DeleteQuestionByIdSP = "Question_DeleteById";
        public QuestionRepository(IConfiguration configuration): base(configuration) { }
        public async Task<List<QuestionType>> GetQuestionTypesAsync()
        {
            using (var connection = Connection)
            {
                return (await connection.QueryAsync<QuestionType>(GetQuestionTypesSP,
                   commandType: System.Data.CommandType.StoredProcedure)) as List<QuestionType>;
            }
        }

        public async Task DeleteQuestionsByQuizIdAsync(int quizId)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(DeleteQuestionsByQuizIdSP,
                    param: new
                    {
                        QuizId = quizId
                    },
                   commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task DeleteChoiceByIdAsync(int choiceId)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(DeleteChoiceByIdSP,
                    param: new
                    {
                        ChoiceId = choiceId
                    },
                   commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public async Task DeleteQuestionByIdAsync(int questionId)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(DeleteQuestionByIdSP,
                    param: new
                    {
                        QuestionId = questionId
                    },
                   commandType: System.Data.CommandType.StoredProcedure);
            }
        }
    }
}
