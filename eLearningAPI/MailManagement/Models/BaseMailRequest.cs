﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace MailManagement.Models
{
    public class BaseMailRequest
    {
        public string Subject { get; set; }
        public string Content { get; set; }
        public List<IFormFile> Attachments { get; set; }
    }
}
